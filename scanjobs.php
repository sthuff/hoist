<?php
/****************************************************************************

	The HOIST automates Nessus scans and reporting features for the ITSO.

****************************************************************************/
/****************************************************************************

	Docs and Details pending.	

****************************************************************************/

	include_once("initial_config.inc.php");
	include_once("doctype.inc.php");
?>
<html>
<head>
	<title>HOIST - Hands-Off ITSO Scanning Tool</title>
<?php
	include_once("master_css.inc.php");
	include_once("meta_data.inc.php");
	include_once("javascripts.inc.php");
?>

</head>
<body>
	
<div id="header">			<!-- header -->
	<div class="bg">
		<div class="container"> 	<!-- container -->
				<div class="title"></div>
				<div class="logo"></div>
				<div class="content">&nbsp;</div>
				<div class="navbar">
<?php
					include_once("navbar.php");					
?>
				</div>
				<div class="clear"></div>
		</div> 				<!-- container end -->
	</div>
</div> 					<!-- header end -->

<div id="maincontent"> <!-- maincontent -->
		<div class="bg">
			<div class="navbarbg"></div>
			<div class="container">
<?php

	// include the Service-Now API functions
	include_once("./assets/servicenow_funcs.php");

/****************************************************************************

	SHOW SINGLE SCAN JOB DETAILS

****************************************************************************/
	if (isset($_GET["jobID"])) {
		
		// Trim and sanitize posted variables...
		$jobID = trim($_GET["jobID"]);
		$jobID = filter_var($jobID, FILTER_SANITIZE_STRING);

		$ioResultList = getRITMData($jobID, $casUser, $modeURL);
		
		$snRitmURL = "https://vt4help".$modeURL.".service-now.com/sc_req_item.do?sys_id=".$ioResultList["reqItemSysID"]."&sysparm_view=ess&sysparm_record_target=sc_req_item";
?>		
				<h2>[ Vulnerability Scan Job - <a href="<?php print $snRitmURL; ?>"><?php print $jobID; ?></a> ]</h2>

		<div id="formFrame">
		<form>
			<input type="hidden" id="casUser" name="casUser" value="<?php print $casUser; ?>" />
			<input type="hidden" id="casUserSysID" name="casUserSysID" value="<?php print $ioResultList["casUserSysID"]; ?>" />
			<input type="hidden" id="jobID" name="jobID" value="<?php print $jobID; ?>" />
			<input type="hidden" id="userPermissions" name="userPermissions" value="<?php print $userPermissions; ?>" />
			<input type="hidden" id="scanCreator" name="scanCreator" value="<?php print $casUser; ?>" />

			
		<table border="0" class="scanFormTable">
		<tr>
			<td>Requestor PID:</td>
			<td>
				<?php print $ioResultList["namePID"]." (".$ioResultList["name"].")"; ?>
				<input type="hidden" id="scanRequestor" name="scanRequestor" value="<?php print $ioResultList["namePID"]; ?>" />
				<input type="hidden" id="scanRequestorName" name="scanRequestorName" value="<?php print $ioResultList["name"]; ?>" />
				<input type="hidden" id="scanRequestorID" name="scanRequestorID" value="<?php print $ioResultList["nameID"]; ?>" />
			</td>
			<td>Requestor Department:</td>
			<td>
				<div id="departmentNameText"><?php print $ioResultList["departmentName"]; ?></div>
				<input type="hidden" id="departmentName" name="departmentName" value="<?php print $ioResultList["departmentName"]; ?>" />
				<input type="hidden" id="departmentID" name="departmentID" value="<?php print $ioResultList["departmentID"]; ?>" />
			</td>

		</tr>
		<tr>
			<td>SN Stage Due Date:</td>
			<td>
				<?php print $ioResultList["reqItemDueDate"]; ?>
			</td>
			<td>SN Request Stage:</td>
			<td>
				<?php print $ioResultList["reqItemStage"]; ?>
			</td>

		</tr>
		<tr>
			<td>
				Credentialed Scan?<br/>
			</td>
			<td>
<?php
				if ($ioResultList["scanIsUnauth"] == "true") {
?>
				Unauthenticated
				<input type="hidden" id="scanUnauth" name="scanUnauth" value="true"> 
<?php
				} else {
?>
				<input type="hidden" id="scanUnauth" name="scanUnauth" value="false"> 
<?php
				}
				
				if ($ioResultList["scanIsAuth"] == "true" && $ioResultList["scanIsUnauth"] == "true") { print ", "; }
		
				if ($ioResultList["scanIsAuth"] == "true") {
?>
				Authenticated
				<input type="hidden" id="scanAuth" name="scanAuth" value="true"> 
<?php
				} else {
?>
				<input type="hidden" id="scanAuth" name="scanAuth" value="false"> 
<?php
				}
		
				if ($ioResultList["scanIsAuth"] == "false" && $ioResultList["scanIsUnauth"] == "false") { print "No option selected"; }
?>
			</td>
			<td>
				Preferred Scan Window:
			</td>
			<td>
<?php
				// after_hours
				// Anytime
				// business
				if ($ioResultList["preferredTime"] == "after_hours") {
					$nicePrefTimeText = "After Hours";
					$time10pmEasternTZ = new DateTime('today 22:00:00', new DateTimeZone('America/New_York'));
					$nicePrefTime = $time10pmEasternTZ->format('Y-m-d H:i:s O');
				} else if ($ioResultList["preferredTime"] == "business") {
					$nicePrefTimeText = "Business Hours";
					$time12pmEasternTZ = new DateTime('today 12:00:00', new DateTimeZone('America/New_York'));
					$nicePrefTime = $time12pmEasternTZ->format('Y-m-d H:i:s O');
				} else {
					$nicePrefTimeText = "Anytime";
					$time11pmEasternTZ = new DateTime('today 23:00:00', new DateTimeZone('America/New_York'));
					$nicePrefTime = $time11pmEasternTZ->format('Y-m-d H:i:s O');
				}
?>
			<?php print $nicePrefTimeText; ?> <br/>(<?php print $nicePrefTime; ?>)
			</td>
		</tr>
		<tr>
			<td>
				Host or Application Scan: <br/>
			</td>
			<td>
				<?php print $ioResultList["scanHostApp"]; ?><br/>
				<input type="hidden" id="scanHostApp" name="scanHostApp" value="<?php print $ioResultList["scanHostApp"]; ?>" />			
			</td>
			<td>
				Recurrence? <br/>
			</td>
			<td>
<?php
				if ($ioResultList["scanRecurrence"] == "true") {
?>
				Recurring Scan<br/>
				<input type="hidden" id="scanRecurrence" name="scanRecurrence" value="true"> 
<?php
				} else {
?>
				None <br/>
				<input type="hidden" id="scanRecurrence" name="scanRecurrence" value="false"> 
<?php
				}
?>				
			</td>
		</tr>
		<tr>
			<td colspan="4">
				Notes from Scan Requestor: <br/>
				<div class="otherNotesFrame"><?php print $ioResultList["otherNotes"]; ?></div>
				<input type="hidden" id="otherNotes" name="otherNotes" value="<?php print $ioResultList["otherNotes"]; ?>" />
			</td>
		</tr>
		<tr>
			<td colspan="3">
				Scan Targets: <br/>
				Should be a hyphenated IP Range, CIDR Notation or Individual IPs in a comma-separated list. <br/>
<?php
		
				$scanTargetIPString = "";
				$ioResultList["scanTargetIPs"] = trim($ioResultList["scanTargetIPs"]);
				$scanTargetIPsList = explode(",", $ioResultList["scanTargetIPs"]);
		
				$malformedIPString = 0;
		
				foreach ($scanTargetIPsList as $scanTargetIP) {
					
					//print "<br/>DEBUG: ScanTargetIP is: ".$scanTargetIP."<br/>";
					
					$scanTargetIP = trim(str_replace(' ', '', $scanTargetIP));
					$scanTargetIPValid = filter_var($scanTargetIP, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE);

					if ($scanTargetIPValid !== FALSE) { 
						$scanTargetIPString .= $scanTargetIPValid.", "; 
					}
					else {
						// check for a hyphenated range
						$scanTargetIPRange = explode("-", $scanTargetIP); 

						//print "<br/>DEBUG: RangeObj is: |";
						//print_r($scanTargetIPRange)."|<br/>";

						$scanTargetIPStart = trim($scanTargetIPRange[0]);
						
						//print "<br/>DEBUG: ScanTargetIPStart is: |".$scanTargetIPStart."|<br/>";

						$scanTargetIPStart = filter_var($scanTargetIPStart, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE);

						if ($scanTargetIPStart !== FALSE) {
							
							$scanTargetIPEnd = trim($scanTargetIPRange[1]);
							
							//print "<br/>DEBUG: ScanTargetIPEnd is: |".$scanTargetIPEnd."|<br/>";
							
							$scanTargetIPEnd = filter_var($scanTargetIPEnd, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE);
							
							if ($scanTargetIPEnd !== FALSE) {
								$scanTargetIPString .= $scanTargetIPStart."-".$scanTargetIPEnd.", "; 
							
							}
						} 
						else {
							
							//print "<br/>DEBUG: ScanTargetIPCIDR is: |".$scanTargetIP."|<br/>";
							
							// check for CIDR notation
							$scanTargetIPCIDR = explode("/", $scanTargetIP); 

							$scanTargetIPCIDRNet = trim($scanTargetIPCIDR[0]);
							$scanTargetIPCIDRNet = filter_var($scanTargetIPCIDRNet, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE);

							if ($scanTargetIPCIDRNet !== FALSE) {

								$scanTargetIPCIDRMask = trim($scanTargetIPCIDR[1]);
								$scanTargetIPCIDRMask = filter_var($scanTargetIPCIDRMask, FILTER_VALIDATE_INT);

								if ($scanTargetIPCIDRMask !== FALSE) {
									$scanTargetIPString .= $scanTargetIPCIDRNet."/".$scanTargetIPCIDRMask.", ";
								} 
								else {
									$malformedIPString = 1; break; 
								}
							}
						}
					}
				}
		
				// if we have malformed IPs, just display the unaltered string and a warning
		
				if ($malformedIPString == 1) { 
					$scanTargetIPString = $ioResultList["scanTargetIPs"]; 
					$readonlyIPString = "";
?>
					<span class="red">WARNING: Please validate the Scan Target IPs.  Allowed formats are any mix of a hyphenated IP range, CIDR notation or a set of individual IP addresses separated by commas.</span>
<?php
				} 
				else {
					if (count($scanTargetIPString) > 6) {
						// shortest possible IP address is 7 chars long (e.g. 8.8.8.8)...
						$scanTargetIPString = substr($scanTargetIPString, 0, -2); 
						$readonlyIPString = "readonly=\"readonly\"";
					}
					else {
						$scanTargetIPString = $ioResultList["scanTargetIPs"]; 
						$readonlyIPString = "";
					}
				}
?>
				<textarea id="scanTargetIPs" name="scanTargetIPs" rows="5" cols="101" <?php print $readonlyIPString; ?>><?php print $scanTargetIPString; ?></textarea>
				<input type="hidden" id="scanTargetIPsOrig" value="<?php print $scanTargetIPString; ?>" />				
			</td>
			<td>
				<input type="button" id="resetTargetIPs" class="styledButton" value="Reset Target IPs" style="margin-left: 84px; top: 15px; display:none;" />
			</td>
		</tr>
		<tr>
			<td colspan="3">
<?php
				$scanRecipientString = "";
				$scanRecipientFullString = "";

				if (isset($ioResultList["scanAccess"][0])) {
					foreach ($ioResultList["scanAccess"] as $scanRecipient) {
						$scanRecipientEmail = trim($scanRecipient["pid"]."@vt.edu");
						$scanRecipientEmail = filter_var($scanRecipientEmail, FILTER_SANITIZE_EMAIL);
						$scanRecipientString .= $scanRecipientEmail.",";

						$scanRecipientFullString .= $scanRecipient["name"]." (".$scanRecipientEmail."), ";
					}
					// remove last comma
					$scanRecipientString = substr($scanRecipientString, 0, -1);
					$scanRecipientFullString = substr($scanRecipientFullString, 0, -2);
				}
?>
				Scan Report Recipients: Should be a comma-separated list of email addresses <br/>
				<textarea id="scanRecipients" name="scanRecipients" rows="2" cols="101" readonly="readonly"><?php print $scanRecipientString; ?></textarea> 
				<input type="hidden" id="scanRecipientsOrig" value="<?php print $scanRecipientString; ?>" />
				<input type="hidden" id="scanRecipientsFullOrig" value="<?php print $scanRecipientFullString; ?>" />
			</td>
			<td>				
<?php
				if ($ioResultList["reqItemStage"] == "ITSO Approval & Scheduling") {
?>
					Go straight to the Schedule Scan page once Approved? <input type="checkbox" id="gotoSchedule" name="gotoSchedule" value="true" checked="checked">
<?php
				} 
?>				<input type="button" id="resetRecipients" class="styledButton" value="Reset Recipients" style="margin-left: 84px; top: 15px; display: none;" />
			</td>
		</tr>
		<tr>
<?php
			if ($ioResultList["reqItemStage"] == "Scanning & Generating Report") {	
?>
			<td>
				&nbsp;
			</td>
			<td colspan="2">
				<div id="feedback" class="feedbackFrame" style="margin-left: 0px; width: 440px;">
					This request has already been approved. <br/>
					<a href="<?php print $snRitmURL; ?>">Check request status and work notes in Service-Now</a> <br/>
					to see if it needs to be
					<a href="scansched.php?jobID=<?php print $jobID; ?>">scheduled</a>.
				</div>
			</td>
			<td>
				&nbsp;
			</td>
<?php
			} else if ($ioResultList["reqItemStage"] == "ITSO Approval & Scheduling") {
?>
			<td>
				<div style="position:relative; top: 25px;"><input type="button" id="rejectScanButton" class="styledButton" value="REJECT Scan" style="width: 217px;" /> </div>
			</td>
			<td colspan="2">
				<div id="feedback" class="feedbackFrame" style="margin-left: 0px; width: 440px;">&nbsp;</div>
			</td>
			<td>
				<div style="position:relative; top: 25px; margin-right: 80px;"><input type="button" id="approveScanButton" class="styledButton" value="APPROVE Scan" style="width: 217px;" /> </div>
			</td>
<?php
			} else {
?>
			<td>
				&nbsp;
			</td>
			<td colspan="2">
				<div id="feedback" class="feedbackFrame" style="margin-left: 0px; width: 440px;">
					Request status UNKNOWN. <br/>
					<a href="<?php print $snRitmURL; ?>">Check request status in Service-Now.</a>
				</div>
			</td>
			<td>
				&nbsp;
			</td>

<?php
			}
?>
		</tr>
		</table>
						
		</form>
		</div>
<?php
		// DEBUG
		//print_r($ioResultList);
			
	}
	else { // NO JOBID SET
/****************************************************************************

	NO SCAN ID SET - SCAN STATUS TABLE
	
****************************************************************************/
?>			
		<!-- form start -->  

		<div id="formFrame">
<?php
	// force DEV mode if we're on a dev instance...
	if ($_SERVER["SERVER_NAME"] == "localhost" || strstr($_SERVER["REQUEST_URI"], "/hoistdev")) { 
		print "<strong>NOTICE: This is the development (".$modeURL.") version of HOIST.</strong><br/><br/>";
		$mode = "DEV"; 
	}
?>
			<br/>
			<h2>[ Vulnerability Scan Job Queue ]</h2>
			
			<br/>
			<br/>
	
<?php
		$snRITMListJSONResponse = getRITMList($casUser, $modeURL);
		
		if ($snRITMListJSONResponse->result == "[]") {
			print "No active Vulnerability Scan jobs exist.";
		} else {
?>
		<form>
			<input type="hidden" id="casUser" name="casUser" value="<?php print $casUser; ?>" />
			<input type="hidden" id="userPermissions" name="userPermissions" value="<?php print $userPermissions; ?>" />
			
			<table id="jobsListTable" class="display" cellspacing="0" width="100%">
			<thead>
			<tr>
				<th>Due Date</th>
				<th>RITM Number</th>
				<th>Created By</th>
				<th>Stage</th>
				<th>Date</th>
			</tr>
			</thead>
			<tfoot>
			<tr>
				<th>Due Date</th>
				<th>RITM Number</th>
				<th>Created By</th>
				<th>Stage</th>
				<th>Date</th>
			</tr>
			</tfoot>
			<tbody>	
<?php
			// approval->value = approved, Display = Approved
				
			foreach ($snRITMListJSONResponse->result as $snRITM) {
				//print "DEBUG: <br/>";
				//print_r($snRITM);
				//print "<br/>";
				
				if ($snRITM->stage->value == "ITSO Approval & Scheduling") { 
					$niceStage = "Pending Approval"; 
					$jobURL = "scanjobs.php?jobID=".$snRITM->number->value;
				}
				else if ($snRITM->stage->value == "Scanning & Generating Report") { 
					$niceStage = "Approved"; 
					$jobURL = "scansched.php?jobID=".$snRITM->number->value;
				}
				else {
					$niceStage = $snRITM->stage->value;
					$jobURL = "scanjobs.php?jobID=".$snRITM->number->value;
				}
					//request->value
?>
				<tr>
					<td><?php print $snRITM->due_date->value; ?></td>
					<td><a href="<?php print $jobURL; ?>"><?php print $snRITM->number->value; ?></a></td>
					<td><?php print $snRITM->sys_created_by->value; ?></td>
					<td><?php print $niceStage; ?></td>
					<td><?php print $snRITM->sys_created_on->value; ?></td>
				</tr>
<?php
			}
?>
			</tbody>
			</table>
			</form>
<?php			
		} // END RITMList ELSE
?>
			
		<br/>
		<br/>
		</div> <!-- end formFrame DIV -->
<?php
	} // NO JOBID SET END
?>

		<div id="responseFrame" style="display:none;">
			<div id="response"><img src='./images/loading16x16.gif' /> Awaiting response from Service-Now, please wait...</div>
			<br/>
			<br/>
		</div>
				
		<div id="feedback"></div>
				
			</div> 					<!-- container class end -->
	</div>
</div> 					<!-- maincontent end -->
			
</body>
</html>