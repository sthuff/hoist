## Synopsis

Hands Off ITSO Scanning Tool (HOIST)

Helps to automate scan tasks with Nessus, including scheduling and generating reports.  Integrtes with Service-Now to streamline initial scan settings and provide user notificaiton after scan completion. Google Drive API is used for report respository.

## Motivation

Improve Scan task efficiencies.


## Installation

See Dockerfile(s)
