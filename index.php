<?php
/****************************************************************************

	The HOIST automates Nessus scans and reporting features for the ITSO.

****************************************************************************/
/****************************************************************************

	Docs and Details pending.	

****************************************************************************/

	include_once("initial_config.inc.php");
	include_once("doctype.inc.php");
?>
<html>
<head>
	<title>HOIST - Hands-Off ITSO Scanning Tool</title>
<?php
	include_once("master_css.inc.php");
	include_once("meta_data.inc.php");
	include_once("javascripts.inc.php");
?>

</head>
<body>
	
<div id="header">			<!-- header -->
	<div class="bg">
		<div class="container"> 	<!-- container -->
				<div class="title"></div>
				<div class="logo"></div>
				<div class="content">&nbsp;</div>
				<div class="navbar">
<?php
					include_once("navbar.php");					
?>
				</div>
				<div class="clear"></div>
		</div> 				<!-- container end -->
	</div>
</div> 					<!-- header end -->

<div id="maincontent"> <!-- maincontent -->
		<div class="bg">
			<div class="navbarbg"></div>
			<div class="container">
<?php

	// include the Nessus API functions
	include_once("./assets/nessus_api_funcs.inc.php");

/****************************************************************************

	SHOW SINGLE SCAN DETAILS

****************************************************************************/
	if (isset($_GET["scanID"])) {
		
		// Trim and sanitize posted variables...
		$scanID = trim($_GET["scanID"]);
		$scanID = filter_var($scanID, FILTER_SANITIZE_NUMBER_INT);
		
		$scanInfoJSON = nessusAPIQuery("scans/".$scanID, "get", "");
		
		$scanInfo = json_decode($scanInfoJSON);
		//print "<br/>DEBUG: scanInfo is: <br/>";
		//var_dump($scanInfo);
		//print "<br/><br/>";

		// JSON objects of interest:
		// policy, status, info->targets, scan_start, scan_end, hostcount, name
		
		if (isset($scanInfo->info->scan_start)) {
			$epochStart = $scanInfo->info->scan_start;
			$scanStartDate = new DateTime("@$epochStart");
			$scanStartDate->setTimeZone(new DateTimeZone('America/New_York'));
		}
		
		if (isset($scanInfo->info->scan_end)) {
			$epochEnd = $scanInfo->info->scan_end; 
			$scanEndDate = new DateTime("@$epochEnd");
			$scanEndDate->setTimeZone(new DateTimeZone('America/New_York'));
		
			$scanDuration = $scanEndDate->diff($scanStartDate);
		}
		
		if (isset($scanInfo->info->policy)) { $nicePolicy = $scanInfo->info->policy; } 
		else { $nicePolicy = "Policy Unknown"; }

		if (isset($scanInfo->info->uuid)) { $niceUUID = $scanInfo->info->uuid; } 
		else { $niceUUID = "Scan UUID Unknown"; }

		if (isset($scanInfo->info->hostcount)) { $niceHostCount = $scanInfo->info->hostcount; } 
		else { $niceHostCount = "Unknown"; }

		if ($scanInfo->info->status == "empty") { $niceStatus = "<span class='red'>Never Run</span>"; }
		else if ($scanInfo->info->status == "completed") { $niceStatus = "<span class='green'>Completed</span>"; }
		else if ($scanInfo->info->status == "running") { $niceStatus = "<span class='blue'>Running</span>"; }
		else { $niceStatus = $scanInfo->info->status; }		
		
		
		if ($scanInfo->info->status == "completed") { 
			$histID = $scanInfo->history[count($scanInfo->history)-1]->history_id; 
			$scanLastModified = $scanInfo->history[count($scanInfo->history)-1]->last_modification_date;
		}
		
		include("assets/hoist_db_funcs.inc.php");

		include("assets/db_info.inc.php");
		$dbName = "hoist";

		$scanFolderID = "";
		$scanFolderName = "";
		$scanCreator = $casUser;
		$scanRequestor = "UNKNOWN";
		$scanRecipients = "";
		$scanAutoExport = "";
		$scanAutoNotify = "";
		$googleReportID = "";
		$googleFolderName = "";
		$snRITM = "";
		$reportFormat = "";
		$scanIsUnauth = "";
		$scanIsAuth = "";
		$scanHostApp = "";
		$scanNotInDB = 0;
		$reportHistURL = "";

		try {
			$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
			$statement = $connection->prepare('SELECT scanID, scanCreator, scanRequestor, scanRecipients, scanName, snRITM, scanIsUnauth, scanIsAuth, scanHostApp, reportFormat, scanFolderID, scanFolderName, googleFolderName, scanAutoExport FROM scans WHERE scanID = :scanID LIMIT 1');
			$statement->execute(array('scanID' => $scanID));

			if ($statement->rowCount() > 0) {

				$rows = $statement->fetchAll(PDO::FETCH_ASSOC);

				foreach ($rows as $scanRow) { 

					//print "<br/>DEBUG: <br/>";
					//var_dump($scanRow);
					//print "<br/>";
					$scanFolderID = $scanRow["scanFolderID"];;
					$scanFolderName = $scanRow["scanFolderName"];
					$scanCreator = $scanRow["scanCreator"];
					$scanRequestor = $scanRow["scanRequestor"];
					$scanRecipients = $scanRow["scanRecipients"];
					$scanName = $scanRow["scanName"];
					$snRITM = $scanRow["snRITM"];
					$reportFormat = $scanRow["reportFormat"];
					$scanAutoExport = $scanRow["scanAutoExport"];
					$googleFolderName = $scanRow["googleFolderName"];
					
					if ($scanInfo->info->status == "completed") {
						try {
							$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
							unset($dbUser);
							unset($dbPass);

							$statement = $connection->prepare('SELECT googleReportID FROM scanhistory WHERE histID = :histID LIMIT 1');
							$statement->execute(array('histID' => $histID));

							if ($statement->rowCount() > 0) {

								$rows = $statement->fetchAll(PDO::FETCH_ASSOC);

								foreach ($rows as $scanRow) { 
									$googleReportID = $scanRow["googleReportID"];
								}
							}
						}
						catch(PDOException $e) { print "Error: ".$e->getMessage(); }
					} // end if scan completed
				} // end foreach scan

			} else {
				$scanNotInDB = 1;
				$scanFolderName = "UNKNOWN";
				$googleFolderName = "UNKNOWN";
				$scanFolderID = $scanInfo->info->folder_id;
				
				$jsonData = "";
				$action = "folders";

				$getFolder = nessusAPIQuery($action, "get", $jsonData);
				$getFolderObj = json_decode($getFolder);

		//		print "<br/>DEBUG: getFolderObj Object: <br/>";
		//		var_dump($getFolderObj);
		//		print "<br/>";

				foreach ($getFolderObj->folders as $folder) {

					if ($folder->id == $scanFolderID) {
						$scanFolderName = $folder->name;
						$folderExists = 1;
					}
				}
			} // end else scan exists
		}
		catch(PDOException $e) { print "Error: ".$e->getMessage(); }

		// clear the connection
		$connection = null;
?>				
		<h1>Scan [ <?php print $scanInfo->info->name; ?> ]</h1> <br/>

		<table class="scanDetailsTable">
		<tr>
			<td width="150px">Policy Used:</td>
			<td width="220px"><strong><?php print $nicePolicy; ?></strong></td>
			
			<td width="150px">Status:</td>
			<td width="220px"><?php print $niceStatus; ?></td>
			
			<td width="150px">Scan ID:</td>
			<td width="220px">
<?php 
				print $scanInfo->info->object_id;
				if ($scanInfo->info->status == "completed") { print ".".$histID; } 
?>
			</td>
		</tr>
		<tr>
			<td>Started:</td>
			<td>
<?php
			if ($scanInfo->info->status != "completed") { print $niceStatus; }
			else { print $scanStartDate->format('Y-m-d H:i:s T'); }
?>
			</td>
			
			<td>Ended:</td>
			<td>
<?php
			if ($scanInfo->info->status != "completed") { print $niceStatus; }
			else { print $scanEndDate->format('Y-m-d H:i:s T'); }
?>
			</td>
			
			<td>Duration:</td>
			<td>
<?php
				if ($scanInfo->info->status != "completed") { print $niceStatus; }
				else { print $scanDuration->format('%H hrs %I mins %S secs'); }
?>
			</td>
		</tr>
		<tr>
			<td>Report Format:</td>
			<td>
<?php 
				if ($reportFormat == "db") { $niceReportFormat = "Nessus DB"; }
				else { $niceReportFormat = strtoupper($reportFormat); }
		
				print $niceReportFormat; 
?>
			</td>

			<td>Nessus Folder:</td>
			<td colspan="2">
<?php
				if ($scanInfo->info->status != "completed") { print $niceStatus; }
				else { print $scanFolderName; }
?>
			</td>
			<td>
				<input type="button" id="scanHistoryButton" class="styledButton" value="Show Scan History" />
			</td>
		</tr>
		</table>
		<br/>
				
		<form>
		<table class="scanDetailsTable">
		<tr>
<?php
		// Check the status of a report for this scan
		if ($scanInfo->info->status == "completed") {			
?>
			<td width="150px" style="vertical-align:middle;">
				<input type="hidden" id="scanID" value="<?php print $scanInfo->info->object_id; ?>" />
				<input type="hidden" id="histID" value="<?php print $histID; ?>" />
				<input type="hidden" id="scanLastModified" value="<?php print $scanLastModified; ?>" />
				<input type="hidden" id="scanName" value="<?php print $scanInfo->info->name; ?>" />				
				<input type="hidden" id="scanUUID" value="<?php print $niceUUID; ?>" />
				<input type="hidden" id="scanTargetIPs" value="<?php print $scanInfo->info->targets; ?>" />
				<input type="hidden" id="scanStartDate" value="<?php print $scanStartDate->format('Y-m-d_H_i_s'); ?>" />
				<input type="hidden" id="scanEndDate" value="<?php print $scanEndDate->format('Y-m-d_H_i_s'); ?>" />
				<input type="hidden" id="scanFolderID" value="<?php print $scanFolderID; ?>" />
				<input type="hidden" id="scanFolderName" value="<?php print $scanFolderName; ?>" />

				<input type="hidden" id="googleFolderName" value="<?php print $googleFolderName; ?>" />
				<input type="hidden" id="scanAutoExport" value="<?php print $scanAutoExport; ?>" />
				<input type="hidden" id="scanAutoNotify" value="<?php print $scanAutoNotify; ?>" />	
				<input type="hidden" id="scanCreator" value="<?php print $scanCreator; ?>" />
				<input type="hidden" id="scanRequestor" value="<?php print $scanRequestor; ?>" />
				<input type="hidden" id="scanRecipients" value="<?php print $scanRecipients; ?>" />
				<input type="hidden" id="snRITM" value="<?php print $snRITM; ?>" />
				<input type="hidden" id="scanIsUnauth" value="<?php print $scanIsUnauth; ?>" />
				<input type="hidden" id="scanIsAuth" value="<?php print $scanIsAuth; ?>" />
				<input type="hidden" id="scanHostApp" value="<?php print $scanHostApp; ?>" />
				<input type="hidden" id="reportFormat" value="<?php print $reportFormat; ?>" />
				
				<input type="hidden" id="formAction" value="genreport" />
				<input type="hidden" id="scanNotInDB" value="<?php print $scanNotInDB; ?>" />
				<input type="hidden" id="casUser" name="casUser" value="<?php print $casUser; ?>" />
				<input type="hidden" id="userPermissions" name="userPermissions" value="<?php print $userPermissions; ?>" />

<?php
			if ($googleReportID != "") {													 
?>
				<span id="genReportStatus">Report Exists.</span>
<?php
			} else {
?>
				<span id="genReportStatus">
				<input type="button" id="generateReport" value="Generate Report" style="width:155px; padding: 0px;" /><br/>
				<input type="button" id="editScanDBButton" value="Update HOIST Details" style="width:155px; padding: 0px;" />
				</span>
<?php
			}
?>
			</td>
			<td width="180px" style="padding-left: 20px;">
<?php
			if ($googleReportID != "") {
?>
				<span id="genReportStatusCBs">&nbsp;</span>
<?php
			} else {
?>
				<span id="genReportStatusCBs">
					<label>Google Drive? <input type="checkbox" id="scanReportGoogle" value="googlereport" checked="checked" /></label> <br/>
					<label>Local Copy? <input type="checkbox" id="scanReportLocal" value="localreport" /></label> <br/>
				</span>
<?php
			}
?>
			</td>
			<td class="scanURLFrame">
				<div id="scanURL">
<?php
			if ($scanNotInDB == 1) {
				print "This scan exists in Nessus but was not found in HOIST or some details were missing.  Please click the Edit HOIST Details button to complete the details needed to generate a report.";
			} else {
				if ($googleReportID != "") {
					
					$googleDriveURL = "https://drive.google.com/uc?export=download&id=".$googleReportID;
					$snRitmURL = "https://vt4help".$modeURL.".service-now.com/sc_req_item.do?sysparm_query=number=".$snRITM."&sysparm_view=ess&sysparm_record_target=sc_req_item";
?>
					Google Drive Link:<br/>
					<a href='<?php print $googleDriveURL; ?>' target='_blank'><?php print $googleDriveURL; ?></a>
					<br/>
<?php
					include_once("assets/servicenow_funcs.php");
					
					$ritmStatusList = getRITMStatus($snRITM, $casUser, $modeURL);

					if (isset($ritmStatusList["reqItemStage"]) && $ritmStatusList["reqItemStage"] != "complete") {
?>
						Notify Scan Recipients from 
						<a href="<?php print $snRitmURL; ?>" target="_blank"><?php print $snRITM; ?></a>
						and send them the link in Service-Now?  <br/>
						<input type="button" id="notifyRecipients" value="Notify Recipients" style="width:155px; padding: 0px;" />
						<input type="hidden" id="googleDriveURL" name="googleDriveURL" value="<?php print $googleDriveURL; ?>" />
<?php
					} else if (isset($ritmStatusList["reqItemStage"]) && $ritmStatusList["reqItemStage"] == "complete") {
?>
						<a href="<?php print $snRitmURL; ?>" target="_blank"><?php print $snRITM; ?></a> 
						is <?php print $ritmStatusList["reqItemStage"]; ?> and users have been notified of the link.
<?php
					} else { print "RITM not found in SN."; }
				}
			}
?>									
					<div id="scanDetailsDialog" style="display:none">
<?php 
					include_once("scandetails_dialog.inc.php");
?>					
					</div>
				</div>
			</td>
<?php
		} else {
?>
			<td colspan="2" width="370px">
				<?php print $niceStatus; ?> <br/><br/>
			</td>
			<td>
				<div id="scanURL">Report generation unavailable until scan completion. 
				<input type="button" id="refreshScanStatus" value="Refresh"/> </div>
			</td>
<?php
		}
?>
		</tr>
		</table>
			
		<div id="scanHistoryDialog" style="display:none">
<?php 
			include_once("scanhistory_dialog.inc.php");
?>
		</div>
		</form>
		<br/>
		<br/>
<?php
		if ($scanInfo->info->status != "empty") {
?>
			<h2>Host Breakdown [ <?php print $niceHostCount; ?> ]:</h2><br/>
<?php		
			foreach ($scanInfo->hosts as $scanHost) {
?>
			<table class="hostBreakdownTable">
			<tr>
				<td>Host:</td>
				<td colspan="3"><h3><?php print $scanHost->hostname; ?></h3></td>

				<td><span class="lightblue">Info:</span></td>
				<td><span class="lightblue"><?php print $scanHost->info; ?></span></td>
			</tr>
			<tr>
				<td><span class="red"><strong>Critical:</strong></span></td>
				<td><span class="red"><strong><?php print $scanHost->critical; ?></strong></span></td>

				<td><span class="orange">High:</span></td>
				<td><span class="orange"><?php print $scanHost->high; ?></span></td>

				<td><span class="yellow">Medium:</span></td>
				<td><span class="yellow"><?php print $scanHost->medium; ?></span></td>
			</tr>
			<tr>
				<td>Checks Progress:</td>
				<td><?php print $scanHost->scanprogresscurrent; ?></td>

				<td>Checks Considered:</td>
				<td><?php print $scanHost->totalchecksconsidered; ?></td>

				<td>Checks Complete:</td>
				<td><?php print $scanHost->scanprogresstotal; ?></td>
			</tr>
			</table>
<?php
			}
		} // END status NOT empty IF
	}
	else { // NO SCANID SET
/****************************************************************************

	NO SCAN ID SET - SCAN STATUS TABLE
	
****************************************************************************/
		
		include("assets/hoist_db_funcs.inc.php");
?>			
		<!-- form start -->  

		<div id="formFrame">
<?php
	// force DEV mode if we're on a dev instance...
	if ($_SERVER["SERVER_NAME"] == "localhost" || strstr($_SERVER["REQUEST_URI"], "/hoistdev")) { 
		print "<strong>NOTICE: This is the development version of HOIST.</strong><br/><br/>";
		$mode = "DEV"; 
	}
?>	
			<br/>
			<h2>[ Scan and Report Status ]</h2>
			
			<br/>
			<br/>
			
		<form>
			<input type="hidden" id="casUser" name="casUser" value="<?php print $casUser; ?>" />
			<input type="hidden" id="userPermissions" name="userPermissions" value="<?php print $userPermissions; ?>" />
<?php
		$scanList = nessusAPIQuery("scans", "get", "");

		$scanListObj = json_decode($scanList);
		
		//print "<br/>DEBUG: ScanList is: <br/>";
		//var_dump($scanListObj);
		//print "<br/><br/>";
		
		if (isset($scanListObj->scans)) {
?>			
		<table id="scansListTable" class="display" cellspacing="0" width="100%">
			<thead>
			<tr>
				<th>Start Time</th>
				<th>Folder</th>
				<th>Name</th>
				<th>Frequency</th>
				<th>Status</th>
				<th>Enabled</th>
				<th>G Drive Report</th>
			</tr>
			</thead>
			<tfoot>
			<tr>
				<th>Start Time</th>
				<th>Folder</th>
				<th>Name</th>
				<th>Frequency</th>
				<th>Status</th>
				<th>Enabled</th>
				<th>G Drive Report</th>
			</tr>
			</tfoot>
			<tbody>
<?php		
			$scanFolderName = array();
			
			foreach ($scanListObj->folders as $scanFolder) {
				$scanFolderName[$scanFolder->id] = $scanFolder->name;
			}
			
			foreach ($scanListObj->scans as $scanItem) {
				
				$niceDate = new DateTime($scanItem->starttime, new DateTimeZone('America/New_York'));
				
				$scanRRules = explode(";", $scanItem->rrules);
				if (isset($scanRRules[0])) { $scanFrequency = str_replace("FREQ=", "", $scanRRules[0]); }
				if (isset($scanRRules[1])) { $scanInterval = str_replace("INTERVAL=", "", $scanRRules[1]); }
				if (isset($scanRRules[2])) { $scanByDay = str_replace("BYDAY=", "", $scanRRules[2]); }
				
				if ($scanItem->rrules == null) { $niceRules = "On Demand"; }
				else if ($scanFrequency == "ONETIME") { $niceRules = "Once"; }
				else if ($scanFrequency == "DAILY") { $niceRules = "Repeat every ".$scanInterval." day"; }
				else if ($scanFrequency == "WEEKLY") { 
					$niceRules = "Repeat every ".$scanInterval." week"; 
					 if ($scanInterval > 1 ) { $niceRules = $niceRules."s"; }
					$niceRules = $niceRules." on ".$scanByDay;
				}
				else if ($scanFrequency == "MONTHLY") { $niceRules = "Repeat every ".$scanInterval." month"; }
				else if ($scanFrequency == "YEARLY") { $niceRules = "Repeat every ".$scanInterval." year"; }
				else { $niceRules = $scanItem->rrules; }
													 
				if (isset($scanInterval) && $scanInterval > 1 && $scanFrequency != "WEEKLY") { $niceRules = $niceRules."s"; }
				
				if ($scanItem->status == "empty") { $niceStatus = "<span class='red'>Never Run</span>"; }
				else if ($scanItem->status == "completed") { $niceStatus = "<span class='green'>Completed</span>"; }
				else if ($scanItem->status == "running") { $niceStatus = "<span class='blue'>Running</span>"; }
				else { $niceStatus = $scanItem->status; }
				
				if ($scanItem->enabled == 0) { $niceEnabled = "<span class='red'>Disabled</span>"; }
				else if ($scanItem->enabled == 1) { $niceEnabled = "Enabled"; }
				else { $niceEnabled = $scanItem->enabled; }
				
				$reportURL = "No Report";
				
				if ($scanItem->status == "completed") {
				
					$scanID = $scanItem->id;
					
					$scanInfoJSON = nessusAPIQuery("scans/".$scanID, "get", "");
					$scanInfo = json_decode($scanInfoJSON);
					
					$histID = $scanInfo->history[count($scanInfo->history)-1]->history_id;					
					
					$googleReportID = getLatestReportWithGoogleReportID($histID);
					if ($googleReportID != "0") { $reportURL = "<a href='https://drive.google.com/uc?export=download&id=".$googleReportID."' target='_blank'>DL Report</a>"; }
				}
?>
			<tr>
				<td><?php print $niceDate->format('Y-m-d H:i:s T'); ?></td>
				<td><?php print $scanFolderName[$scanItem->folder_id]; ?></td>
				<td><a href="index.php?scanID=<?php print $scanItem->id; ?>"><?php print $scanItem->name; ?></a></td>
				<td><?php print $niceRules; ?></td>
				<td><?php print $niceStatus; ?></td>
				<td><?php print $niceEnabled; ?></td>
				<td><?php print $reportURL; ?></td>
			</tr>
<?php
			}
		}
		else {
?>
				No scans found in NESSUS. <a href="scansched.php">Schedule one?</a>
<?php
		}
?>
			</tbody>
		</table>
		</form>
		<br/>
		<br/>
		</div> <!-- end formFrame DIV -->
<?php
	} // NO SCANID SET END
?>

		<div id="responseFrame" style="display:none;">
			<div id="response"><img src='./images/loading16x16.gif' /> Awaiting response from Nessus, please wait...</div>
			<br/>
			<br/>
		</div>
				
		<div id="feedback"></div>
				
			</div> 					<!-- container class end -->
	</div>
</div> 					<!-- maincontent end -->
			
</body>
</html>