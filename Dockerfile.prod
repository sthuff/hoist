FROM centos:centos6

MAINTAINER Steve Huff <sthuff@vt.edu>

RUN rpm --rebuilddb \
	&& rpm -Uvh http://mirror.webtatic.com/yum/el6/latest.rpm \
	&& yum --setopt=tsflags=nodocs -y install \
		httpd \
	&& yum remove php-common \
	&& yum --setopt=tsflags=nodocs -y install \
		php56w php56w-mysql php56w-common php56w-pdo php56w-opcache \
		links \
	&& rm -rf /var/cache/yum/* \
	&& yum clean all

EXPOSE 8444

# sed example replace
# sed -i 's/old_string/new_string/g'

RUN sed -i \
	-e 's/Listen 80/Listen 8444/g' \
	-e 's/ServerTokens OS/ServerTokens Prod/g' \
	-e 's,LoadModule auth_basic_module,#LoadModule auth_basic_module,g' \
	-e 's,LoadModule auth_digest_module,#LoadModule auth_digest_module,g' \
	-e 's,LoadModule authn_file_module,#LoadModule authn_file_module,g' \
	-e 's,LoadModule authn_alias_module,#LoadModule authn_alias_module,g' \
	-e 's,LoadModule authn_anon_module,#LoadModule authn_anon_module,g' \
	-e 's,LoadModule authn_dbm_module,#LoadModule authn_dbm_module,g' \
	-e 's,LoadModule authn_default_module,#LoadModule authn_default_module,g' \
#	-e 's,LoadModule authz_host_module,#LoadModule authz_host_module,g' \ # REQUIRED
	-e 's,LoadModule authz_user_module,#LoadModule authz_user_module,g' \
	-e 's,LoadModule authz_owner_module,#LoadModule authz_owner_module,g' \
	-e 's,LoadModule authz_groupfile_module,#LoadModule authz_groupfile_module,g' \
	-e 's,LoadModule authz_dbm_module,#LoadModule authz_dbm_module,g' \
	-e 's,LoadModule authz_default_module,#LoadModule authz_default_module,g' \
	-e 's,LoadModule ldap_module,#LoadModule ldap_module,g' \
	-e 's,LoadModule authnz_ldap_module,#LoadModule authnz_ldap_module,g' \
	-e 's,LoadModule include_module,#LoadModule include_module,g' \
#	-e 's,LoadModule log_config_module,#LoadModule log_config_module,g' \ # REQUIRED
	-e 's,LoadModule logio_module,#LoadModule logio_module,g' \
	-e 's,LoadModule env_module,#LoadModule env_module,g' \
	-e 's,LoadModule ext_filter_module,#LoadModule ext_filter_module,g' \
	-e 's,LoadModule mime_magic_module,#LoadModule mime_magic_module,g' \
	-e 's,LoadModule expires_module,#LoadModule expires_module,g' \
	-e 's,LoadModule deflate_module,#LoadModule deflate_module,g' \
	-e 's,LoadModule headers_module,#LoadModule headers_module,g' \
	-e 's,LoadModule usertrack_module,#LoadModule usertrack_module,g' \
#	-e 's,LoadModule setenvif_module,#LoadModule setenvif_module,g' \ # REQUIRED
#	-e 's,LoadModule dav_module,#LoadModule dav_module,g' \ # REQUIRED
#	-e 's,LoadModule status_module,#LoadModule status_module,g' \  # REQUIRED
#	-e 's,LoadModule autoindex_module,#LoadModule autoindex_module,g' \ # REQUIRED
	-e 's,LoadModule info_module,#LoadModule info_module,g' \
	-e 's,LoadModule dav_fs_module,#LoadModule dav_fs_module,g' \
	-e 's,LoadModule vhost_alias_module,#LoadModule vhost_alias_module,g' \
#	-e 's,LoadModule negotiation_module,#LoadModule negotiation_module,g' \ # REQUIRED
	-e 's,LoadModule actions_module,#LoadModule actions_module,g' \
	-e 's,LoadModule speling_module,#LoadModule speling_module,g' \
	-e 's,LoadModule userdir_module,#LoadModule userdir_module,g' \
#	-e 's,LoadModule alias_module,#LoadModule alias_module,g' \ # REQUIRED
	-e 's,LoadModule substitute_module,#LoadModule substitute_module,g' \
	-e 's,LoadModule proxy_module,#LoadModule proxy_module,g' \ 
	-e 's,LoadModule proxy_balancer_module,#LoadModule proxy_balancer_module,g' \
	-e 's,LoadModule proxy_ftp_module,#LoadModule proxy_ftp_module,g' \
	-e 's,LoadModule proxy_http_module,#LoadModule proxy_http_module,g' \
	-e 's,LoadModule proxy_ajp_module,#LoadModule proxy_ajp_module,g' \
	-e 's,LoadModule proxy_connect_module,#LoadModule proxy_connect_module,g' \
	-e 's,LoadModule cache_module,#LoadModule cache_module,g' \
	-e 's,LoadModule suexec_module,#LoadModule suexec_module,g' \
	-e 's,LoadModule disk_cache_module,#LoadModule disk_cache_module,g' \
	-e 's,LoadModule cgi_module,#LoadModule cgi_module,g' \
	-e 's,LoadModule version_module,#LoadModule version_module,g' \
	-e 's/Options Indexes FollowSymLinks/Options -Indexes FollowSymLinks/g' \
	-e 's/ErrorLog logs\/error_log/ErrorLog \/home\/sthuff\/logs\/error_log_hoist/g' \
	-e 's/CustomLog logs\/access_log combined/CustomLog \/home\/sthuff\/logs\/access_log_hoist combined/g' \
	-e 's/PidFile run\/httpd.pid/PidFile \/home\/sthuff\/run\/httpd.pid/g' \	
	/etc/httpd/conf/httpd.conf

RUN useradd --uid 527 --groups apache -s /sbin/nologin sthuff

# attaching a volume to the host risked leaving DEAD docker containers so...
# copy HOIST project and set permissions

COPY hoist /var/www/html/hoist
RUN chown sthuff.apache /var/www/html/hoist && chown sthuff.apache /var/www/html/hoist/* -R 
RUN chmod 644 /var/www/html/hoist/*.php -R && chmod 644 /var/www/html/hoist/css/*.css -R && chmod 644 /var/www/html/hoist/js/*.js -R 
RUN chmod 640 /var/www/html/hoist/assets/api_info.inc.php && chmod 640 /var/www/html/hoist/assets/db_info.inc.php && chmod 640 /var/www/html/hoist/assets/sn_info.inc.php && chmod 640 /var/www/html/hoist/assets/itso-hoist-dd5c14498638.json

USER sthuff

RUN mkdir -p /home/sthuff/logs
RUN mkdir -p /home/sthuff/run

# Current PROD Docker Build Command:
# docker build -t sthuff/hoist -f ./hoist/Dockerfile.prod .

# Current PROD Docker Run Command:
# docker run -tid --name hoist -p 127.0.0.1:8555:8444 sthuff/hoist 

ENTRYPOINT ["/bin/bash", "-c", "/var/www/html/hoist/assets/run_hoist.sh"]
