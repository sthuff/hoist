<?php 

/****************************************************************************

	The HOIST automates Nessus scans and reporting features for the ITSO.

****************************************************************************/
/****************************************************************************

	navbar.php
	
	Included by index.php
	
	Provides a navigation menu to users based on their access level.
	
****************************************************************************/

	if ($_SERVER["SERVER_NAME"] == "localhost") { $_SERVER["HTTP_CAS_UUPID"] = "localUser"; }
?>
		<div id="navstatus"><a href="./index.php">Scans &amp; Reports</a></div>
		<div id="navjobs"><a href="./scanjobs.php">Job Queue</a></div>
		<div id="navsched"><a href="./scansched.php">Schedule Scan</a></div>

<?php
		if ($userPermissions == "dbreadwrite") {
/*
?>
		<div id="navlogs"><a href="./index.php">Scan Logs</a></div>
<?php
*/
		}			
?>
		<div id="navlogout"><?php print $_SERVER["HTTP_CAS_UUPID"]; ?> (<a href="logout.php">Logout</a>)</div>
					
<?php

// EOF

?>