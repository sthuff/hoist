<?php 

/****************************************************************************

	The HOIST automates Nessus scans and reporting features for the ITSO.

****************************************************************************/
/****************************************************************************

	meta_data.inc.php
	
	Included by index.php
	
	Includes the meta data tags for the site.

****************************************************************************/

?>
	<!-- Meta Tags -->
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="robots" content="noindex, nofollow" />
		<meta name="keywords" content="HOIST Hands-Off ITSO Scanning Tool">
		<meta name="description" content="HOIST Hands-Off ITSO Scanning Tool">
<?php 

// EOF

?>