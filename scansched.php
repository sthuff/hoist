<?php
/****************************************************************************

	The HOIST automates Nessus scans and reporting features for the ITSO.

****************************************************************************/
/****************************************************************************

	Docs and Details pending.	

****************************************************************************/

	include_once("initial_config.inc.php");
	include_once("doctype.inc.php");
?>
<html>
<head>
	<title>HOIST - Hands-Off ITSO Scanning Tool</title>
<?php
	include_once("master_css.inc.php");
	include_once("meta_data.inc.php");
	include_once("javascripts.inc.php");
?>

</head>
<body>
	
<div id="header">			<!-- header -->
	<div class="bg">
		<div class="container"> 	<!-- container -->
				<div class="title"></div>
				<div class="logo"></div>
				<div class="content">&nbsp;</div>
				<div class="navbar">
<?php
					include_once("navbar.php");					
?>
				</div>
				<div class="clear"></div>
		</div> 				<!-- container end -->
	</div>
</div> 					<!-- header end -->

<div id="maincontent"> <!-- maincontent -->
		<div class="bg">
			<div class="navbarbg"></div>
			<div class="container">
<?php
	include_once("./assets/nessus_api_funcs.inc.php");
	
	$time10pmEasternTZ = new DateTime('today 22:00:00', new DateTimeZone('America/New_York'));
	$nicePrefTime = $time10pmEasternTZ->format('Y-m-d H:i:s O');
	$nicePrefTimeText = "Not pulling data from SN";
	$ioResultList["namePID"] = $casUser;
	$ioResultList["departmentName"] = "General_Scans";
	$ioResultList["otherNotes"] = "";
	$ioResultList["scanTargetIPs"] = "";
	$scanRecipientString = "";
	$scanRecipientFullString = "";
	$scanName = "";
	$jobID = "";
				
	if (isset($_GET["jobID"])) {
				
		// include the Service-Now API functions
		include_once("./assets/servicenow_funcs.php");
		
		// Trim and sanitize posted variables...
		$jobID = trim($_GET["jobID"]);
		$jobID = filter_var($jobID, FILTER_SANITIZE_STRING);

		// include the DB functions
		include_once("./assets/hoist_db_funcs.inc.php");
		$scanRITMScheduled = checkRITMScanScheduled($jobID);

		$sRITMData = explode("||", $scanRITMScheduled);
		if ($sRITMData[0] == 1) {
			$ritmURL = "https://vt4help".$modeURL.".service-now.com/sc_req_item.do?sysparm_query=number=".$jobID."&sysparm_view=ess&sysparm_record_target=sc_req_item";
?>
			Scan for <a href="<?php print $ritmURL; ?>"><?php print $jobID; ?></a> has already been scheduled. <br/><br/>
			<a href="index.php?scanID=<?php print $sRITMData[1]; ?>">Check its status and details</a>.
<?php
			exit;
		}

		$ioResultList = getRITMData($jobID, $casUser, $modeURL);
		
		$ioResultList["departmentName"] = trim($ioResultList["departmentName"]);
		$ioResultList["departmentName"] = preg_replace("([^\w\s\d\-_\[\].])", '', $ioResultList["departmentName"]);
		$ioResultList["departmentName"] = preg_replace("([\.]{2,})", '', $ioResultList["departmentName"]);
		$ioResultList["departmentName"] = str_replace(" ", "_", $ioResultList["departmentName"]);
		
		$scanName = trim($jobID."-".$ioResultList["departmentName"]."-".trim($ioResultList["namePID"]));
		
		$nicePrefTimeText = "";
		
		if ($ioResultList["preferredTime"] == "after_hours") {
			$nicePrefTimeText = "After Hours";
			$time10pmEasternTZ = new DateTime('today 22:00:00', new DateTimeZone('America/New_York'));
			$nicePrefTime = $time10pmEasternTZ->format('Y-m-d H:i:s O');
		} else if ($ioResultList["preferredTime"] == "business") {
			$nicePrefTimeText = "Business Hours";
			$time12pmEasternTZ = new DateTime('today 12:00:00', new DateTimeZone('America/New_York'));
			$nicePrefTime = $time12pmEasternTZ->format('Y-m-d H:i:s O');
		} else {
			$nicePrefTimeText = "Anytime";
			$time11pmEasternTZ = new DateTime('today 23:00:00', new DateTimeZone('America/New_York'));
			$nicePrefTime = $time11pmEasternTZ->format('Y-m-d H:i:s O');
		}
			
		if (isset($ioResultList["scanAccess"][0])) {
			foreach ($ioResultList["scanAccess"] as $scanRecipient) {
				$scanRecipientString .= $scanRecipient["pid"]."@vt.edu,";
				
				$scanRecipientFullString .= $scanRecipient["name"]." (".$scanRecipient["pid"]."@vt.edu), ";
			}
			// remove last comma
			$scanRecipientString = substr($scanRecipientString, 0, -1);
			$scanRecipientFullString = substr($scanRecipientFullString, 0, -2);
		}		

	}
?>			
			
			
		<!-- form start -->  

		<div id="formFrame">
			<p>
			<br/>
<?php
	// force DEV mode if we're on a dev instance...
	if ($_SERVER["SERVER_NAME"] == "localhost" || strstr($_SERVER["REQUEST_URI"], "/hoistdev")) { 
		print "<strong>NOTICE: This is the development version of HOIST.</strong><br/><br/>";
		$mode = "DEV"; 
	}
?>
			Select the scan type and set the schedule for this scan to run.  Then, set the desired options for the Scan Report.<br/>			
			<br/>
			<br/>
			</p>
			<h2>[ Scan Options ]</h2>
						
		<form>
			<input type="hidden" id="casUser" name="casUser" value="<?php print $casUser; ?>" />
			<input type="hidden" id="userPermissions" name="userPermissions" value="<?php print $userPermissions; ?>" />
			<input type="hidden" id="scanCreator" name="scanCreator" value="<?php print $casUser; ?>" />
			<input type="hidden" id="snRITM" name="snRITM" value="<?php print $jobID; ?>" />
			
		<table border="0" class="scanFormTable">
		<tr>
			<td width="200px">Scan Type: <br/></td>
			<td>
				<select id="scanID" name="scanID">
<?php
		$action = "policies"; 

		//print "<br/>DEBUG: API action is: ".$action."<br/>";

		$policies = nessusAPIQuery($action, "get", "");
		$policiesObj = json_decode($policies);
		$policiesList = $policiesObj->policies;

		// ITSO Full Scan is id 5
		foreach ($policiesList as $key => $tpl) {
			
			if ($tpl->id == 5) { // default to ITSO Full Scan
?>
				<option value="<?php print $tpl->id."||".$tpl->template_uuid; ?>" selected="selected"><?php print $tpl->name; ?></option>
<?php
			}
			else {
?>
				<option value="<?php print $tpl->id."||".$tpl->template_uuid; ?>"><?php print $tpl->name; ?></option>
<?php
			}
		}				
?>
				</select>
			</td>		
			<td width="200px">Preferred Scan Window:</td>
			<td>
				<?php print $nicePrefTimeText; ?>
			</td>
		</tr>
		<tr>
			<td>Scheduled or Immediate?</td>
			<td>
				<select id="scanScheduled" name="scanScheduled">
					<option value="sched" selected="selected"> Scheduled
					<option value="now"> Scan Once, Now
				</select>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				Enabled? <input type="checkbox" id="scanEnabled" name="scanEnabled" value="true" checked="checked">
			</td>
			<td>Scan Start Date:</td>
			<td>
					<input type="text" name="dateTimeSchedScan" id="dateTimeSchedScan" value="<?php print $nicePrefTime; ?>" size="20" />
					<input type="button" id="nowButton" class="styledButton" value="NOW" />
			</td>
		</tr>
		<tr>
			<td>Scan Frequency</td>
			<td>
					<select id="scanFrequency" name="scanFrequency">
						<option value="ONCE" selected="selected"> Once
						<option value="DAILY"> Daily
						<option value="WEEKLY"> Weekly
						<option value="MONTHLY"> Monthly
						<option value="YEARLY"> Yearly
					</select><br/>
					<div id="scanWeeklyBoxes">
						<input type="checkbox" id="scanWeeklySU" name="BYDAY" value="SU" /><label for="scanWeeklySU"> SU</label>
						<input type="checkbox" id="scanWeeklyMO" name="BYDAY" value="MO" checked="checked" /><label for="scanWeeklyMO"> MO</label>
						<input type="checkbox" id="scanWeeklyTU" name="BYDAY" value="TU" /><label for="scanWeeklyTU"> TU</label>
						<input type="checkbox" id="scanWeeklyWE" name="BYDAY" value="WE" /><label for="scanWeeklyWE"> WE</label>
						<input type="checkbox" id="scanWeeklyTH" name="BYDAY" value="TH" /><label for="scanWeeklyTH"> TH</label>
						<input type="checkbox" id="scanWeeklyFR" name="BYDAY" value="FR" /><label for="scanWeeklyFR"> FR</label>
						<input type="checkbox" id="scanWeeklySA" name="BYDAY" value="SA" /><label for="scanWeeklySA"> SA</label>
					</div>
			</td>
			<td>Scan Interval:</td>
			<td>
				<input type="text" id="scanInterval" name="scanInterval" value="1" size="3" /> <span style="font-size: 10px; padding-left: 5px;">(MAX 20)</span><br/>		
				<span id="scanIntervalText">Scan will <strong>not</strong> repeat<br/></span>
			</td>
		</tr>
		<tr>
			<td width="200px">Scan Name:</td>
			<td>
				<input type="text" name="scanName" id="scanName" value="<?php print $scanName; ?>" size="27" />			
			</td>
			<td>Requestor PID:</td>
			<td>
				<input type="text" id="scanRequestor" name="scanRequestor" value="<?php print $ioResultList["namePID"]; ?>" size="27" />	
			</td>

		</tr>
<?php
	if (isset($_GET["jobID"])) {
?>
		<tr>
			<td width="200px">Scan Authentication:</td>
			<td>
<?php
				if ($ioResultList["scanIsUnauth"] == "true") { print "Unauthenticated"; }
				if ($ioResultList["scanIsUnauth"] == "true" && $ioResultList["scanIsAuth"] == "true") { print ", "; }
				if ($ioResultList["scanIsAuth"] == "true") { print "Authenticated"; }
?>
				<input type="hidden" id="scanIsUnauth" name="scanIsUnauth" value="<?php print $ioResultList["scanIsUnauth"]; ?>" />
				<input type="hidden" id="scanIsAuth" name="scanIsAuth" value="<?php print $ioResultList["scanIsAuth"]; ?>" />
			</td>
			<td>
				Host or Application:<br/>
				<br/>
				Recurrence:<br/>
			</td>
			<td>
				<?php print $ioResultList["scanHostApp"]; ?>
				<input type="hidden" id="scanHostApp" name="scanHostApp" value="<?php print $ioResultList["scanHostApp"]; ?>" /> <br/>
				<br/>
<?php
		if ($ioResultList["scanRecurrence"]	== "true") {
?>
				<strong>Scan should repeat</strong>.  <br/>
				Please contact the requestor for scheduling details before submitting.
<?php
		} else {
?>
				No recurrence
<?php
		}
?>
			</td>
		</tr>		
<?php
	}
?>
		<tr>
			<td colspan="4">
<?php
	if (!isset($_GET["jobID"])) {
?>
				Service-Now Integration: <br/>
				<div class="otherNotesFrame">
					No Requested Item (RITM) exists for this scan in Service-Now.  Once the scan is submitted, a Service-Now RITM will be created 
					and the APPROVAL step will be auto-completed so that Service-Now's email and tracking capabilities can be used. <br/>
					<br/>
					<input type="hidden" id="preferredTime" value="Anytime" />
					
					Perform scan Unauthenticated or scan as an Authenticated User? 
					<label><input type="checkbox" id="scanIsUnauth" value="scanisunauth" checked="checked" /></label> Unauthenticated,
					<label><input type="checkbox" id="scanIsAuth" value="scansauth" /></label> Authenticated <br/>
					<br/>
					Scan a Host or Host and Application? 
					<select id="scanHostApp" name="scanHostApp">
						<option value="host" selected="selected"> Host
						<option value="application"> Application
					</select> <br/>
					<br/>
					Schedule Scan to Repeat? 
					<label><input type="checkbox" id="scanRecurrence" value="false" /></label> Recurrence <br/>
					<br/>
					Other Scan Notes: <br/>
					<textarea id="otherNotes" name="otherNotes" rows="2" cols="126"></textarea>
				</div>
<?php
	} else {
?>				
				Notes from Scan Requestor: <br/>
				<div class="otherNotesFrame"><?php print $ioResultList["otherNotes"]; ?></div>
				<input type="hidden" id="otherNotes" name="otherNotes" value="<?php print $ioResultList["otherNotes"]; ?>" />				
<?php
	}
?>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				Scan Targets: Should be CIDR Notation or Individual IPs in a comma-separated list. <br/>
<?php
				$scanTargetIPString = "";
				$ioResultList["scanTargetIPs"] = trim($ioResultList["scanTargetIPs"]);
				$scanTargetIPsList = explode(",", $ioResultList["scanTargetIPs"]);
		
				$malformedIPString = 0;
		
				foreach ($scanTargetIPsList as $scanTargetIP) {
					
					//print "<br/>DEBUG: ScanTargetIP is: ".$scanTargetIP."<br/>";
					
					$scanTargetIP = trim(str_replace(' ', '', $scanTargetIP));
					$scanTargetIPValid = filter_var($scanTargetIP, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE);

					if ($scanTargetIPValid !== FALSE) { 
						$scanTargetIPString .= $scanTargetIPValid.", "; 
					}
					else {
						// check for a hyphenated range
						$scanTargetIPRange = explode("-", $scanTargetIP); 

						//print "<br/>DEBUG: RangeObj is: |";
						//print_r($scanTargetIPRange)."|<br/>";

						$scanTargetIPStart = trim($scanTargetIPRange[0]);
						
						//print "<br/>DEBUG: ScanTargetIPStart is: |".$scanTargetIPStart."|<br/>";

						$scanTargetIPStart = filter_var($scanTargetIPStart, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE);

						if ($scanTargetIPStart !== FALSE) {
							
							$scanTargetIPEnd = trim($scanTargetIPRange[1]);
							
							//print "<br/>DEBUG: ScanTargetIPEnd is: |".$scanTargetIPEnd."|<br/>";
							
							$scanTargetIPEnd = filter_var($scanTargetIPEnd, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE);
							
							if ($scanTargetIPEnd !== FALSE) {
								$scanTargetIPString .= $scanTargetIPStart."-".$scanTargetIPEnd.", "; 
							
							}
						} 
						else {
							
							//print "<br/>DEBUG: ScanTargetIPCIDR is: |".$scanTargetIP."|<br/>";
							
							// check for CIDR notation
							$scanTargetIPCIDR = explode("/", $scanTargetIP); 

							$scanTargetIPCIDRNet = trim($scanTargetIPCIDR[0]);
							$scanTargetIPCIDRNet = filter_var($scanTargetIPCIDRNet, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE);

							if ($scanTargetIPCIDRNet !== FALSE) {

								$scanTargetIPCIDRMask = trim($scanTargetIPCIDR[1]);
								$scanTargetIPCIDRMask = filter_var($scanTargetIPCIDRMask, FILTER_VALIDATE_INT);

								if ($scanTargetIPCIDRMask !== FALSE) {
									$scanTargetIPString .= $scanTargetIPCIDRNet."/".$scanTargetIPCIDRMask.", ";
								} 
								else {
									$malformedIPString = 1; break; 
								}
							}
						}
					}
				}
		
				// if we have malformed IPs, just display the unaltered string and a warning
		
				if ($malformedIPString == 1) { 
					$scanTargetIPString = $ioResultList["scanTargetIPs"]; 
?>
					<span class="red">WARNING: Please validate the Scan Target IPs.  Allowed formats are any mix of a hyphenated IP range, CIDR notation or a set of individual IP addresses separated by commas.</span>
<?php
				} 
				else { 
					
					if (count($scanTargetIPString) > 6) {
						// shortest possible IP address is 7 chars long (e.g. 8.8.8.8)...
						$scanTargetIPString = substr($scanTargetIPString, 0, -2); 
					}
					else {
						$scanTargetIPString = $ioResultList["scanTargetIPs"]; 
					}
				}				
?>
				
				<textarea id="scanTargetIPs" name="scanTargetIPs" rows="5" cols="101"><?php print $scanTargetIPString; ?></textarea>
				<input type="hidden" id="scanTargetIPsOrig" value="<?php print $scanTargetIPString; ?>" />				
			</td>
			<td>
				<input type="button" id="resetTargetIPs" class="styledButton" value="Reset Target IPs" style="margin-left: 84px; top: 70px;" />
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<h2>[ Report Options ]</h2>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				Scan Report Recipients: Should be a comma-separated list of <em>full email addresses</em> <br/>
				<textarea id="scanRecipients" name="scanRecipients" rows="2" cols="101" readonly="readonly"><?php print $scanRecipientString; ?></textarea> 
				<input type="hidden" id="scanRecipientsOrig" value="<?php print $scanRecipientString; ?>" />
				<input type="hidden" id="scanRecipientsFullOrig" value="<?php print $scanRecipientFullString; ?>" />
			</td>
			<td>				
				<input type="button" id="resetRecipients" class="styledButton" value="Reset Recipients" style="margin-left: 84px; top: 45px; display: none;" />
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<label><input type="checkbox" id="setFolderByPID" value="autosetfolder" checked="checked" /></label>
				Auto-set Nessus/Google Drive Folder Destination based on Requestor PID's Department lookup in Service-Now? 
				<br/>
				<label><input type="checkbox" id="scanAutoExport" value="autoexportreport" checked="checked" /></label>
				Auto-export report to Google Drive once scan is complete? 
				<br/>
				<label><input type="checkbox" id="scanAutoNotify" value="autonotifyreport" checked="checked" /></label>				
				Auto-notify the report recipients of the Google Drive link through Service-Now once scan is complete? 
			</td>
		</tr>
		<tr>
			<td>Nessus/Google Folder Destination:</td>
			<td>
				<input type="text" id="googleFolderName" name="googleFolderName" value="<?php print $ioResultList["departmentName"]; ?>" size="27" />
				<input type="hidden" id="departmentSysID" name="departmentSysID" value="" />
				<input type="hidden" id="scanRequestorSysID" name="scanRequestorSysID" value="" />
				<input type="hidden" id="casUserSysID" name="casUserSysID" value="" />
				
			</td>
			<td>Report Format:</td>
			<td>
				<select id="reportFormat" name="reportFormat">
					<option value="html" selected="selected"> HTML
					<option value="csv"> CSV (Excel)
					<option value="nessus"> Nessus
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<div id="feedback" class="feedbackFrame">&nbsp;</div>
			</td>
			<td>
				<div style="position:relative; top: 15px; margin-right: 80px;"><input type="button" id="submitScan" value="Submit Scan" style="width: 217px;" /> </div>
			</td>
		</tr>
		</table>
		</form>
		<br/>
		<br/>
		</div> <!-- end formFrame DIV -->
			
		<div id="responseFrame" style="display:none;">
			<div id="response"><img src='./images/loading16x16.gif' /> Awaiting response from Nessus, please wait...</div>
			<br/>
			<br/>
			<a href="./scansched.php">Submit Another Scan to Nessus</a><br/>
			<br/>
			<br/>			
			<a href="./index.php">Review Scans &amp; Reports</a><br/>
		</div>

		</div> 					<!-- container class end -->
	</div>
</div> 					<!-- maincontent end -->
			
</body>
</html>