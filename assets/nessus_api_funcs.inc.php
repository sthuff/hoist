<?php

/****************************************************************************

	The HOIST automates Nessus scans and reporting features for the ITSO.

****************************************************************************/
/****************************************************************************

	api_funcs_inc.php
	
	Included by index.php
	
	This script holds functions called from index.php and performs 
	REST API calls to Nessus
	
****************************************************************************/

$mode = "LIVE";
$modeURL = "";

	// PHP ERROR REPORTING LEVEL
	error_reporting(E_ALL); 
	ini_set('display_errors', '1');

	// SERVER TIMEZONE REQUIRED FOR DATE FUNCTIONS
	date_default_timezone_set('UTC');

// force DEV mode if we're on a dev instance... 
if (isset($_SERVER["SERVER_NAME"])) {
	if ($_SERVER["SERVER_NAME"] == "localhost") { $mode = "DEV"; $modeURL = "dev"; }
}

if (isset($_SERVER["REQUEST_URI"])) {
	if (strstr($_SERVER["REQUEST_URI"], "/hoistdev")) { $mode = "DEV"; $modeURL = "dev"; }
}


// Handle a scan submission...


if (isset($_POST["scanID"])) {

/****************************************************************************
	
	REQUESTING REPORT EXPORT

****************************************************************************/
	if (isset($_POST["formAction"]) && $_POST["formAction"] == "genreport") {

		$scanDBInfo["scanID"] = trim($_POST["scanID"]);
		$scanDBInfo["scanID"] = filter_var($scanDBInfo["scanID"], FILTER_SANITIZE_NUMBER_INT);
		
		$scanDBInfo["histID"] = trim($_POST["histID"]);
		$scanDBInfo["histID"] = filter_var($scanDBInfo["histID"], FILTER_SANITIZE_NUMBER_INT);

		$scanDBInfo["snRITM"] = trim($_POST["snRITM"]);
		$scanDBInfo["snRITM"] = filter_var($scanDBInfo["snRITM"], FILTER_SANITIZE_STRING);
		
		$scanDBInfo["reportType"] = trim($_POST["reportType"]);
		$scanDBInfo["reportType"] = filter_var($scanDBInfo["reportType"], FILTER_SANITIZE_STRING);

		$scanDBInfo["scanReportGoogle"] = trim($_POST["scanReportGoogle"]);
		$scanDBInfo["scanReportGoogle"] = filter_var($scanDBInfo["scanReportGoogle"], FILTER_SANITIZE_NUMBER_INT);		

		$scanDBInfo["scanName"] = trim($_POST["scanName"]);
		$scanDBInfo["scanName"] = filter_var($scanDBInfo["scanName"], FILTER_SANITIZE_STRING);
		
		$scanDBInfo["scanStartDate"] = trim($_POST["scanStartDate"]);
		$scanDBInfo["scanStartDate"] = filter_var($scanDBInfo["scanStartDate"], FILTER_SANITIZE_STRING);

		$scanDBInfo["scanEndDate"] = trim($_POST["scanEndDate"]);
		$scanDBInfo["scanEndDate"] = filter_var($scanDBInfo["scanEndDate"], FILTER_SANITIZE_STRING);
		
		$scanDBInfo["scanLastModified"] = trim($_POST["scanLastModified"]);
		$scanDBInfo["scanLastModified"] = filter_var($scanDBInfo["scanLastModified"], FILTER_SANITIZE_STRING);
		
		$scanDBInfo["scanCreator"] = trim($_POST["scanCreator"]);
		$scanDBInfo["scanCreator"] = filter_var($scanDBInfo["scanCreator"], FILTER_SANITIZE_STRING);
		
		$scanDBInfo["scanRequestor"] = trim($_POST["scanRequestor"]);
		$scanDBInfo["scanRequestor"] = filter_var($scanDBInfo["scanRequestor"], FILTER_SANITIZE_STRING);
		
		$scanDBInfo["scanRecipients"] = trim($_POST["scanRecipients"]);
		$scanDBInfo["scanRecipients"] = filter_var($scanDBInfo["scanRecipients"], FILTER_SANITIZE_STRING);
		
		$scanDBInfo["scanTargetIPs"] = trim($_POST["scanTargetIPs"]);
		$scanDBInfo["scanTargetIPs"] = filter_var($scanDBInfo["scanTargetIPs"], FILTER_SANITIZE_STRING);
		
		$scanDBInfo["reportFormat"] = trim($_POST["reportFormat"]);
		$scanDBInfo["reportFormat"] = filter_var($scanDBInfo["reportFormat"], FILTER_SANITIZE_STRING);
		
		$scanDBInfo["scanFolderID"] = trim($_POST["scanFolderID"]);
		$scanDBInfo["scanFolderID"] = filter_var($scanDBInfo["scanFolderID"], FILTER_SANITIZE_STRING);
		
		$scanDBInfo["scanFolderName"] = trim($_POST["scanFolderName"]);
		$scanDBInfo["scanFolderName"] = filter_var($scanDBInfo["scanFolderName"], FILTER_SANITIZE_STRING);
		
		$scanDBInfo["googleFolderName"] = trim($_POST["googleFolderName"]);
		$scanDBInfo["googleFolderName"] = filter_var($scanDBInfo["googleFolderName"], FILTER_SANITIZE_STRING);

		//print "Report Type is: ".$scanDBInfo["reportType"]."<br/>";
		// SEE FILTERS DEFINED IN:
		// https://cornflakes.iso.vt.edu:8834/api#/resources/scans/export-request
		// FOR MORE INFO

		$debugMsg = generateNessusReport($scanDBInfo, "manualExport");
	}
	else if (isset($_POST["formAction"]) && $_POST["formAction"] == "updatescandb") {
/****************************************************************************
	
	UPDATING THE SCAN DB

****************************************************************************/		
		
		$scanDBInfo["scanID"] = trim($_POST["scanID"]);
		$scanDBInfo["scanID"] = filter_var($scanDBInfo["scanID"], FILTER_SANITIZE_NUMBER_INT);

		$scanDBInfo["snRITM"] = trim($_POST["snRITM"]);
		$scanDBInfo["snRITM"] = filter_var($scanDBInfo["snRITM"], FILTER_SANITIZE_STRING);

		$scanDBInfo["scanName"] = trim($_POST["scanName"]);
		$scanDBInfo["scanName"] = filter_var($scanDBInfo["scanName"], FILTER_SANITIZE_STRING);
		
		$scanDBInfo["scanStartDate"] = trim($_POST["scanStartDate"]);
		$scanDBInfo["scanStartDate"] = filter_var($scanDBInfo["scanStartDate"], FILTER_SANITIZE_STRING);
		
		$scanDBInfo["scanCreator"] = trim($_POST["scanCreator"]);
		$scanDBInfo["scanCreator"] = filter_var($scanDBInfo["scanCreator"], FILTER_SANITIZE_STRING);
		
		$scanDBInfo["scanRequestor"] = trim($_POST["scanRequestor"]);
		$scanDBInfo["scanRequestor"] = filter_var($scanDBInfo["scanRequestor"], FILTER_SANITIZE_STRING);
		
		$scanDBInfo["scanRecipients"] = trim($_POST["scanRecipients"]);
		$scanDBInfo["scanRecipients"] = filter_var($scanDBInfo["scanRecipients"], FILTER_SANITIZE_STRING);
		
		$scanDBInfo["scanTargetIPs"] = trim($_POST["scanTargetIPs"]);
		$scanDBInfo["scanTargetIPs"] = filter_var($scanDBInfo["scanTargetIPs"], FILTER_SANITIZE_STRING);
		
		$scanDBInfo["scanFolderID"] = trim($_POST["scanFolderID"]);
		$scanDBInfo["scanFolderID"] = filter_var($scanDBInfo["scanFolderID"], FILTER_SANITIZE_STRING);
		
		$scanDBInfo["scanFolderName"] = trim($_POST["scanFolderName"]);
		$scanDBInfo["scanFolderName"] = filter_var($scanDBInfo["scanFolderName"], FILTER_SANITIZE_STRING);
		
		$scanDBInfo["googleFolderName"] = trim($_POST["googleFolderName"]);
		$scanDBInfo["googleFolderName"] = filter_var($scanDBInfo["googleFolderName"], FILTER_SANITIZE_STRING);

		$scanDBInfo["scanNotInDB"] = trim($_POST["scanNotInDB"]);
		$scanDBInfo["scanNotInDB"] = filter_var($scanDBInfo["scanNotInDB"], FILTER_SANITIZE_NUMBER_INT);
		
		$scanDBInfo["scanAutoExport"] = trim($_POST["scanAutoExport"]);
		$scanDBInfo["scanAutoExport"] = filter_var($scanDBInfo["scanAutoExport"], FILTER_SANITIZE_NUMBER_INT);
		
		$scanDBInfo["scanAutoNotify"] = trim($_POST["scanAutoNotify"]);
		$scanDBInfo["scanAutoNotify"] = filter_var($scanDBInfo["scanAutoNotify"], FILTER_SANITIZE_NUMBER_INT);
		
		$scanDBInfo["scanDescriptionString"] = $scanDBInfo["scanRequestor"]."||".$scanDBInfo["googleFolderName"]."||".$scanDBInfo["scanAutoExport"];

		$scanDBInfo["debugMsg"] = "";
		
		include("hoist_db_funcs.inc.php");
		
		if ($scanDBInfo["scanNotInDB"] == 1) {
			$debugMsg = insertMissingScanDetails($scanDBInfo);
		}
		else {
			$debugMsg = updateMissingScanDetails($scanDBInfo);
		}
		print $scanDBInfo["scanID"]."|DATA|".$debugMsg;
	}
	else if (isset($_POST["formAction"]) && $_POST["formAction"] == "submitscan") { 
/****************************************************************************
	
	SUBMIT A SCAN

****************************************************************************/

		// Nessus date format: YYYYMMDDTHHMMSS

		$scanIDParts = explode("||", $_POST["scanID"]);

		// Trim and sanitize posted variables...
		$scanDBInfo["scanID"] = trim($scanIDParts[0]);
		$scanDBInfo["scanID"] = filter_var($scanDBInfo["scanID"], FILTER_SANITIZE_NUMBER_INT);
		
		$scanDBInfo["scanType"] = $modeURL;

		$scanDBInfo["debugMsg"] = "DEBUG: ";
			
		$scanDBInfo["scanUUID"] = trim($scanIDParts[1]);
		$scanDBInfo["scanUUID"] = filter_var($scanDBInfo["scanUUID"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);

		$scanDBInfo["snRITM"] = trim($_POST["snRITM"]);
		$scanDBInfo["snRITM"] = filter_var($scanDBInfo["snRITM"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
		
		$scanDBInfo["scanRequestor"] = trim($_POST["scanRequestor"]);
		$scanDBInfo["scanRequestor"] = filter_var($scanDBInfo["scanRequestor"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
		$scanDBInfo["scanRequestor"] = preg_replace("([^\w\s\d\-_\[\].])", '', $scanDBInfo["scanRequestor"]);
		$scanDBInfo["scanRequestor"] = preg_replace("([\.]{2,})", '', $scanDBInfo["scanRequestor"]);
		$scanDBInfo["scanRequestor"] = str_replace(" ", "_", $scanDBInfo["scanRequestor"]);
		
		$scanDBInfo["scanCreator"] = trim($_POST["scanCreator"]);
		$scanDBInfo["scanCreator"] = filter_var($scanDBInfo["scanCreator"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);

		$scanDBInfo["scanScheduled"] = trim($_POST["scanScheduled"]);
		$scanDBInfo["scanScheduled"] = filter_var($scanDBInfo["scanScheduled"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);

		$scanDBInfo["scanEnabled"] = trim($_POST["scanEnabled"]);
		$scanDBInfo["scanEnabled"] = filter_var($scanDBInfo["scanEnabled"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
		if ($scanDBInfo["scanEnabled"] == "true") { $scanDBInfo["scanEnabled"] = TRUE; } else { $scanDBInfo["scanEnabled"] = FALSE; }

		$scanDBInfo["dateTimeSchedScan"] = trim($_POST["dateTimeSchedScan"]);
		$scanDBInfo["dateTimeSchedScan"] = filter_var($scanDBInfo["dateTimeSchedScan"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
		
		$scanDBInfo["scanHostApp"] = trim($_POST["scanHostApp"]);
		$scanDBInfo["scanHostApp"] = filter_var($scanDBInfo["scanHostApp"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
		
		$scanDBInfo["scanIsUnauth"] = trim($_POST["scanIsUnauth"]);
		$scanDBInfo["scanIsUnauth"] = filter_var($scanDBInfo["scanIsUnauth"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
		
		$scanDBInfo["scanIsAuth"] = trim($_POST["scanIsAuth"]);
		$scanDBInfo["scanIsAuth"] = filter_var($scanDBInfo["scanIsAuth"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
		
		$scanDBInfo["reportFormat"] = trim($_POST["reportFormat"]);
		$scanDBInfo["reportFormat"] = filter_var($scanDBInfo["reportFormat"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
		
		$scanDBInfo["scanFrequency"] = trim($_POST["scanFrequency"]);
		$scanDBInfo["scanFrequency"] = filter_var($scanDBInfo["scanFrequency"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
		
		$scanDBInfo["scanRRules"] = trim($_POST["scanRRules"]);
		$scanDBInfo["scanRRules"] = filter_var($scanDBInfo["scanRRules"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
			
		// if a scan is set to NOW (Immediate) we want to set its timing to now +1 minute so it is guaranteed to run
		if ($scanDBInfo["scanScheduled"] == "now") { 	$scanDBInfo["dateTimeSchedScanFormatObj"] = new DateTime('NOW', new DateTimeZone('America/New_York')); $scanDBInfo["dateTimeSchedScanFormatObj"]->add(new DateInterval('PT1M')); }
		else { $scanDBInfo["dateTimeSchedScanFormatObj"] = new DateTime($scanDBInfo["dateTimeSchedScan"], new DateTimeZone('America/New_York')); }

		// if a scan is set to run ONCE but the time is less than (before) now, the scan will never run, As a workaround, update it to now +1 minute
		if ($scanDBInfo["scanFrequency"] == "ONCE") {
			$dateNow = new DateTime('NOW', new DateTimeZone('America/New_York'));
			$scanDBInfo["dateTimeSchedScanFormatObj"] = new DateTime($scanDBInfo["dateTimeSchedScan"], new DateTimeZone('America/New_York'));
			
			if ($scanDBInfo["dateTimeSchedScanFormatObj"] < $dateNow) { 
				$scanDBInfo["dateTimeSchedScanFormatObj"] = $dateNow; 
				$scanDBInfo["dateTimeSchedScanFormatObj"]->add(new DateInterval('PT1M')); 
			}
		}

		$dateTimeSchedScanFormat = $scanDBInfo["dateTimeSchedScanFormatObj"]->format('Ymd\THis');

		$scanDBInfo["scanName"] = trim($_POST["scanName"]);
		$scanDBInfo["scanName"] = filter_var($scanDBInfo["scanName"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
		$scanDBInfo["scanName"] = preg_replace("([^\w\s\d\-_\[\].])", '', $scanDBInfo["scanName"]);
		$scanDBInfo["scanName"] = preg_replace("([\.]{2,})", '', $scanDBInfo["scanName"]);
		$scanDBInfo["scanName"] = str_replace(" ", "_", $scanDBInfo["scanName"]);
		
		$scanDBInfo["scanTargetIPs"] = trim($_POST["scanTargetIPs"]);
		$scanDBInfo["scanTargetIPs"] = filter_var($scanDBInfo["scanTargetIPs"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
		
		$scanDBInfo["scanRecipients"] = trim($_POST["scanRecipients"]);
		$scanDBInfo["scanRecipients"] = filter_var($scanDBInfo["scanRecipients"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
		
		$scanDBInfo["googleFolderName"] = trim($_POST["googleFolderName"]);
		$scanDBInfo["googleFolderName"] = filter_var($scanDBInfo["googleFolderName"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
		$scanDBInfo["googleFolderName"] = preg_replace("([^\w\s\d\-_\[\].])", '', $scanDBInfo["googleFolderName"]);
		$scanDBInfo["googleFolderName"] = preg_replace("([\.]{2,})", '', $scanDBInfo["googleFolderName"]);
		$scanDBInfo["googleFolderName"] = str_replace(" ", "_", $scanDBInfo["googleFolderName"]);
		
		
		$scanDBInfo["scanAutoExport"] = trim($_POST["scanAutoExport"]);
		$scanDBInfo["scanAutoExport"] = filter_var($scanDBInfo["scanAutoExport"], FILTER_SANITIZE_NUMBER_INT);
		
		$scanDBInfo["scanAutoNotify"] = trim($_POST["scanAutoNotify"]);
		$scanDBInfo["scanAutoNotify"] = filter_var($scanDBInfo["scanAutoNotify"], FILTER_SANITIZE_NUMBER_INT);

		$scanDBInfo["scanFolderID"] = "";
		$folderExists = 0;
		
		$folderData["name"] = $scanDBInfo["googleFolderName"];
		$jsonData = "";
		$action = "folders";

		$getFolder = nessusAPIQuery($action, "get", $jsonData);
		$getFolderObj = json_decode($getFolder);
		
//		print "<br/>DEBUG: getFolderObj Object: <br/>";
//		var_dump($getFolderObj);
//		print "<br/>";

		foreach ($getFolderObj->folders as $folder) {
			
			// Nessus has a 20 char limit on folder names... UGGGGHHHHHH
			if ($folder->name == trim(substr($scanDBInfo["googleFolderName"], 0, 20))) {
				$scanDBInfo["scanFolderID"] = $folder->id;
				$scanDBInfo["scanFolderName"] = $folder->name;
				$folderExists = 1;
//				print "<br/>FOLDER FOUND<br/>";
				break;
			}
//				print "<br/>DEBUG: Folder is: ".$folder->id.", ".$folder->name."<br/>";
		}

		if ($folderExists == 0) {
			
			// Nessus has a 20 char limit on folder names... UGGGGHHHHHH
			$folderData["name"] = trim(substr($scanDBInfo["googleFolderName"], 0, 20));
			$jsonData = json_encode($folderData);
			$action = "folders";

			$createFolder = nessusAPIQuery($action, "create", $jsonData);
			$createFolderObj = json_decode($createFolder);
		
			//print "<br/>DEBUG: Destination is: ".$googleFolderName.",<br/>createFolderObj Object: <br/>";
			//var_dump($createFolderObj);
			//print "<br/>";

			$scanDBInfo["scanFolderID"] = $createFolderObj->id;
			$scanDBInfo["scanFolderName"] = trim(substr($scanDBInfo["googleFolderName"], 0, 20));
		}
		
		// Just in case all folder actions failed, fallback to 'General_Scans'
		if ($scanDBInfo["scanFolderID"] == "") { 
			
			$folderData["name"] = "General_Scans";
			$jsonData = "";
			$action = "folders";

			$getFolder = nessusAPIQuery($action, "get", $jsonData);
			$getFolderObj = json_decode($getFolder);

			foreach ($getFolderObj->folders as $folder) {

				// Nessus has a 20 char limit on folder names... UGGGGHHHHHH
				if ($folder->name == "General_Scans") {
					$scanDBInfo["scanFolderID"] = $folder->id;
					$folderExists = 1;
	//				print "<br/>FOLDER FOUND<br/>";
					break;
				}
	//				print "<br/>DEBUG: Folder is: ".$folder->id.", ".$folder->name."<br/>";
			}

				$scanDBInfo["googleFolderName"] = "General_Scans"; $scanDBInfo["scanFolderName"] = "General_Scans";
				print "Warning! Folder could not be created or does not exist. Scan created in 'General_Scans' (".$scanDBInfo["scanFolderID"].").";
		}

		// DEBUG:
		//print "<br/> scanFolderID is: ".$scanFolderID."<br/>";
		
		$scanData["uuid"] = $scanDBInfo["scanUUID"];
		$scanData["settings"]["name"] = $scanDBInfo["scanName"];
		$scanData["settings"]["description"] = $scanDBInfo["scanRequestor"]."||".$scanDBInfo["googleFolderName"]."||".$scanDBInfo["scanAutoExport"]."||".$scanDBInfo["scanAutoNotify"]."||".$scanDBInfo["snRITM"];
		$scanData["settings"]["folder_id"] = $scanDBInfo["scanFolderID"];
		$scanData["settings"]["enabled"] = $scanDBInfo["scanEnabled"];
		$scanData["settings"]["policy_id"] = $scanDBInfo["scanID"];
		$scanData["settings"]["starttime"] = $dateTimeSchedScanFormat;

		if ($scanDBInfo["scanScheduled"] != "now" && $scanDBInfo["scanFrequency"] != "ONCE") {			
			$scanData["settings"]["launch"] = $scanDBInfo["scanFrequency"]; 
			$scanData["settings"]["rrules"] = $scanDBInfo["scanRRules"];
		}
		
		$scanData["settings"]["timezone"] = "America/New_York";
		$scanData["settings"]["text_targets"] = $scanDBInfo["scanTargetIPs"];

		$jsonData = json_encode($scanData);

		$action = "scans";
		$createScan = nessusAPIQuery($action, "create", $jsonData);
		$createScanObj = json_decode($createScan);
		
		if (isset($createScanObj->scan->name)) {
			
		//print "<br/>DEBUG: createScanObj Object: <br/>";
		//var_dump($createScanObj);
		//print "<br/>";

			$scanDBInfo["scanDescriptionString"] = trim($createScanObj->scan->description);
			$scanDescription = explode("||", $createScanObj->scan->description);
			
			$scanDBInfo["scanIDCreated"] = $createScanObj->scan->id;
?>
			<h1>[ <?php print $createScanObj->scan->name; ?> ] Created</h1> <br/>

			<table class="scanDetailsTable">
			<tr>
				<td width="150px">Scan ID:</td>
				<td width="220px"><?php print $createScanObj->scan->id; ?></td>

				<td width="150px">Scan Start Date:</td>
				<td width="220px"><?php print $dateTimeSchedScanFormat; ?></td>

				<td width="150px">Repeat Rules:</td>
				<td width="220px"><?php print $scanDBInfo["scanRRules"]; ?></td>

			</tr>
			<tr>
				<td width="150px">Nesssus Folder:</td>
				<td width="220px"><?php print trim(substr($scanDescription[1], 0, 20)).", ID:".$scanDBInfo["scanFolderID"]; ?></td>

				<td width="150px">Google Folder:</td>
				<td width="220px"><?php print $scanDescription[1]; ?></td>

				<td width="150px">Scan Requestor:</td>
				<td width="220px"><?php print $scanDescription[0]; ?></td>
			</tr>
			<tr>
				<td width="150px">Target IPs:</td>
				<td colspan="5"><?php print $scanDBInfo["scanTargetIPs"]; ?></td>
			</tr>
			<tr>
				<td width="150px">Scan UUID:</td>
				<td colspan="5"><?php print $scanDBInfo["scanUUID"]; ?></td>
			</tr>
			</table>
<?php		
			include("hoist_db_funcs.inc.php");
			
			$debugMsg = insertScanDB($scanDBInfo);
		
		} // END SCAN NAME EXISTS IF
		else { print "ERROR: Scan Not created successfully. ".$createScan."<br/>"; }
		
	} // END SUBMITSCAN ELSE
	else {
		print "Error: Form action not recognized.<br/>";	
	}
}

function generateNessusReport($scanDBInfo, $exportType) {

		print "Generating report...<br/>";
	
		$scanReportFilename = $scanDBInfo["scanStartDate"]."_".$scanDBInfo["scanName"]."_".$scanDBInfo["scanID"]."_".$scanDBInfo["histID"];
		$debugMsg = "DEBUG: Filename: ".$scanReportFilename.".".$scanDBInfo["reportFormat"]."<br/>\n";

		$scanData["scan_id"] = $scanDBInfo["scanID"];
		$scanData["history_id"] = $scanDBInfo["histID"];
		$scanData["format"] = $scanDBInfo["reportFormat"]; // nessus, csv, db, html, pdf are valid options
		$scanData["chapters"] = "vuln_by_host"; // vuln_hosts_summary, vuln_by_host, compliance_exec, remediations, vuln_by_plugin, compliance
		//$scanData["filters"]["someFilterSetting1"] = $someFilterSetting1;
		//$scanData["filters"]["someFilterSetting2"] = $someFilterSetting2;

		$jsonData = json_encode($scanData);

		$action = "scans/".$scanDBInfo["scanID"]."/export";
		$exportScanJSON = nessusAPIQuery($action, "export", $jsonData);
		$exportScan = json_decode($exportScanJSON);

		print "<br/>DEBUG: exportScan: <br/>";
		var_dump($exportScan);
		print "<br/>";
		
		$reportFileID = "";
		$processing = 1;
		
		include_once("googledrive_api_funcs.inc.php");
		include_once("hoist_db_funcs.inc.php");

		while ($processing == 1) {
			
			if ($exportType == "autoExport") { print "Beginning Export process on NESSUS for Scan: ".$scanDBInfo["scanID"].", Hist: ".$scanDBInfo["histID"].", Format: ".$scanDBInfo["reportFormat"].", this may take a bit... <br/>\n"; }
			
			print "Beginning Export process on NESSUS for Scan: ".$scanDBInfo["scanID"].", Hist: ".$scanDBInfo["histID"].", Format: ".$scanDBInfo["reportFormat"].", this may take a bit... <br/>\n";
			
			$action = "scans/".$scanDBInfo["scanID"]."/export/".$exportScan->file."/status";
			$scanDLStatusJSON = nessusAPIQuery($action, "get", "");
			$scanDLStatus = json_decode($scanDLStatusJSON);
			
			// if the scan is ready...
			if ($scanDLStatus->status == "ready") {
				
				if ($exportType == "autoExport") { print "Report READY for export...<br/>\n"; }

				$fileCreationDate = new DateTime('now', new DateTimeZone('America/New_York')); 

				// download the scan to a file stream...
				$action = "scans/".$scanDBInfo["scanID"]."/export/".$exportScan->file."/download";
				$scanDownload = nessusAPIQuery($action, "download", $scanReportFilename);

				//DEBUG: print $scanDownload;
				$processing = 0;
				$reportFileID = "NONE";
				
				if ($exportType == "autoExport") { print "NESSUS has sent the filestream, ending NESSUS processing loop... [".$processing."]<br/>\n"; }

				// if we're uploading to Google...
				if ($scanDBInfo["scanReportGoogle"] == 1) {
										
					$reportFileIDString = exportToGoogleDrive($scanDBInfo["googleFolderName"], $scanReportFilename, $scanDownload, $scanDBInfo["scanRecipients"], $scanDBInfo["reportFormat"], $exportType);
					$reportFileIDList = explode("|GOOGLEDATA|", $reportFileIDString);
					$reportFileDebug = trim($reportFileIDList[0]);
					$reportFileID = trim($reportFileIDList[1]);
					
					//print "DEBUG: reportFileString: ".$reportFileIDString."<br/>\n";

					if ($exportType == "manualExport") { print $reportFileDebug."|GENREPORTDATA|".$reportFileID."|GENREPORTDATA|".$scanDownload; }
					else { print $reportFileDebug."|GENREPORTDATA|".$reportFileID."|GENREPORTDATA|File data omitted here to prevent log spam.";}
					
					// Update Scan DB
					updateGoogleReportID($scanDBInfo["scanID"], $scanDBInfo["histID"], $scanDBInfo["scanLastModified"], $scanDBInfo["scanCreator"], $reportFileID, $exportType);
					
					//print "<br/>DEBUG: Full reportFileID is: <br/>";
					//var_dump($reportFileID);
					//print "<br/>";
				}
				else {
					if ($exportType == "manualExport") { print "Google Export Disabled|GENREPORTDATA|NONE|GENREPORTDATA|".$scanDownload; }
					else { print "Google Export Disabled|GENREPORTDATA|NONE|GENREPORTDATA|File data omitted here to prevent log spam."; }
				}				
			}
			else { 
				if ($exportType == "autoExport") { "No reports were ready for export.  Trying again shortly...<br/>\n"; }
				sleep(3);
				$processing = 1;
			}
			
		} // END WHILE PROCESSING
	
	return $reportFileID;
}

class curlWithHeaders {

	public $headers; 
	
	public function execCURL($opts) {
        $this->headers = array();
        $opts[CURLOPT_HEADERFUNCTION] = array($this, '_processHeader');
        $ch = curl_init();
        curl_setopt_array($ch, $opts);
        return curl_exec($ch);
    }

    private function _processHeader($ch, $header) {
        $this->headers[] = $header;
        return strlen($header);
    }  
}

// Perform an API query to pull data into HOIST or set data in Nessus
function nessusAPIQuery($action, $type, $jsonData) {
	
	include("api_info.inc.php");

	// DEBUG
	//print "API Get Action: <br/>";
	
	$apiURL = "https://cheerios.iso.vt.edu:8834/".$action;	

	// CURL is used to interact with the API.  The X-ApiKeys-Token header must be set to facilitate 
	// communication with the Nessus API.
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $apiURL);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	if ($type == "create" || $type == "export") { 
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		//curl_setopt($ch, CURLOPT_POST, true); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-ApiKeys: accessKey='.$apiAccess.'; secretKey='.$apiSecret,'Accept: application/json; charset=utf-8','Content-Type: application/json','Content-Length: ' . strlen($jsonData)));
	}
	
	if ($type == "get") {
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-ApiKeys: accessKey='.$apiAccess.'; secretKey='.$apiSecret,'Accept: application/json; charset=utf-8'));
	}

	if ($type == "download") {
		//curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-ApiKeys: accessKey='.$apiAccess.'; secretKey='.$apiSecret,'Accept: application/octet-stream'));
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-ApiKeys: accessKey='.$apiAccess.'; secretKey='.$apiSecret,'Content-Disposition: attachment; filename="'.$jsonData));
	}

	$apiResponse = curl_exec($ch);
	
	if (curl_error($ch)) {
		print "<br/>Unable to connect to the Nessus server.<br/>";
		print "<br/>Error info: ".curl_error($ch);
		exit("<br/>Halting execution since Nessus could not be reached.");
	}

	curl_close($ch);
	$apiAccess = "";
	$apiSecret = "";

//	print "<br/>DEBUG: RAW Response is: <br/>";
//	var_dump($apiResponse);
//	print "<br/>";

	return $apiResponse;
}
	
?>