<?php

function ip2cidr($ipRangeArray) {

	$cidrArray = array();
	$num = ip2long($ipRangeArray[1]) - ip2long($ipRangeArray[0]) + 1;
	$bin = decbin($num);

	$chunk = str_split($bin);
	$chunk = array_reverse($chunk);
	$startCount = 0;

	while ($start < count($chunk)) {

		if ($chunk[$start] != 0) {
			$startIP = isset($range) ? long2ip(ip2long($range[1]) + 1) : $ipRangeArray[0];
			$range = cidr2ip($startIP . '/' . (32 - $startCount));
			$cidrArray[] = $startIP . '/' . (32 - $startCount);
		}
		$start++;
	}
	return $cidrArray;
}

?>