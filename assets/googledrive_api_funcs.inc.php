<?php


function exportToGoogleDrive($scanFolderName, $scanReportFilename, $scanDownload, $scanRecipients, $reportFormat, $exportType) {
	$debugMsg = "DEBUG: ";
	
	$debugMsg = $debugMsg."Export to Google Enabled<br/>\n";

	// Google Drive API
	require_once '../google-api-php-client-2.2.0/vendor/autoload.php';
	putenv('GOOGLE_APPLICATION_CREDENTIALS=../assets/itso-hoist-dd5c14498638.json');

	$client = new Google_Client();

	if (getenv('GOOGLE_APPLICATION_CREDENTIALS')) {
		// use the application default credentials
		$client->useApplicationDefaultCredentials();
		//print "<br/>Success: Google API Authorized. <br/>";
	}
	else { 
		$debugMsg = $debugMsg."<br/>ERROR: Google API Authorization Failed. Exiting. <br/>"; 
		exit; 
	}

	$client->addScope(Google_Service_Drive::DRIVE);

	$drive_service = new Google_Service_Drive($client);

	// test parent folder existence
	$scanFolderFound = 0;

	try {
		$tdFilesList = $drive_service->files->listFiles(array("corpora"=>"teamDrive","supportsTeamDrives"=>1,"teamDriveId"=>"0AH64_VBABStyUk9PVA","includeTeamDriveItems"=>1,"q"=>"trashed=false"))->getFiles();

		foreach ($tdFilesList as $tdFile) {
			if ($tdFile->name == $scanFolderName) {
				$scanFolderFound = 1;
				$scanFolderID = $tdFile->id;
				$debugMsg = $debugMsg."<br/>Parent Folder ID: ".$tdFile->id.", Parent Folder Name: ".$tdFile->name."<br/>";
			}
		}
	}
	catch (Exception $e) {
		$debugMsg = $debugMsg.$e->getMessage();
	}

	if ($scanFolderFound != 1) {
		// create folder structure if needed... 
		try {

			$parentFolderID = "0AH64_VBABStyUk9PVA"; // ITSO SCAN REPORTS (TEAM DRIVE ROOT)
			$folderMetadata = new Google_Service_Drive_DriveFile(array(
				'name' => $scanFolderName,
				'teamDriveId' => $parentFolderID,
				'mimeType' => 'application/vnd.google-apps.folder',
				'parents' => array($parentFolderID)
			));
				//'parents' => array($folderID)

			$folder = $drive_service->files->create($folderMetadata, array(
				'supportsTeamDrives' => 1,
				'fields' => 'id'));

			$scanFolderID = trim($folder->id);
		}
		catch (Exception $e) {
			$debugMsg = $debugMsg.$e->getMessage();
		}
	}


	// check to see if the report already exists in Google Drive
	$reportFileFound = 0;
	$reportFileID = "";

	try {
		$tdFilesList = $drive_service->files->listFiles(array("corpora"=>"teamDrive","supportsTeamDrives"=>1,"teamDriveId"=>"0AH64_VBABStyUk9PVA","includeTeamDriveItems"=>1,"q"=>"trashed=false"))->getFiles();

		foreach ($tdFilesList as $tdFile) {
			if ($tdFile->name == $scanReportFilename.".".$reportFormat) {
				$reportFileFound = 1;
				$reportFileID = $tdFile->id;
				$debugMsg = $debugMsg."<br/>Report File ID: ".$tdFile->id.", Report File Name: ".$tdFile->name."<br/>";
			}
		}
	}
	catch (Exception $e) {
		$debugMsg = $debugMsg.$e->getMessage();
	}

	if ($reportFileFound != 1) {
		
		// create and upload a new Google Drive file, including the data
		try {
			$debugMsg = $debugMsg."<br/>CREATING NEW FILE: ".$scanReportFilename.'.'.$reportFormat."<br/>\n";
			
			$subFolderID = $scanFolderID;
			$fileMetadata = new Google_Service_Drive_DriveFile(array(
				'name' => $scanReportFilename.'.'.$reportFormat, 
				'teamDriveId' => $subFolderID,
				'parents' => array($subFolderID)
			));

			if ($reportFormat == "html") { $reportMimeType = "text/html"; }
			else if ($reportFormat == "pdf") { $reportMimeType = "application/pdf"; }
			else if ($reportFormat == "csv") { $reportMimeType = "text/csv"; }
			else if ($reportFormat == "nessus") { $reportMimeType = "application/octet-stream"; }
			else if ($reportFormat == "db") { $reportMimeType = "application/octet-stream"; }
			else { $reportFormat = "html"; $reportMimeType = "text/html"; }
			
			if ($reportFormat == "UNKNOWN") { $content = file_get_contents($scanDownload); }
			else { $content = $scanDownload; }
			
			$file = $drive_service->files->create($fileMetadata, array(
				'data' => $content,
				'mimeType' => $reportMimeType,
				'uploadType' => 'multipart',
				'supportsTeamDrives' => 1,
				'fields' => 'id'));

			$reportFileID = trim($file->id);
			$drive_service->getClient()->setUseBatch(true);

			$debugMsg = $debugMsg."\n<br/>Recipients: ".$scanRecipients."<br/>\n";

			$debugMsg = $debugMsg."DEBUG PERMISSIONS: <br/>\n";

			// Batch doesn't seem to work unless I execute each item individually...
			// otherwise, it seems to overwrite each batched item with just the last item added to the batch...
			// Moved execute() into the foreach loop and added it below itsotest to ensure all permissions are added.
			try {
				$batch = $drive_service->createBatch();

				$userPermission = new Google_Service_Drive_Permission(array('type' => 'user', 'role' => 'reader', 'emailAddress' => 'itsotest@vt.edu' ));
				$request = $drive_service->permissions->create($reportFileID, $userPermission, array('supportsTeamDrives' => 1, 'sendNotificationEmail' => 0, 'fields' => 'id'));
				$batch->add($request, 'user');
				$results = $batch->execute();
							
				if ($scanRecipients != "") {
					$scanRecipientList = explode(",", $scanRecipients);

					foreach ($scanRecipientList as $scanRecipientItem) {
						$debugMsg = $debugMsg."<br/>\nScan Recipient: ".$scanRecipientItem."<br/>\n";
						
						$userPermission = new Google_Service_Drive_Permission(array('type' => 'user', 'role' => 'reader', 'emailAddress' => $scanRecipientItem));
						$request = $drive_service->permissions->create($reportFileID, $userPermission, array('supportsTeamDrives' => 1, 'sendNotificationEmail' => 0, 'fields' => 'id'));
						$batch->add($request, 'user');
						$results = $batch->execute();
					}
				}
/*
				$results = $batch->execute();
				
				foreach ($results as $result) {
					if ($result instanceof Google_Service_Exception) {
						// Handle error
						$debugMsg = $debugMsg."<br/>".print_r($result)."<br/>";
					} else {
						$debugMsg = $debugMsg."<br/>File Permission ID: ".print_r($result)."<br/>";
					}
				}
*/
			} finally { $drive_service->getClient()->setUseBatch(false); }

		}
		catch (Exception $e) { $debugMsg = $debugMsg.$e->getMessage(); }
	} // END IF REPORTFILE NOT FOUND IF
	
	return $debugMsg."|GOOGLEDATA|".$reportFileID;
}

?>