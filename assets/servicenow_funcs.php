<?php

	$mode = "LIVE"; $modeURL = "";

	// PHP ERROR REPORTING LEVEL
	error_reporting(E_ALL); 
	ini_set('display_errors', '1');

	// force DEV mode if we're on a dev instance...
	if (isset($_SERVER["SERVER_NAME"]) && $_SERVER["SERVER_NAME"] == "localhost") { $mode = "DEV"; $modeURL = "dev"; }
	else if (isset($_SERVER["REQUEST_URI"]) && strstr($_SERVER["REQUEST_URI"], "/hoistdev")) { $mode = "DEV"; $modeURL = "dev"; }

function snAPIQuery($url, $user, $pass, $jsonData, $type) {
		
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_USERPWD, $user.":".$pass);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_VERBOSE, 0);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Accept: application/json'));
	//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
	
	if ($type == "insert" || $type == "set") { 
		curl_setopt($ch, CURLOPT_POST, true); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
	}
	elseif ($type == "update") { 
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT"); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
	} 
	elseif ($type == "patch") { 
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH"); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
	} 
	elseif ($type == "get") {
		//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET"); 
	}	

	$snResponse = curl_exec($ch);
	
	if (curl_error($ch)) {
		print "<br/>Unable to connect to the Service-Now API.";
		print "<br/>Error info: ".curl_error($ch);
		exit("<br/>Halting execution since Service-Now could not be reached.");
	}

	curl_close($ch);

	//print "<br/>DEBUG: RAW Response is: <br/>";
	//var_dump($snResponse);
	//print_r($snResponse);
	//$snInciXML = simplexml_load_string($snInciResponse);
	//$snInciJSON = json_encode($snInciXML);
	
	//print "<br/>DEBUG: JSON Response is: <br/>";
	//var_dump($snInciJSON);
	
	//return $snInciJSON;
	return $snResponse;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function getRITMList($casUser, $modeURL) {
	
	include("sn_info.inc.php");

	// dev category  : 5fa271330f3bb280b97f0bcce1050eee
	// live category : 5fa271330f3bb280b97f0bcce1050eee
	$snRITMListURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sc_req_item?sysparm_query=active=true&cat_item=5fa271330f3bb280b97f0bcce1050eee&sysparm_display_value=all"; 
	$snRITMList = snAPIQuery($snRITMListURL, $snUser, $snPass, $casUser, "get");
	$snRITMListJSONResponse = json_decode($snRITMList);
	
	return $snRITMListJSONResponse;
}

function notifyRecipients($reqItemNumber, $googleDriveURL, $casUser, $modeURL) {

	include("sn_info.inc.php");
	
	if ($casUser == "itso_user") {	
		print "Performing automated notification.<br/>\n";
		print "Vars: ".$reqItemNumber.", ".$googleDriveURL.", ".$casUser.", ".$modeURL."\n\n<br/>";
	}
	
	$snCASUserURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sys_user?user_name=".$casUser;  
	$snGetCASUser = snAPIQuery($snCASUserURL, $snUser, $snPass, $casUser, "get");

	if ($snGetCASUser == "{\"result\":[]}") { 
		print "Somehow YOU were not found in Service-Now... This should never happen.  Halting.";
		exit;
	}

	$snCASUserJSONResponse = json_decode($snGetCASUser);
	$casUserSysID = $snCASUserJSONResponse->result[0]->sys_id;

	
	$snTaskItemURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sc_task?sysparm_query=request_item.number=".$reqItemNumber."&sysparm_display_value=all";
	$snTaskItem = snAPIQuery($snTaskItemURL, $snUser, $snPass, $casUser, "get");
	$snTaskItemJSONResponse = json_decode($snTaskItem);
	//print "DEBUG: TASK<br/>";
	//print_r($snTaskItemJSONResponse);		
	//print "<br/>";

	foreach ($snTaskItemJSONResponse->result as $task) {
		
		if (trim($task->short_description->value) == "Send Report") {

			$taskID = $task->sys_id->value;
			$snTaskUpdateURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sc_task/".$taskID;
			
			$jsonDataUpd["work_notes"] = "Scan Completed.\n\nReport is available at ".trim($googleDriveURL)."\n";
			$jsonDataUpd["sys_updated_by"] = $casUser;
			$jsonDataUpd["state"] = "3";
			$jsonDataUpd["approval"] = "approved";
			if ($casUser != "scanAutoNotify") { $jsonDataUpd["assigned_to"] = $casUserSysID; }

			$jsonDataUpdEnc = json_encode($jsonDataUpd, JSON_FORCE_OBJECT);

			//print "<br/>DEBUG: URL is: ".$snTaskApproveURL."<br/>ENC jsonDataUpdEnc is: <br/>";
			//print $jsonDataUpdEnc."<br/>";

			$snTaskUpdate = snAPIQuery($snTaskUpdateURL, $snUser, $snPass, $jsonDataUpdEnc, "update");

			$snUpdateJSONResponse = json_decode($snTaskUpdate);
			//print "<br/>DEBUG: Update response JSON is: <br/>".$snTaskUpdate."<br/>";

			if (isset($snUpdateJSONResponse->result->number)) {
				$snTaskUpdateNumber = $snUpdateJSONResponse->result->number;
				print "Task ".$snTaskUpdateNumber." updated.  Advancing RITM...<br/>|NOTIFYDATA|".$reqItemNumber;
				
				// clear the data so we don't have errant fields getting updated...
				unset($jsonDataUpd);

				$snReqItemURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sc_req_item?sysparm_query=number=".$reqItemNumber."&sysparm_display_value=all";
				$snReqItem = snAPIQuery($snReqItemURL, $snUser, $snPass, $casUser, "get");
				$snReqItemJSONResponse = json_decode($snReqItem);
				
				// Get the sys_id from the RITM number...
				$reqItemSysID = "";
				foreach ($snReqItemJSONResponse->result as $ritm) { $reqItemSysID = $ritm->sys_id->value; }
				
				$snRITMUpdateURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sc_req_item/".$reqItemSysID;

				$jsonDataUpd["comments"] = "Scan Completed.\n\nReport is available at ".trim($googleDriveURL)."\n";
				//$jsonDataUpd["work_notes"] = "Scan Completed.\n\nReport is available at ".trim($googleDriveURL)."\n";
				$jsonDataUpd["sys_updated_by"] = $casUser;
				$jsonDataUpd["state"] = "3";
				$jsonDataUpd["approval"] = "approved";
				if ($casUser != "scanAutoNotify") { $jsonDataUpd["assigned_to"] = $casUserSysID; }

				$jsonDataUpdEnc = json_encode($jsonDataUpd, JSON_FORCE_OBJECT);

				//print "<br/>DEBUG: URL is: ".$snRITMUpdateURL."<br/>ENC jsonDataUpdEnc is: <br/>";
				//print $jsonDataUpdEnc."<br/>";

				$snRITMUpdate = snAPIQuery($snRITMUpdateURL, $snUser, $snPass, $jsonDataUpdEnc, "update");

				$snUpdateJSONResponse = json_decode($snRITMUpdate);
				//print "<br/>DEBUG: Update response JSON is: <br/>".$snRITMUpdate."<br/>";

				if (isset($snUpdateJSONResponse->result->number)) {
					$snRITMUpdateNumber = $snUpdateJSONResponse->result->number;
					print $snRITMUpdateNumber." updated COMPLETE.<br/>|NOTIFYDATA|".$reqItemNumber;
				}
				else {
					print "ERROR|NOTIFYDATA|Error updating RITM.<br/>";
				}
			}
			else {
				print "ERROR|NOTIFYDATA|Error updating Task.<br/>";
			}
			
			unset($snUser);
			unset($snPass);
		}
	}
}

function approveScan($reqItemNumber, $casUser, $casUserSysID, $modeURL) {

	include("sn_info.inc.php");
		
	$snTaskItemURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sc_task?sysparm_query=request_item.number=".$reqItemNumber."&sysparm_display_value=all";
	$snTaskItem = snAPIQuery($snTaskItemURL, $snUser, $snPass, $casUser, "get");
	$snTaskItemJSONResponse = json_decode($snTaskItem);
	//print "DEBUG: TASK<br/>";
	//print_r($snTaskItemJSONResponse);		
	//print "<br/>";

	foreach ($snTaskItemJSONResponse->result as $task) {
		
		if (trim($task->short_description->value) == "Approval and Scheduling") {

			$taskID = $task->sys_id->value;
			$snTaskApproveURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sc_task/".$taskID;
			
			$jsonDataUpd["work_notes"] = "Scan APPROVED by ".trim($casUser)."\n";
			$jsonDataUpd["sys_updated_by"] = $casUser;
			$jsonDataUpd["state"] = "3";
			$jsonDataUpd["approval"] = "approved";
			$jsonDataUpd["assigned_to"] = $casUserSysID;

			$jsonDataUpdEnc = json_encode($jsonDataUpd, JSON_FORCE_OBJECT);

			//print "<br/>DEBUG: URL is: ".$snTaskApproveURL."<br/>ENC jsonDataUpdEnc is: <br/>";
			//print $jsonDataUpdEnc."<br/>";

			$snTaskApprove = snAPIQuery($snTaskApproveURL, $snUser, $snPass, $jsonDataUpdEnc, "update");

			$snUpdateJSONResponse = json_decode($snTaskApprove);
			//print "<br/>DEBUG: Update response JSON is: <br/>".$snTaskApprove."<br/>";

			if (isset($snUpdateJSONResponse->result->number)) {
				$snTaskUpdateNumber = $snUpdateJSONResponse->result->number;
				print "Task ".$snTaskUpdateNumber." updated, advancing ".$reqItemNumber." to the next stage...<br/>";
				
				// clear the data so we don't have errant fields getting updated...
				unset($jsonDataUpd);
				
				$snReqItemURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sc_req_item?sysparm_query=number=".$reqItemNumber."&sysparm_display_value=all";
				$snReqItem = snAPIQuery($snReqItemURL, $snUser, $snPass, $casUser, "get");
				$snReqItemJSONResponse = json_decode($snReqItem);
				
				// Get the sys_id from the RITM number...
				$reqItemSysID = "";
				foreach ($snReqItemJSONResponse->result as $ritm) { $reqItemSysID = $ritm->sys_id->value; }
				
				$snRITMUpdateURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sc_req_item/".$reqItemSysID;

				$jsonDataUpd["comments"] = "Scan APPROVED by ".trim($casUser)."\n";
				//$jsonDataUpd["work_notes"] = "Scan APPROVED by ".trim($casUser)."\n";
				$jsonDataUpd["sys_updated_by"] = $casUser;

				$jsonDataUpdEnc = json_encode($jsonDataUpd, JSON_FORCE_OBJECT);

				//print "<br/>DEBUG: URL is: ".$snRITMUpdateURL."<br/>ENC jsonDataUpdEnc is: <br/>";
				//print $jsonDataUpdEnc."<br/>";

				$snRITMUpdate = snAPIQuery($snRITMUpdateURL, $snUser, $snPass, $jsonDataUpdEnc, "update");

				$snUpdateJSONResponse = json_decode($snRITMUpdate);
				//print "<br/>DEBUG: Update response JSON is: <br/>".$snRITMUpdate."<br/>";

				if (isset($snUpdateJSONResponse->result->number)) {
					$snRITMUpdateNumber = $snUpdateJSONResponse->result->number;
					print "RITM ".$snRITMUpdateNumber." updated with <span class='green'>APPROVAL</span>, Scan Requestor notified.<br/>|APPROVEDATA|".$reqItemNumber;
				}
				else {
					print "ERROR|APPROVEDATA|Error updating RITM.<br/>";
				}
			}
			else {
				print "ERROR|APPROVEDATA|Error updating Task.<br/>";
			}
			
			unset($snUser);
			unset($snPass);

			// REJECTED
			// [state] => [display_value] => Closed Incomplete [value] => 4
			// [approval] => [display_value] => Rejected [value] => rejected

			// APPROVED
			// [state] => [display_value] => Closed Complete [value] => 3
			// [approval] => [display_value] => Approved [value] => approved

			// [assigned_to] => [display_value] => Stephen Huff [value] => 80a528b50f626500ee5a0bcce1050ecf

		}
	}
}

function rejectScan($reqItemNumber, $casUser, $casUserSysID, $modeURL) {

	include("sn_info.inc.php");
		
	$snTaskItemURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sc_task?sysparm_query=request_item.number=".$reqItemNumber."&sysparm_display_value=all";
	$snTaskItem = snAPIQuery($snTaskItemURL, $snUser, $snPass, $casUser, "get");
	$snTaskItemJSONResponse = json_decode($snTaskItem);
	//print "DEBUG: TASK<br/>";
	//print_r($snTaskItemJSONResponse);		
	//print "<br/>";

	foreach ($snTaskItemJSONResponse->result as $task) {
		// DEBUG
		//print "DEBUG: REJECTTASK<br/>";
		//print_r($task);
		//print "DescVal = ".$task->short_description->value;
		//print "<br/>";
		
		if (trim($task->short_description->value) == "Approval and Scheduling") {
			//print "IN APPVOVAL AND SCHED, ".$modeURL."<br/>";
			$taskID = $task->sys_id->value;
			$snTaskRejectURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sc_task/".$taskID;
			
			$jsonDataUpd["work_notes"] = "Scan REJECTED by ".trim($casUser)."\n";
			$jsonDataUpd["sys_updated_by"] = $casUser;
			$jsonDataUpd["state"] = "4";
			$jsonDataUpd["approval"] = "rejected";
			$jsonDataUpd["assigned_to"] = $casUserSysID;

			$jsonDataUpdEnc = json_encode($jsonDataUpd, JSON_FORCE_OBJECT);

			//print "<br/>DEBUG: URL is: ".$snTaskRejectURL."<br/>ENC jsonDataUpdEnc is: <br/>";
			print $jsonDataUpdEnc."<br/>";

			$snTaskReject = snAPIQuery($snTaskRejectURL, $snUser, $snPass, $jsonDataUpdEnc, "update");

			$snUpdateJSONResponse = json_decode($snTaskReject);
			//print "<br/>DEBUG: Update response JSON is: <br/>".$snTaskReject."<br/>";

			if (isset($snUpdateJSONResponse->result->number)) {
				$snTaskUpdateNumber = $snUpdateJSONResponse->result->number;
				print "<br/>Task ".$snTaskUpdateNumber." updated.<br/>";
				
				// clear the data so we don't have errant fields getting updated...
				unset($jsonDataUpd);

				$snReqItemURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sc_req_item?sysparm_query=number=".$reqItemNumber."&sysparm_display_value=all";
				$snReqItem = snAPIQuery($snReqItemURL, $snUser, $snPass, $casUser, "get");
				$snReqItemJSONResponse = json_decode($snReqItem);
				
				// Get the sys_id from the RITM number...
				$reqItemSysID = "";
				foreach ($snReqItemJSONResponse->result as $ritm) { $reqItemSysID = $ritm->sys_id->value; }
				
				$snRITMUpdateURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sc_req_item/".$reqItemSysID;

				$jsonDataUpd["comments"] = "Scan rejected/cancelled by ".trim($casUser)."\n\nPlease contact the ITSO if you have any questions regarding this decision.\n";
				//$jsonDataUpd["work_notes"] = "Scan rejected/cancelled by ".trim($casUser)."\n\nPlease contact the ITSO if you have any questions regarding this decision.\n";
				$jsonDataUpd["state"] = "4";
				$jsonDataUpd["approval"] = "rejected";
				$jsonDataUpd["assigned_to"] = $casUserSysID;
				$jsonDataUpd["sys_updated_by"] = $casUser;

				$jsonDataUpdEnc = json_encode($jsonDataUpd, JSON_FORCE_OBJECT);

				//print "<br/>DEBUG: URL is: ".$snRITMUpdateURL."<br/>ENC jsonDataUpdEnc is: <br/>";
				//print $jsonDataUpdEnc."<br/>";

				$snRITMUpdate = snAPIQuery($snRITMUpdateURL, $snUser, $snPass, $jsonDataUpdEnc, "update");

				$snUpdateJSONResponse = json_decode($snRITMUpdate);
				//print "<br/>DEBUG: Update response JSON is: <br/>".$snRITMUpdate."<br/>";

				if (isset($snUpdateJSONResponse->result->number)) {
					$snRITMUpdateNumber = $snUpdateJSONResponse->result->number;
					print "RITM ".$snRITMUpdateNumber." updated.<br/>|REJECTDATA|".$reqItemNumber;
				}
				else {
					print "ERROR|REJECTDATA|Error updating RITM.<br/>";
				}
			}
			else {
				print "<br/>Error updating Task.<br/>";
			}
			
			unset($snUser);
			unset($snPass);

			// REJECTED
			// [state] => [display_value] => Closed Incomplete [value] => 4
			// [approval] => [display_value] => Rejected [value] => rejected

			// APPROVED
			// [state] => [display_value] => Closed Complete [value] => 3
			// [approval] => [display_value] => Approved [value] => approved

			// [assigned_to] => [display_value] => Stephen Huff [value] => 80a528b50f626500ee5a0bcce1050ecf

		}
	}

}

function getRITMStatus($reqItemNumber, $casUser, $modeURL) {

	include("sn_info.inc.php");
		
	$snReqItemURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sc_req_item?sysparm_query=number=".$reqItemNumber."&sysparm_display_value=all";
	$snReqItem = snAPIQuery($snReqItemURL, $snUser, $snPass, $casUser, "get");
	$snReqItemJSONResponse = json_decode($snReqItem);
	
	// Get the sys_id from the RITM number...
	$ritmStatusList["reqItemSysID"] = "";
	
	foreach ($snReqItemJSONResponse->result as $ritm) {
		$ritmStatusList["reqItemSysID"] = $ritm->sys_id->value;
		$ritmStatusList["reqItemStage"] = $ritm->stage->value;
		$ritmStatusList["reqItemDueDate"] = $ritm->due_date->value;
		$ritmStatusList["reqItemNumber"] = $reqItemNumber;
	}
	
	return $ritmStatusList;
}

function getRITMData($reqItemNumber, $casUser, $modeURL) {

	include("sn_info.inc.php");
	
	$snCASUserURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sys_user?user_name=".$casUser;  
	$snGetCASUser = snAPIQuery($snCASUserURL, $snUser, $snPass, $casUser, "get");

	if ($snGetCASUser == "{\"result\":[]}") { 
		print "Somehow YOU were not found in Service-Now... This should never happen.  Halting.";
		exit;
	}

	$snCASUserJSONResponse = json_decode($snGetCASUser);
	$ioResultList["casUserSysID"] = $snCASUserJSONResponse->result[0]->sys_id;
	
	$snReqItemURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sc_req_item?sysparm_query=number=".$reqItemNumber."&sysparm_display_value=all";
	$snReqItem = snAPIQuery($snReqItemURL, $snUser, $snPass, $casUser, "get");
	$snReqItemJSONResponse = json_decode($snReqItem);
	
	// Get the sys_id from the RITM number...
	$ioResultList["reqItemSysID"] = "";
	foreach ($snReqItemJSONResponse->result as $ritm) {
		$ioResultList["reqItemSysID"] = $ritm->sys_id->value;
		$ioResultList["reqItemStage"] = $ritm->stage->value;
		$ioResultList["reqItemDueDate"] = $ritm->due_date->value;
		$ioResultList["reqItemNumber"] = $reqItemNumber;
		
		//print "DEBUG: RITM<br/>";
		//print_r($ritm);
		//print "<br/>";
				
		$snReqItemURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sc_task?sysparm_query=request_item.number=".$reqItemNumber."&sysparm_display_value=all";
		$snReqItem = snAPIQuery($snReqItemURL, $snUser, $snPass, $casUser, "get");
		$snReqItemJSONResponse = json_decode($snReqItem);
		//print "DEBUG: TASK<br/>";
		//print_r($snReqItemJSONResponse);		
		//print "<br/>";
		
		// REJECTED
		// [state] => [display_value] => Closed Incomplete [value] => 4
		// [approval] => [display_value] => Rejected [value] => rejected
		
		// APPROVED
		// [state] => [display_value] => Closed Complete [value] => 3
		// [approval] => [display_value] => Approved [value] => approved
		
		// [assigned_to] => [display_value] => Stephen Huff [value] => 80a528b50f626500ee5a0bcce1050ecf
	}

	// now use the $ioResultList["reqItemSysID"] to get all options in the many-to-many table associated with that RITM sys_id...
	// this will result in a list of sc_item_option values, which are sys_ids of each question/answer...
	$snReqItemOptionsURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sc_item_option_mtom?sysparm_query=request_item=".$ioResultList["reqItemSysID"]."&sysparm_display_value=all";
	$snReqItemOptions = snAPIQuery($snReqItemOptionsURL, $snUser, $snPass, $casUser, "get");
	$snReqItemOptionsJSONResponse = json_decode($snReqItemOptions);
	
	foreach ($snReqItemOptionsJSONResponse->result as $ritmOptions) {
		//print "<br/>FULL rtimRESULT START<br/>";
		//print_r($ritmOptions);
		//print "<br/>FULL rtimRESULT END<br/>";
		
		// for each sc_item_option values (sys_ids),
		$reqOptionSysID = $ritmOptions->sys_id->value;
		$itemOptionSysID = $ritmOptions->sc_item_option->value;
			
		$snItemOptionURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sc_item_option?sysparm_query=sys_id=".$itemOptionSysID."&sysparm_display_value=all";	
		$snItemOption = snAPIQuery($snItemOptionURL, $snUser, $snPass, $casUser, "get");
		$snItemOptionJSONResponse = json_decode($snItemOption);
		
		foreach ($snItemOptionJSONResponse->result as $ioResult) {
			//print "<br/>FULL ioRESULT START<br/>";
			//print_r($ioResult);
			//print "<br/>FULL ioRESULT END<br/>";
						
			if ($ioResult->item_option_new->value == "a7a2f1f30f3bb280b97f0bcce1050e4c") {
				// 1. Name
				$ioResultList["nameID"] = $ioResult->value->value;
				
				$snUserURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sys_user?sys_id=".$ioResultList["nameID"];
				$snUserObj = snAPIQuery($snUserURL, $snUser, $snPass, $ioResultList["nameID"], "get");
				$snUserObjJSONResponse = json_decode($snUserObj);

				$ioResultList["namePID"] = $snUserObjJSONResponse->result[0]->user_name;
				$ioResultList["name"] = $snUserObjJSONResponse->result[0]->name;
				
				//print_r($snUserObjJSONResponse->result[0]);
				//print "DEBUG - PID is: ".$ioResultList["namePID"]."<br/>";
			}
			else if ($ioResult->item_option_new->value == "7ba2f1f30f3bb280b97f0bcce1050e63") {
				// 2. Department
				$ioResultList["departmentID"] = $ioResult->value->value;
								
				$snDeptURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/cmn_department?sys_id=".$ioResultList["departmentID"];
				$snDeptObj = snAPIQuery($snDeptURL, $snUser, $snPass, $ioResultList["departmentID"], "get");
				$snDeptObjJSONResponse = json_decode($snDeptObj);

				$ioResultList["departmentName"] = $snDeptObjJSONResponse->result[0]->name;				
			}
			else if ($ioResult->item_option_new->value == "afa2f1f30f3bb280b97f0bcce1050e34") {
				// Best Results allow Scanners Text
				
			}
			else if ($ioResult->item_option_new->value == "e3a2f1f30f3bb280b97f0bcce1050e50") {
				// 3. Systems to Scan
				$ioResultList["scanTargetIPs"] = $ioResult->value->value;
			}
			else if ($ioResult->item_option_new->value == "7fa2f1f30f3bb280b97f0bcce1050e57") {
				// 4. Host or Application
				$ioResultList["scanHostApp"] = $ioResult->value->value;
			}
			else if ($ioResult->item_option_new->value == "bba2f1f30f3bb280b97f0bcce1050e5f") {
				// 5. Preferred time for scanning
				$ioResultList["preferredTime"] = $ioResult->value->value;
			}
			else if ($ioResult->item_option_new->value == "33a2f1f30f3bb280b97f0bcce1050e54") {
				// 6. Perform Unauthenticated Scan or Scan as an Authenticated User Text
				
			}
			else if ($ioResult->item_option_new->value == "bba2f1f30f3bb280b97f0bcce1050e6b") {
				// => Unauthenticate Option
				$ioResultList["scanIsUnauth"] = $ioResult->value->value;
			}
			else if ($ioResult->item_option_new->value == "2ba2f1f30f3bb280b97f0bcce1050e2d") {
				// => Authenticated Option
				$ioResultList["scanIsAuth"] = $ioResult->value->value;
			}
			else if ($ioResult->item_option_new->value == "d9f81764db0e13c0c53b13141b9619a4" && $modeURL == "") {
				// 7. Schedule Scan to Repeat Text (LIVE)
				
			}
			else if ($ioResult->item_option_new->value == "f9be93ec0f46d700b97f0bcce1050e04" && $modeURL == "dev") {
				// 7. Schedule Scan to Repeat Text (DEV)
				
			}
			else if ($ioResult->item_option_new->value == "21495764db0e13c0c53b13141b96192b" && $modeURL == "") {
				// => Recurrence Option (LIVE)
				$ioResultList["scanRecurrence"] = $ioResult->value->value;
			}
			else if ($ioResult->item_option_new->value == "da2f53a40f46d700b97f0bcce1050e3a" && $modeURL == "dev") {
				// => Recurrence Option (DEV)
				$ioResultList["scanRecurrence"] = $ioResult->value->value;
			}
			else if ($ioResult->item_option_new->value == "bba2f1f30f3bb280b97f0bcce1050e6f") {
				// 8. Names of Individuals who will access scan reports text

			}
			else if ($ioResult->item_option_new->value == "3fa2f1f30f3bb280b97f0bcce1050e5b") {
				// #1 Scan Access
				if ($ioResult->value->value != "") {
					$ioResultList["scanAccess"][0]["id"] = $ioResult->value->value;

					$snUserURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sys_user?sys_id=".$ioResultList["scanAccess"][0]["id"];
					$snUserObj = snAPIQuery($snUserURL, $snUser, $snPass, $ioResultList["scanAccess"][0]["id"], "get");
					$snUserObjJSONResponse = json_decode($snUserObj);

					$ioResultList["scanAccess"][0]["pid"] = $snUserObjJSONResponse->result[0]->user_name;
					$ioResultList["scanAccess"][0]["name"] = $snUserObjJSONResponse->result[0]->name;
				}
			}
			else if ($ioResult->item_option_new->value == "23a2f1f30f3bb280b97f0bcce1050e45") {
				// #2 Scan Access
				if ($ioResult->value->value != "") {
					$ioResultList["scanAccess"][1]["id"]  = $ioResult->value->value;

					$snUserURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sys_user?sys_id=".$ioResultList["scanAccess"][1]["id"];
					$snUserObj = snAPIQuery($snUserURL, $snUser, $snPass, $ioResultList["scanAccess"][1]["id"], "get");
					$snUserObjJSONResponse = json_decode($snUserObj);

					$ioResultList["scanAccess"][1]["pid"] = $snUserObjJSONResponse->result[0]->user_name;
					$ioResultList["scanAccess"][1]["name"] = $snUserObjJSONResponse->result[0]->name;
				}
			}
			else if ($ioResult->item_option_new->value == "bba2f1f30f3bb280b97f0bcce1050e67") {
				// #3 Scan Access
				if ($ioResult->value->value != "") {
					$ioResultList["scanAccess"][2]["id"] = $ioResult->value->value;

					$snUserURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sys_user?sys_id=".$ioResultList["scanAccess"][2]["id"];
					$snUserObj = snAPIQuery($snUserURL, $snUser, $snPass, $ioResultList["scanAccess"][2]["id"], "get");
					$snUserObjJSONResponse = json_decode($snUserObj);

					$ioResultList["scanAccess"][2]["pid"] = $snUserObjJSONResponse->result[0]->user_name;
					$ioResultList["scanAccess"][2]["name"] = $snUserObjJSONResponse->result[0]->name;
				}
			}
			else if ($ioResult->item_option_new->value == "6fa2f1f30f3bb280b97f0bcce1050e28") {
				// #4 Scan Access
				if ($ioResult->value->value != "") {
					$ioResultList["scanAccess"][3]["id"] = $ioResult->value->value;

					$snUserURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sys_user?sys_id=".$ioResultList["scanAccess"][3]["id"];
					$snUserObj = snAPIQuery($snUserURL, $snUser, $snPass, $ioResultList["scanAccess"][3]["id"], "get");
					$snUserObjJSONResponse = json_decode($snUserObj);

					$ioResultList["scanAccess"][3]["pid"] = $snUserObjJSONResponse->result[0]->user_name;
					$ioResultList["scanAccess"][3]["name"] = $snUserObjJSONResponse->result[0]->name;
				}
			}
			else if ($ioResult->item_option_new->value == "e3a2f1f30f3bb280b97f0bcce1050e31") {
				// #5 Scan Access
				if ($ioResult->value->value != "") {
					$ioResultList["scanAccess"][4]["id"] = $ioResult->value->value;

					$snUserURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sys_user?sys_id=".$ioResultList["scanAccess"][4]["id"];
					$snUserObj = snAPIQuery($snUserURL, $snUser, $snPass, $ioResultList["scanAccess"][4]["id"], "get");
					$snUserObjJSONResponse = json_decode($snUserObj);

					$ioResultList["scanAccess"][4]["pid"] = $snUserObjJSONResponse->result[0]->user_name;
					$ioResultList["scanAccess"][4]["name"] = $snUserObjJSONResponse->result[0]->name;
				}
			}
			else if ($ioResult->item_option_new->value == "eba2f1f30f3bb280b97f0bcce1050e48") {
				// 8. Notes (provide any other relevant information):
				$ioResultList["otherNotes"] = $ioResult->value->value;
			}
			else {
				print "UNKNOWN SN DATA. DEBUG: <br/>";	
				print_r($ioResult);
				print "<br/>";
			}
		}
	}
	return $ioResultList;
}

function createRITM($ritmObj, $modeURL) {

	include("sn_info.inc.php");
	
	/* 
	ITSO_name 	ITSO_department 	ITSO_systems  	ITSO_host_or_app 	ITSO_time 	ITSO_unauthenticated 	ITSO_authenticated 	ITSO_recurrence 	ITSO_access_to_reports_1 	ITSO_access_to_reports_2 	ITSO_access_to_reports_3 	
	ITSO_access_to_reports_4 	ITSO_access_to_reports_5 	ITSO_notes
	*/

	$snCASUserURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sys_user?user_name=".$ritmObj["casUser"];  
	$snGetCASUser = snAPIQuery($snCASUserURL, $snUser, $snPass, $ritmObj["casUser"], "get");

	if ($snGetCASUser == "{\"result\":[]}") { 
		print "Somehow YOU were not found in Service-Now... This should never happen.  Halting.";
		exit;
	}

	$snCASUserJSONResponse = json_decode($snGetCASUser);
	$ritmObj["casUserSysID"] = $snCASUserJSONResponse->result[0]->sys_id;
	
	$jsonDataIns["sysparm_quantity"] = 1; // required
	
	$jsonDataIns["variables"]["ITSO_name"] = $ritmObj["scanRequestorSysID"];
	$jsonDataIns["variables"]["ITSO_department"] = $ritmObj["departmentSysID"];
	$jsonDataIns["variables"]["ITSO_systems"] = $ritmObj["scanTargetIPs"];
	$jsonDataIns["variables"]["ITSO_host_or_app"] = $ritmObj["scanHostApp"];
	$jsonDataIns["variables"]["ITSO_time"] = $ritmObj["preferredTime"];
	$jsonDataIns["variables"]["ITSO_unauthenticated"] = $ritmObj["scanIsUnauth"];
	$jsonDataIns["variables"]["ITSO_authenticated"] = $ritmObj["scanIsAuth"];
	$jsonDataIns["variables"]["ITSO_recurrence"] = $ritmObj["scanRecurrence"];
	$jsonDataIns["variables"]["ITSO_notes"] = $ritmObj["otherNotes"];

	// in case the recip list is empty...
	if ($ritmObj["scanRecipients"] == "") {
		$jsonDataIns["variables"]["ITSO_access_to_reports_1"] = $ritmObj["scanRequestorSysID"];
	}
	else {
		$recipientList = explode(",", $ritmObj["scanRecipients"]);
		$r = 1;
		foreach ($recipientList as $recipient) {
			$recipientPID = str_replace("@vt.edu", "", $recipient);
			//print "Recip PID: ".$recipientPID."<br/>";
			$snUserURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sys_user?user_name=".$recipientPID;  
			$snGetUser = snAPIQuery($snUserURL, $snUser, $snPass, $ritmObj["casUser"], "get");

			if ($snGetUser != "{\"result\":[]}") { 

				$snGetUserJSONResponse = json_decode($snGetUser);
				$jsonDataIns["variables"]["ITSO_access_to_reports_".$r] = $snGetUserJSONResponse->result[0]->sys_id;
				$r++;
			}
		}
	}	
	print "<br/>JSON Data: <br/>";
	print_r($jsonDataIns);
	print "<br/>JSON Data END<br/>";
	
	// DEBUG
	//	id=sc_cat_item&sys_id=5fa271330f3bb280b97f0bcce1050eee

	$snRitmURLInsert = "https://vt4help".$modeURL.".service-now.com/api/sn_sc/servicecatalog/items/5fa271330f3bb280b97f0bcce1050eee/add_to_cart"; 
	
	$jsonDataInsEnc = json_encode($jsonDataIns, JSON_FORCE_OBJECT);
	$snRitmInsert = snAPIQuery($snRitmURLInsert, $snUser, $snPass, $jsonDataInsEnc, "insert");

	$snRitmInsertJSONResponse = json_decode($snRitmInsert);
	
	$snRitmURLSubmit = "https://vt4help".$modeURL.".service-now.com/api/sn_sc/servicecatalog/cart/submit_order"; 
	$snRitmSubmit = snAPIQuery($snRitmURLSubmit, $snUser, $snPass, $jsonDataInsEnc, "insert");
	
	$snRitmSubmitJSONResponse = json_decode($snRitmSubmit);

	//print "DEBUG submit response: <br/>";
	//print_r($snRitmSubmitJSONResponse);
	//print "<br/>DEBUG END <br/>";

	// DEBUG
	//stdClass Object ( [error] => stdClass Object ( [message] => Cart is empty! [detail] => ) [status] => failure )
	
	unset($jsonDataIns);
	unset($jsonDataUpd);

	if (!isset($snRitmSubmitJSONResponse->error)) {
		foreach ($snRitmSubmitJSONResponse as $submitResult) {
			
			$reqItemNumber = $submitResult->request_number;
			$reqItemID = $submitResult->request_id;

			//print "DEBUG: submit result val: ".$reqItemNumber."<br/>";

			$snRITMListURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sc_req_item?sysparm_query=active=true&cat_item=5fa271330f3bb280b97f0bcce1050eee&request=".$reqItemID."&sysparm_display_value=all"; 
			$snRITMList = snAPIQuery($snRITMListURL, $snUser, $snPass, $ritmObj["casUser"], "get");
			$snRITMListJSONResponse = json_decode($snRITMList);
			
			//print "DEBUG snRITMList result: <br/>";
			//print_r($snRITMList);
			//print "<br/>DEBUG END <br/>";
			
			//$jsonDataUpd["comments"] = "Scan CREATED by ".$ritmObj["casUser"]."\n";
			$jsonDataUpd["work_notes"] = "Scan CREATED by ".$ritmObj["casUser"]."\n";
			$jsonDataUpd["sys_updated_by"] = $ritmObj["casUser"];	
			$jsonDataUpd["opened_by"] = $ritmObj["casUserSysID"];
			$jsonDataUpd["assignment_group"] = "bd5215e70f362900ee5a0bcce1050e1d";	// ITSO Group

			$jsonDataUpdEnc = json_encode($jsonDataUpd, JSON_FORCE_OBJECT);

			$reqItemSysID = "";
			$reqItemNumber = "";
			foreach ($snRITMListJSONResponse->result as $ritm) { 
				
				//print "DEBUG ritm result: <br/>";
				//print_r($ritm);
				$reqItemSysID = $ritm->sys_id->value; 
				$reqItemNumber = $ritm->number->value;
			}
			
			$snRITMUpdateURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sc_req_item/".$reqItemSysID;

			//print "<br/>DEBUG: URL is: ".$snRITMUpdateURL."<br/>ENC jsonDataUpdEnc is: <br/>";
			//print $jsonDataUpdEnc."<br/>";
			
			$snRITMUpdate = snAPIQuery($snRITMUpdateURL, $snUser, $snPass, $jsonDataUpdEnc, "update");

			$snUpdateJSONResponse = json_decode($snRITMUpdate);
			//print "<br/>DEBUG: Update response JSON is: <br/>".$snRITMUpdate."<br/>";

			if (isset($snUpdateJSONResponse->result->number)) {
				$snRITMUpdateNumber = $snUpdateJSONResponse->result->number;
				//print "<a href='https://vt4help".$modeURL.".service-now.com/sc_req_item.do?sys_id=".$reqItemSysID."&sysparm_view=ess&sysparm_record_target=sc_req_item'>".$snRITMUpdateNumber."</a> updated.<br/>"|CREATEDATA|.$reqItemNumber;
				
				// only auto-approve on live
				if ($modeURL != "dev") {
					approveScan($reqItemNumber, $ritmObj["casUser"], $ritmObj["casUserSysID"], $modeURL);
					print "<a href='https://vt4help".$modeURL.".service-now.com/sc_req_item.do?sys_id=".$reqItemSysID."&sysparm_view=ess&sysparm_record_target=sc_req_item'>".$snRITMUpdateNumber."</a> auto-approved.<br/>";
					print "|CREATEDATA|".$reqItemNumber."|CREATEDATA|".$reqItemSysID."|CREATEDATA|".$modeURL;
				}
				else {
					print "<a href='https://vt4help".$modeURL.".service-now.com/sc_req_item.do?sys_id=".$reqItemSysID."&sysparm_view=ess&sysparm_record_target=sc_req_item'>".$snRITMUpdateNumber."</a> created in DEV.<br/>";
					print "|CREATEDATA|".$reqItemNumber."|CREATEDATA|".$reqItemSysID."|CREATEDATA|".$modeURL;					
				}
			}
			else {
				print "Error updating RITM.<br/>|CREATEDATA|ERROR";
			}		
		}
	}
}

if (isset($_POST["formAction"]) && $_POST["formAction"] == "approvescan") {
	
	// Trim and sanitize posted variables...
	$casUserSysID = trim($_POST["casUserSysID"]);
	$casUserSysID = filter_var($casUserSysID, FILTER_SANITIZE_STRING);
	
    $casUser = trim($_POST["casUser"]);
	$casUser = filter_var($casUser, FILTER_SANITIZE_STRING);
	
	$jobID = trim($_POST["jobID"]);
	$jobID = filter_var($jobID, FILTER_SANITIZE_STRING);
	
	$userPermissions = trim($_POST["userPermissions"]);
	$userPermissions = filter_var($userPermissions, FILTER_SANITIZE_STRING);
	
	approveScan($jobID, $casUser, $casUserSysID, $modeURL);
}

if (isset($_POST["formAction"]) && $_POST["formAction"] == "rejectscan") {
	
	// Trim and sanitize posted variables...
	$casUserSysID = trim($_POST["casUserSysID"]);
	$casUserSysID = filter_var($casUserSysID, FILTER_SANITIZE_STRING);
	
    $casUser = trim($_POST["casUser"]);
	$casUser = filter_var($casUser, FILTER_SANITIZE_STRING);
	
	$jobID = trim($_POST["jobID"]);
	$jobID = filter_var($jobID, FILTER_SANITIZE_STRING);
	
	$userPermissions = trim($_POST["userPermissions"]);
	$userPermissions = filter_var($userPermissions, FILTER_SANITIZE_STRING);
	
	rejectScan($jobID, $casUser, $casUserSysID, $modeURL);
}

if (isset($_POST["formAction"]) && $_POST["formAction"] == "notifyrecipients") {
	
	// Trim and sanitize posted variables...
	$snRITM = trim($_POST["snRITM"]);
	$snRITM = filter_var($snRITM, FILTER_SANITIZE_STRING);
	
	$googleDriveURL = trim($_POST["googleDriveURL"]);
	$googleDriveURL = filter_var($googleDriveURL, FILTER_SANITIZE_STRING);
	
    $casUser = trim($_POST["casUser"]);
	$casUser = filter_var($casUser, FILTER_SANITIZE_STRING);
	
	$userPermissions = trim($_POST["userPermissions"]);
	$userPermissions = filter_var($userPermissions, FILTER_SANITIZE_STRING);
	
	notifyRecipients($snRITM, $googleDriveURL, $casUser, $modeURL);
}

if (isset($_POST["formAction"]) && $_POST["formAction"] == "createritm") {
	
	// Trim and sanitize posted variables...
	$ritmObj["scanRequestor"] = trim($_POST["scanRequestor"]);
	$ritmObj["scanRequestor"] = filter_var($ritmObj["scanRequestor"], FILTER_SANITIZE_STRING);
	
	$ritmObj["scanRequestorSysID"] = trim($_POST["scanRequestorSysID"]);
	$ritmObj["scanRequestorSysID"] = filter_var($ritmObj["scanRequestorSysID"], FILTER_SANITIZE_STRING);

	$ritmObj["departmentSysID"] = trim($_POST["departmentSysID"]);
	$ritmObj["departmentSysID"] = filter_var($ritmObj["departmentSysID"], FILTER_SANITIZE_STRING);
	
	$ritmObj["casUserSysID"] = trim($_POST["casUserSysID"]);
	$ritmObj["casUserSysID"] = filter_var($ritmObj["casUserSysID"], FILTER_SANITIZE_STRING);

	$ritmObj["scanCreator"] = trim($_POST["scanCreator"]);
	$ritmObj["scanCreator"] = filter_var($ritmObj["scanCreator"], FILTER_SANITIZE_STRING);
	
	$ritmObj["preferredTime"] = trim($_POST["preferredTime"]);
	$ritmObj["preferredTime"] = filter_var($ritmObj["preferredTime"], FILTER_SANITIZE_STRING);
	
	$ritmObj["scanHostApp"] = trim($_POST["scanHostApp"]);
	$ritmObj["scanHostApp"] = filter_var($ritmObj["scanHostApp"], FILTER_SANITIZE_STRING);
	
	$ritmObj["scanIsUnauth"] = trim($_POST["scanIsUnauth"]);
	$ritmObj["scanIsUnauth"] = filter_var($ritmObj["scanIsUnauth"], FILTER_SANITIZE_STRING);
	
	$ritmObj["scanIsAuth"] = trim($_POST["scanIsAuth"]);
	$ritmObj["scanIsAuth"] = filter_var($ritmObj["scanIsAuth"], FILTER_SANITIZE_STRING);
	
	$ritmObj["scanRecurrence"] = trim($_POST["scanRecurrence"]);
	$ritmObj["scanRecurrence"] = filter_var($ritmObj["scanRecurrence"], FILTER_SANITIZE_STRING);
	
	$ritmObj["scanTargetIPs"] = trim($_POST["scanTargetIPs"]);
	$ritmObj["scanTargetIPs"] = filter_var($ritmObj["scanTargetIPs"], FILTER_SANITIZE_STRING);
	
	$ritmObj["scanRecipients"] = trim($_POST["scanRecipients"]);
	$ritmObj["scanRecipients"] = filter_var($ritmObj["scanRecipients"], FILTER_SANITIZE_STRING);
	
	$ritmObj["otherNotes"] = trim($_POST["otherNotes"]);
	$ritmObj["otherNotes"] = filter_var($ritmObj["otherNotes"], FILTER_SANITIZE_STRING);	
	
	$ritmObj["casUser"] = trim($_POST["casUser"]);
	$ritmObj["casUser"] = filter_var($ritmObj["casUser"], FILTER_SANITIZE_STRING);
	
	$ritmObj["userPermissions"] = trim($_POST["userPermissions"]);
	$ritmObj["userPermissions"] = filter_var($ritmObj["userPermissions"], FILTER_SANITIZE_STRING);
	
	createRITM($ritmObj, $modeURL);
}
	
if (isset($_POST["formAction"]) && $_POST["formAction"] == "getfolderdest") {

	// use LIVE or DEV API URLs?

	// Trim and sanitize posted variables...
	$scanRequestor = trim($_POST["scanRequestor"]);
	$scanRequestor = filter_var($scanRequestor, FILTER_SANITIZE_STRING);
	
    $casUser = trim($_POST["casUser"]);
	$casUser = filter_var($casUser, FILTER_SANITIZE_STRING);
	
	$userPermissions = trim($_POST["userPermissions"]);
	$userPermissions = filter_var($userPermissions, FILTER_SANITIZE_STRING);
	
	/////////////////////////////////////////////////////////////
	//
	//	Get User Info
	//
	/////////////////////////////////////////////////////////////

		include("sn_info.inc.php");

		$snCASUserURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sys_user?user_name=".$casUser; 

		if ($mode == "DEV") { $logMode = "DEV"; } else { $logMode = ""; }

		$callerNotFound = 0;

		$snGetCASUser = snAPIQuery($snCASUserURL, $snUser, $snPass, $casUser, "get");

		//print "<br/>DEBUG: snGetCASUser is: ".$snGetCASUser."<br/>";


		if ($snGetCASUser == "{\"result\":[]}") { 
			print "Somehow YOU were not found in Service-Now... This should never happen.  Halting.";
			exit;
		}

		$snCASUserJSONResponse = json_decode($snGetCASUser);

		$snCASUserID = $snCASUserJSONResponse->result[0]->sys_id;
		$snCASUserPhone = $snCASUserJSONResponse->result[0]->phone;

		// GET ITSO USER value
		//print "DEBUG: ITSO User is: ".$casUser."<br/>";

		$snCallerURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/sys_user?user_name=".$scanRequestor; 

		if ($mode == "DEV") { $logMode = "DEV"; } else { $logMode = ""; }

		$snGetCaller = snAPIQuery($snCallerURL, $snUser, $snPass, $scanRequestor, "get");

		//print "<br/>DEBUG: snGetCaller is: ".$snGetCaller."<br/>";

		// if we get an empty result...
		if ($snGetCaller == "{\"result\":[]}") { 

			//print "<br/>Caller value was not found.  Setting the Caller value to ".$casUser."<br/>"; 
			$callerNotFound = 1;
			$snGetCaller = $snGetCASUser;
		}

		$snGetCallerJSONResponse = json_decode($snGetCaller);

		//print "DEBUG: <br/>";
		//var_dump($snGetCallerJSONResponse);
		//print "<br/>";
	
		// u_department_number string
		$snCallerID = $snGetCallerJSONResponse->result[0]->sys_id;
		$snCallerDeptID = $snGetCallerJSONResponse->result[0]->department->value;

		//print "<br/>DEBUG: SN CAS UserID is: ".$snCASUserID."<br/>";
		//print "<br/>DEBUG: SN Caller UserID is: ".$snCallerID."<br/>";
		//print "<br/>DEBUG: SN Caller DeptID is: ".$snCallerDeptID."<br/>";

		// Get DEPARTMENT value

		if ($mode == "DEV") { $snDeptURL = "https://vt4help".$modeURL.".service-now.com/api/now/table/cmn_department?sys_id=".$snCallerDeptID; } 
		else { $snDeptURL = "https://vt4help.service-now.com/api/now/table/cmn_department?sys_id=".$snCallerDeptID; } 

		if ($mode == "DEV") { $logMode = "DEV"; } else { $logMode = ""; }

		$snGetCallerDept = snAPIQuery($snDeptURL, $snUser, $snPass, $scanRequestor, "get");
	
		// if we get an empty result...
		if ($snGetCallerDept == "{\"result\":[]}") { 

			//print "<br/>Caller Department value was not found.  Setting the Caller Department value to 'General Scans'<br/>"; 
			$callerDeptNotFound = 1;
			$snGetCallerDept = "General_Scans";
		}

		$snGetCallerDeptJSONResponse = json_decode($snGetCallerDept);
	
		//print "<br/>DEBUG: snGetCallerDept is: ";
		//print "<textarea>".$snGetCallerDept."</textarea>";
		//print "<br/>";

		// Store the SN folder name sys_id in case we have to perform a SN lookup later...
		$departmentSysID = trim($snGetCallerDeptJSONResponse->result[0]->sys_id);
		$departmentSysID = filter_var($departmentSysID, FILTER_SANITIZE_STRING);

		// Sanitize and remove characters that Nessus will choke on
	
		$departmentName = trim($snGetCallerDeptJSONResponse->result[0]->name);
		$departmentName = preg_replace("([^\w\s\d\-_\[\].])", '', $departmentName);
		$departmentName = preg_replace("([\.]{2,})", '', $departmentName);
		$departmentName = str_replace(" ","_",$departmentName);

		//print "<br/>DEBUG: snGetCallerDept Name is: ".$snGetCallerDeptJSONResponse->result[0]->name.", Dept Name is:".$departmentName."<br/>";

		print $callerNotFound."|DATA|".$departmentName."|DATA|".$departmentSysID."|DATA|".$snCallerID."|DATA|".$snCASUserID;
	
} // end scanRequestor isset

// EOF

?>