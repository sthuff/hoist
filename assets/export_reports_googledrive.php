<?php

/****************************************************************************

	WORKER SCRIPT TO HANDLE NESSUS REPORT AUTO-EXPORT TO GOOGLE DRIVE

****************************************************************************/

	// SERVER TIMEZONE REQUIRED FOR DATE FUNCTIONS
	date_default_timezone_set('UTC');

	// PHP ERROR REPORTING LEVEL
	error_reporting(E_ALL); 
	ini_set('display_errors', '1');

	// include the Nessus API functions
	include_once("nessus_api_funcs.inc.php");

	include("db_info.inc.php");
	$dbName = "hoist";

	// DEBUG
	$nowDate = date("Y-m-d H:i:s T");
	print "\n\n====  BEGIN EXPORT PROCESS ===== ".$nowDate."<br/> \n";

	//print "<br/>DEBUG: Server var is: ";
	//print_r($_SERVER);
	//print "<br/>";

	$scanList = nessusAPIQuery("scans", "get", "");
	$scanListObj = json_decode($scanList);

	if (isset($scanListObj->scans)) {
		foreach ($scanListObj->scans as $scanItem) {
			if ($scanItem->status == "completed") {
				$nessListInfo["scanLastModified"] = $scanItem->last_modification_date;	
				$nessListInfo["scanID"] = $scanItem->id;
				
				$latestScanModifiedDB = "";
				
				try {
					$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
					$statement = $connection->prepare("SELECT scanID, scanCreator, scanRequestor, scanRecipients, scanName, snRITM, reportFormat, googleFolderName, scanAutoExport, scanAutoNotify FROM scans WHERE scanID = :scanID AND scanAutoExport = 1");
					$statement->execute(array(':scanID' => $nessListInfo["scanID"]));
					
					if ($statement->rowCount() > 0) {
						
						$rows = $statement->fetchAll(PDO::FETCH_ASSOC);

						foreach ($rows as $scanRow) {
							
							$scanDBInfo["scanID"] = $scanRow["scanID"];
							$scanDBInfo["scanCreator"] = $scanRow["scanCreator"];
							$scanDBInfo["scanRequestor"] = $scanRow["scanRequestor"];
							$scanDBInfo["scanRecipients"] = $scanRow["scanRecipients"];

							$scanDBInfo["googleFolderName"] = $scanRow["googleFolderName"];
							$scanDBInfo["googleFolderName"] = preg_replace("([^\w\s\d\-_\[\].])", '', $scanDBInfo["googleFolderName"]);
							$scanDBInfo["googleFolderName"] = preg_replace("([\.]{2,})", '', $scanDBInfo["googleFolderName"]);
							$scanDBInfo["googleFolderName"] = str_replace(" ", "_", $scanDBInfo["googleFolderName"]);
							
							$scanDBInfo["scanName"] = $scanRow["scanName"];
							$scanDBInfo["snRITM"] = $scanRow["snRITM"];
							$scanDBInfo["reportFormat"] = $scanRow["reportFormat"];
							$scanDBInfo["scanReportGoogle"] = 1;
							$scanDBInfo["scanAutoExport"] = $scanRow["scanAutoExport"];
							$scanDBInfo["scanAutoNotify"] = $scanRow["scanAutoNotify"];
						
							try {
								$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
								$statement = $connection->prepare("SELECT histID, scanID, scanLastModified, googleReportID FROM scanhistory WHERE scanID = :scanID ORDER BY scanLastModified DESC LIMIT 1");
								$statement->execute(array(':scanID' => $nessListInfo["scanID"]));

								//print "ScanID is: ".$nessListInfo["scanID"]."<br/>\n";

								if ($statement->rowCount() > 0) {
									$histRows = $statement->fetchAll(PDO::FETCH_ASSOC);

									//print "DEBUG: histRows is: <br/>\n";
									//var_dump($histRows);
									//print "<br/>\n";
									
									foreach ($histRows as $scanHistDBRow) {
										$latestScanModifiedDB = $scanHistDBRow["scanLastModified"];
									} // end scanhistory foreach
								} // end if scanhist results exist
								else {
/****************************************************************************************************************************************

									// a scan history item exists in Nessus but the DB has no entry for current scanID so let's generate a report
									
****************************************************************************************************************************************/
									
									print "A Scan History item exists in NESSUS but the scanhistory DB has no entry for scan (".$nessListInfo["scanID"].").  Inserting it and generating report.\n";
									
									$scanInfoJSON = nessusAPIQuery("scans/".$nessListInfo["scanID"], "get", "");
									$scanInfo = json_decode($scanInfoJSON);

									// this should be a redundant completeness check from the scanlist
									if ($scanInfo->info->status == "completed") {

										foreach ($scanInfo->history as $scanHistItem) {
											$nessHistInfo["histID"] = $scanHistItem->history_id; 
											$nessHistInfo["scanLastModified"] = $scanHistItem->last_modification_date;

											$epochStart = $scanHistItem->creation_date;
											$scanStartDate = new DateTime("@$epochStart");
											$scanStartDate->setTimeZone(new DateTimeZone('America/New_York'));
											$scanDBInfo["scanStartDate"] = $scanStartDate->format('Y-m-d_H_i_s');
											
											// add missing items to scanDBInfo array so that the generateNessusReport() function can be used here
											$scanDBInfo["histID"] = $nessHistInfo["histID"];
											$scanDBInfo["scanLastModified"] = $nessHistInfo["scanLastModified"];

											// generate report and update scanhistory
											$reportFileID = generateNessusReport($scanDBInfo, "autoExport");

											// only notify if AutoNotify is enabled AND the history item we just added is the scan's latest report
											if ($scanDBInfo["scanAutoNotify"] == 1 && $nessHistInfo["scanLastModified"] == $nessListInfo["scanLastModified"]) {

												include_once("servicenow_funcs.php");

												// force DEV mode if we're on a dev instance...
												if (strstr($_SERVER["PWD"], "/hoistdev")) { 
													$mode = "DEV"; 
													$modeURL = "dev"; 
													print "Using DEVELOP instance of SN.\n"; 
												} 
												else { 
													$mode = "LIVE"; 
													$modeURL = ""; 
													print "Using LIVE instance of SN.\n"; 
												}

												$googleDriveURL = "https://drive.google.com/uc?export=download&id=".trim($reportFileID);

												print "Auto-Notification Enabled.\nSending Google Drive link: ".$googleDriveURL."\nService-Now ".$mode.": ".$scanDBInfo["snRITM"]."\n\n";

												notifyRecipients($scanDBInfo["snRITM"], $googleDriveURL, "itso_user", $modeURL);

											}
											else {
													print "\nSkipping notification, Hist Last Modified: (".$nessHistInfo["scanLastModified"]."), Scan Last Modified: (".$nessListInfo["scanLastModified"].")<br>\n";	
											}
										} // end foreach scan history item
									} // redundant nessus complete status
								} // end if else db item not found					
							} // end try scanhistory
							catch(PDOException $e) { print "Error: ".$e->getMessage(); }
							
/****************************************************************************************************************************************

							// Test to see if the scan item in Nessus is newer than what's in the DB 
									
****************************************************************************************************************************************/							
							//print "DEBUG: Scan (".$nessListInfo["scanID"].") - IS DB (".$latestScanModifiedDB." LT ".$nessListInfo["scanLastModified"].") NESSUS scanLastModified?<br/>\n";
										
							if ($latestScanModifiedDB != "" && $latestScanModifiedDB < $nessListInfo["scanLastModified"]) {

								print "DEBUG: Scan (".$nessListInfo["scanID"].") - Nessus scan contains a newer history item than the DB...<br/>\n";

								$scanInfoJSON = nessusAPIQuery("scans/".$nessListInfo["scanID"], "get", "");
								$scanInfo = json_decode($scanInfoJSON);

								// this should be a redundant completeness check from the scanlist
								if ($scanInfo->info->status == "completed") {

									foreach ($scanInfo->history as $scanHistItem) {
										$nessHistInfo["histID"] = $scanHistItem->history_id; 
										$nessHistInfo["scanLastModified"] = $scanHistItem->last_modification_date;

										$epochStart = $scanHistItem->creation_date;
										$scanStartDate = new DateTime("@$epochStart");
										$scanStartDate->setTimeZone(new DateTimeZone('America/New_York'));
										$scanDBInfo["scanStartDate"] = $scanStartDate->format('Y-m-d_H_i_s');

										include("db_info.inc.php");
										$dbName = "hoist";

										try {
											$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
											$statement = $connection->prepare("SELECT histID, scanID, scanLastModified, googleReportID FROM scanhistory WHERE histID = :histID");
											$statement->execute(array(':histID' => $nessHistInfo["histID"]));

											if ($statement->rowCount() > 0) {
												$histRowsDB = $statement->fetchAll(PDO::FETCH_ASSOC);
												print "DB histID ".$nessHistInfo["histID"]." already exists, skipping...<br/>\n";
											} // end if scanhist results exist
											else {
										
												// add missing items to scanDBInfo array so that the generateNessusReport() function can be used here
												$scanDBInfo["histID"] = $nessHistInfo["histID"];
												$scanDBInfo["scanLastModified"] = $nessHistInfo["scanLastModified"];

												// generate report and update scanhistory
												$reportFileID = generateNessusReport($scanDBInfo, "autoExport");

												// only notify if AutoNotify is enabled AND the history item we just added is the scan's latest report
												if ($scanDBInfo["scanAutoNotify"] == 1 && $nessHistInfo["scanLastModified"] == $nessListInfo["scanLastModified"]) {

													include_once("servicenow_funcs.php");

													// force DEV mode if we're on a dev instance...
													if (strstr($_SERVER["PWD"], "/hoistdev")) { 
														$mode = "DEV"; 
														$modeURL = "dev"; 
														print "Using DEVELOP instance of SN.\n"; 
													} 
													else { 
														$mode = "LIVE"; 
														$modeURL = ""; 
														print "Using LIVE instance of SN.\n"; 
													}

													$googleDriveURL = "https://drive.google.com/uc?export=download&id=".trim($reportFileID);

													print "Auto-Notification Enabled.\nSending Google Drive link: ".$googleDriveURL."\nService-Now ".$modeURL."(".$mode."): ".$scanDBInfo["snRITM"]."\n\n";

													notifyRecipients($scanDBInfo["snRITM"], $googleDriveURL, "itso_user", $modeURL);

												} // end if scanAutoNotify
												else {
													print "\nSkipping notification, Hist Last Modified: (".$nessHistInfo["scanLastModified"]."), Scan Last Modified: (".$nessListInfo["scanLastModified"].")<br>\n";	
												}
											}
										} // end try scanhist histID exists
										catch(PDOException $e) { print "Error: ".$e->getMessage(); }
									} // end foreach scanhist item
								} // end if nessus scan item completed
							} // end if db lastmodified < ness lastmodified
							else {
								//print "DEBUG: Scan: (".$nessListInfo["scanID"].") - No new reports to export.<br/><br/>\n\n";
							}
	
							
						} // end if scans foreach
					} // end if scans results exist
				} // end try scans
				catch(PDOException $e) { print "Error: ".$e->getMessage(); }
			} // end if nessus status completed
			else {
				print "Scan (".$scanItem->id.") status is: ".$scanItem->status."<br/>\n";
			}
		} // end nessus scans foreach
	} // end if nessus scans exist

	$nowDate = date("Y-m-d H:i:s T");
	print "\n====  END EXPORT PROCESS ===== ".$nowDate."<br/>\n";

	// clear the connection
	unset($dbUser);
	unset($dbPass);
	$connection = null;
?>