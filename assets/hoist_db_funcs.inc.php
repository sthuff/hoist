<?php

function insertScanDB($scanDBInfo) {
	// Update Scan DB

	include("./db_info.inc.php");
	$dbName = "hoist";

	try {
		$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
		$statement = $connection->prepare("INSERT INTO scans (scanID, scanType, scanCreator, scanRequestor, scanRecipients, scanStartDate, scanName, snRITM, scanIsUnauth, scanIsAuth, scanHostApp, scanTargetIPs, reportFormat, scanFolderID, scanFolderName, googleFolderName, scanAutoExport, scanAutoNotify) VALUES (:scanID, :scanType, :scanCreator, :scanRequestor, :scanRecipients, :scanStartDate, :scanName, :snRITM, :scanIsUnauth, :scanIsAuth, :scanHostApp, :scanTargetIPs, :reportFormat, :scanFolderID, :scanFolderName, :googleFolderName, :scanAutoExport, :scanAutoNotify)");

		$statement->execute(array("scanID" => $scanDBInfo["scanIDCreated"], "scanType" => $scanDBInfo["scanType"], "scanCreator" => $scanDBInfo["scanCreator"], "scanRequestor" => $scanDBInfo["scanRequestor"], "scanRecipients" => $scanDBInfo["scanRecipients"], "scanStartDate" => $scanDBInfo["dateTimeSchedScanFormatObj"]->format('Y-m-d H:i:s'), "scanName" => $scanDBInfo["scanName"], "snRITM" => $scanDBInfo["snRITM"], "scanIsUnauth" => $scanDBInfo["scanIsUnauth"], "scanIsAuth" => $scanDBInfo["scanIsAuth"], "scanHostApp" => $scanDBInfo["scanHostApp"], "scanTargetIPs" => $scanDBInfo["scanTargetIPs"], "reportFormat" => $scanDBInfo["reportFormat"], "scanFolderID" => $scanDBInfo["scanFolderID"], "scanFolderName" => $scanDBInfo["scanFolderName"], "googleFolderName" => $scanDBInfo["googleFolderName"], "scanAutoExport" => $scanDBInfo["scanAutoExport"], "scanAutoNotify" => $scanDBInfo["scanAutoNotify"]));
	}
	catch(PDOException $e) { $scanDBInfo["debugMsg"] = "Error: ".$e->getMessage(); }

	// Log the action

	try {
		$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
		unset($dbUser);
		unset($dbPass);
		$statement = $connection->prepare("INSERT INTO logs (logUser, logType, logDataID, logInfo) VALUES (:logUser, :logType, :logDataID, :logInfo)");
		$statement->execute(array("logUser" => $scanDBInfo["scanCreator"], "logType" => $scanDBInfo["scanType"]."createScan", "logDataID" => $scanDBInfo["scanIDCreated"], "logInfo" => $scanDBInfo["scanDescriptionString"]));
	}
	catch(PDOException $e) { $scanDBInfo["debugMsg"] = "Error: ".$e->getMessage(); }

	// clear the connection
	$connection = null;	
	
	return $scanDBInfo["debugMsg"];
}

function insertMissingScanDetails($scanDBInfo) {
	
	include("db_info.inc.php");
	$dbName = "hoist";

	try {
		$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
		$statement = $connection->prepare("INSERT INTO scans (scanID, scanCreator, scanRequestor, scanRecipients, scanStartDate, scanName, snRITM, scanIsUnauth, scanIsAuth, scanHostApp, scanTargetIPs, reportFormat, scanFolderID, scanFolderName, googleFolderName, scanAutoExport, scanAutoNotify) VALUES (:scanID, :scanCreator, :scanRequestor, :scanRecipients, :scanStartDate, :scanName, :snRITM, :scanIsUnauth, :scanIsAuth, :scanHostApp, :scanTargetIPs, :reportFormat, :scanFolderID, :scanFolderName, :googleFolderName, :scanAutoExport, :scanAutoNotify)");

		$statement->execute(array("scanID" => $scanDBInfo["scanID"], "scanCreator" => $scanDBInfo["scanCreator"], "scanRequestor" => $scanDBInfo["scanRequestor"], "scanRecipients" => $scanDBInfo["scanRecipients"], "scanStartDate" => $scanDBInfo["scanStartDate"], "scanName" => $scanDBInfo["scanName"], "snRITM" => $scanDBInfo["snRITM"], "scanIsUnauth" => $scanDBInfo["scanIsUnauth"], "scanIsAuth" => $scanDBInfo["scanIsAuth"], "scanHostApp" => $scanDBInfo["scanHostApp"], "scanTargetIPs" => $scanDBInfo["scanTargetIPs"], "reportFormat" => $scanDBInfo["reportFormat"], "scanFolderID" => $scanDBInfo["scanFolderID"], "scanFolderName" => $scanDBInfo["scanFolderName"], "googleFolderName" => $scanDBInfo["googleFolderName"], "scanAutoExport" => $scanDBInfo["scanAutoExport"], "scanAutoNotify" => $scanDBInfo["scanAutoNotify"]));
	}
	catch(PDOException $e) { $scanDBInfo["debugMsg"] = "Error: ".$e->getMessage(); }

	// Log the action

	try {
		$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
		unset($dbUser);
		unset($dbPass);
		$statement = $connection->prepare("INSERT INTO logs (logUser, logType, logDataID, logInfo) VALUES (:logUser, :logType, :logDataID, :logInfo)");
		$statement->execute(array("logUser" => $scanCreator, "logType" => "createScanDetails", "logDataID" => $scanID, "logInfo" => $scanDBInfo["scanDescriptionString"]));
	}
	catch(PDOException $e) { $scanDBInfo["debugMsg"] = "Error: ".$e->getMessage(); }

	// clear the connection
	$connection = null;	
	
	return $scanDBInfo["debugMsg"];
}

function updateMissingScanDetails($scanDBInfo) {

	include("db_info.inc.php");
	$dbName = "hoist";

	try {
		$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
		
		$statement = $connection->prepare("UPDATE scans SET scanCreator = :scanCreator, scanRequestor = :scanRequestor, scanRecipients = :scanRecipients, scanStartDate = :scanStartDate, scanName = :scanName, snRITM = :snRITM, scanIsUnauth = :scanIsUnauth, scanIsAuth = :scanIsAuth, scanHostApp = :scanHostApp, scanTargetIPs = :scanTargetIPs, reportFormat = :reportFormat, googleFolderName = :googleFolderName, scanAutoExport = :scanAutoExport, scanAutoNotify = :scanAutoNotify WHERE scanID = :scanID");
		
		$statement->bindValue(":scanCreator", $scanDBInfo["scanCreator"], PDO::PARAM_STR);
		$statement->bindValue(":scanRequestor", $scanDBInfo["scanRequestor"], PDO::PARAM_STR);
		$statement->bindValue(":scanRecipients", $scanDBInfo["scanRecipients"], PDO::PARAM_STR);
		$statement->bindValue(":scanStartDate", $scanDBInfo["scanStartDate"], PDO::PARAM_STR);
		$statement->bindValue(":scanName", $scanDBInfo["scanName"], PDO::PARAM_STR);
		$statement->bindValue(":snRITM", $scanDBInfo["snRITM"], PDO::PARAM_STR);
		$statement->bindValue(":scanIsUnauth", $scanDBInfo["scanIsUnauth"], PDO::PARAM_INT);
		$statement->bindValue(":scanIsAuth", $scanDBInfo["scanIsAuth"], PDO::PARAM_INT);
		$statement->bindValue(":scanHostApp", $scanDBInfo["scanHostApp"], PDO::PARAM_STR);
		$statement->bindValue(":scanTargetIPs", $scanDBInfo["scanTargetIPs"], PDO::PARAM_STR);
		$statement->bindValue(":reportFormat", $scanDBInfo["reportFormat"], PDO::PARAM_STR);
		$statement->bindValue(":googleFolderName", $scanDBInfo["googleFolderName"], PDO::PARAM_STR);
		$statement->bindValue(":scanAutoExport", $scanDBInfo["scanAutoExport"], PDO::PARAM_INT);
		$statement->bindValue(":scanAutoNotify", $scanDBInfo["scanAutoNotify"], PDO::PARAM_INT);
		$statement->bindValue(":scanID", $scanDBInfo["scanID"], PDO::PARAM_INT); 						
		$update = $statement->execute();
	}
	catch(PDOException $e) { $scanDBInfo["debugMsg"] = "Error: ".$e->getMessage(); }

	// Log the action

	try {
		$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
		unset($dbUser);
		unset($dbPass);
		$statement = $connection->prepare("INSERT INTO logs (logUser, logType, logDataID, logInfo) VALUES (:logUser, :logType, :logDataID, :logInfo)");
		$statement->execute(array("logUser" => $scanDBInfo["scanCreator"], "logType" => "updateScan", "logDataID" => $scanDBInfo["scanID"], "logInfo" => $scanDBInfo["scanCreator"]."||".$scanDBInfo["scanRequestor"]."||".$scanDBInfo["googleFolderName"]."||".$scanDBInfo["scanAutoExport"]));
	}
	catch(PDOException $e) { $scanDBInfo["debugMsg"] = "Error: ".$e->getMessage(); }

	// clear the connection
	$connection = null;
	
	return $scanDBInfo["debugMsg"];
}

function getLatestReportWithGoogleReportID($histID) {
		
	include("./assets/db_info.inc.php");
	$dbName = "hoist";
	
	$googleReportID = 0;
	
	//print "histID in: ".$histID."<br/>";
	
	try {
		$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
		unset($dbUser);
		unset($dbPass);

		$statement = $connection->prepare('SELECT googleReportID FROM scanhistory WHERE histID = :histID');
		$statement->execute(array('histID' => $histID));

		if ($statement->rowCount() > 0) {

			$histrows = $statement->fetchAll(PDO::FETCH_ASSOC);

			foreach ($histrows as $scanHistRow) {
				$googleReportID = $scanHistRow["googleReportID"];
			}
		}
	}
	catch(PDOException $e) { print "Error: ".$e->getMessage(); }
	
	//print "in: ".$googleReportID."<br/>";
	
	return $googleReportID;
}

function checkRITMScanScheduled($snRITM) {
		
	include("./assets/db_info.inc.php");
	$dbName = "hoist";
	
	$scanRITMScheduled = 0;
	$scanID = "";

	try {
		$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
		unset($dbUser);
		unset($dbPass);

		$statement = $connection->prepare("SELECT sID, scanID, snRITM, googleReportID FROM scans WHERE snRITM = :snRITM");
		$statement->execute(array(':snRITM' => $snRITM));

		if ($statement->rowCount() > 0) {

			$rows = $statement->fetchAll(PDO::FETCH_ASSOC);

			//print "<br/>DEBUG: rows:";
			//var_dump($rows);
			//print "<br/>";
			
			foreach ($rows as $row) {
				$scanID = $row["scanID"];	
			}
			
			$scanRITMScheduled = 1;

		}
		else { $scanRITMScheduled = 0; }
		
		return $scanRITMScheduled."||".$scanID;
	}
	catch(PDOException $e) {
		print "Error: ".$e->getMessage();
	}
}

function updateGoogleReportID($scanID, $histID, $scanLastModified, $scanCreator, $googleReportID, $exportType) {
	
		// Update Scan DB

		include("db_info.inc.php");
		$dbName = "hoist";

		try {
			$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
			$statement = $connection->prepare("UPDATE scans SET googleReportID = :googleReportID WHERE scanID = :scanID");
			$statement->bindValue(":googleReportID", $googleReportID, PDO::PARAM_STR); 
			$statement->bindValue(":scanID", $scanID, PDO::PARAM_INT); 						
			$update = $statement->execute();
		}
		catch(PDOException $e) { print "Error: ".$e->getMessage(); }
	
		try {
			$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
			$statement = $connection->prepare('SELECT histID FROM scanhistory WHERE histID = :histID LIMIT 1');
			$statement->execute(array('histID' => $histID));

			if ($statement->rowCount() > 0) {

				try {
					$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
					$statement = $connection->prepare("UPDATE scanhistory SET scanID = :scanID, scanLastModified = :scanLastModified, googleReportID = :googleReportID WHERE histID = :histID");
					$statement->bindValue(":scanID", $scanID, PDO::PARAM_INT);
					$statement->bindValue(":scanLastModified", $scanLastModified, PDO::PARAM_INT); 
					$statement->bindValue(":googleReportID", $googleReportID, PDO::PARAM_STR); 
					$statement->bindValue(":histID", $histID, PDO::PARAM_INT);
					$update = $statement->execute();
				}
				catch(PDOException $e) { print "Error: ".$e->getMessage(); }

			}
			else {
				try {
					$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
					$statement = $connection->prepare("INSERT INTO scanhistory (histID, scanID, scanLastModified, googleReportID) VALUES (:histID, :scanID, :scanLastModified, :googleReportID)");
					$statement->execute(array("histID" => $histID, "scanID" => $scanID, "scanLastModified" => $scanLastModified, "googleReportID" => $googleReportID));
				}
				catch(PDOException $e) { print "Error: ".$e->getMessage(); }
			}
		}
		catch(PDOException $e) { print "Error: ".$e->getMessage(); }

		// Log the action

		try {
			$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
			unset($dbUser);
			unset($dbPass);
			$statement = $connection->prepare("INSERT INTO logs (logUser, logType, logDataID, logInfo) VALUES (:logUser, :logType, :logDataID, :logInfo)");
			$statement->execute(array("logUser" => $scanCreator, "logType" => "updateScan", "logDataID" => $scanID, "logInfo" => $googleReportID.",".$histID.",".$scanLastModified));
		}
		catch(PDOException $e) { print "Error: ".$e->getMessage(); }

		// clear the connection
		$connection = null;
}

?>