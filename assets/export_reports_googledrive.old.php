<?php

/****************************************************************************

	WORKER SCRIPT TO HANDLE NESSUS REPORT AUTO-EXPORT TO GOOGLE DRIVE

****************************************************************************/

	// SERVER TIMEZONE REQUIRED FOR DATE FUNCTIONS
	date_default_timezone_set('UTC');

	// PHP ERROR REPORTING LEVEL
	error_reporting(E_ALL); 
	ini_set('display_errors', '1');

	// include the Nessus API functions
	include_once("nessus_api_funcs.inc.php");

	include("db_info.inc.php");
	$dbName = "hoist";

	// DEBUG
	$nowDate = date("Y-m-d H:i:s T");
	print "\n\n====  BEGIN EXPORT PROCESS ===== ".$nowDate."<br/> \n";

	//print "<br/>DEBUG: Server var is: ";
	//print_r($_SERVER);
	//print "<br/>";

	try {
		$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
		unset($dbUser);
		unset($dbPass);

		$statement = $connection->prepare("SELECT scanID, scanCreator, scanRequestor, scanName, snRITM, scanFolderID, scanFolderName, googleFolderName, googleReportID, scanAutoExport, scanAutoNotify FROM scans WHERE googleReportID IS NULL AND scanAutoExport = 1");
		$statement->execute();

		if ($statement->rowCount() > 0) {

			// set some defaults
			$scanID = "NONE";
			$scanCreator = "NONE";
			$scanRequestor = "NONE";
			$googleFolderName = "General Scans";
			$scanName = "UNNAMED_SCAN";
			$snRITM = "";
			$scanReportGoogle = 1;
			$debugMsg = "DEBUG: ";
			$scanAutoExport = 0;
			$scanAutoNotify = 0;
			
			$rows = $statement->fetchAll(PDO::FETCH_ASSOC);

			foreach ($rows as $scanRow) {
				//var_dump($scanRow);
				print "DEBUG: scanID: ".$scanRow["scanID"].", googleFolderName: ".$scanRow["googleFolderName"]."<br/>\n";
				
				$scanInfoJSON = nessusAPIQuery("scans/".$scanRow["scanID"], "get", "");
		
				$scanInfo = json_decode($scanInfoJSON);
				//print "<br/>DEBUG: scanInfo is: <br/>";
				//var_dump($scanInfo);
				//print "<br/><br/>";
				
				$scanID = $scanRow["scanID"];
				$scanCreator = $scanRow["scanCreator"];
				$scanRequestor = $scanRow["scanRequestor"];
				
				$googleFolderName = $scanRow["googleFolderName"];
				$googleFolderName = preg_replace("([^\w\s\d\-_\[\].])", '', $googleFolderName);
				$googleFolderName = preg_replace("([\.]{2,})", '', $googleFolderName);
				$googleFolderName = str_replace(" ", "_", $googleFolderName);

				$scanName = $scanRow["scanName"];
				$snRITM = $scanRow["snRITM"];
				$scanReportGoogle = 1;
				$scanAutoExport = $scanRow["scanAutoExport"];
				$scanAutoNotify = $scanRow["scanAutoNotify"];

				print "Checking NESSUS Status of reports...<br/>\n";
				
				if (isset($scanInfo->info->status) && $scanInfo->info->status == "completed") {
					print "Found Nessus Report that is COMPLETED...<br/>\n";

					if (isset($scanInfo->info->scan_start)) {
						$epochStart = $scanInfo->info->scan_start;
						$scanStartDate = new DateTime("@$epochStart");
						$scanStartDate->setTimeZone(new DateTimeZone('America/New_York'));
						
						//print "Start date is: ".$scanStartDate->format('Y-m-d_H_i_s')."<br/>\n";

					}

					if (isset($scanInfo->info->scan_end)) {
						$epochEnd = $scanInfo->info->scan_end; 
						$scanEndDate = new DateTime("@$epochEnd");
						$scanEndDate->setTimeZone(new DateTimeZone('America/New_York'));
						
						//print "End date is: ".$scanEndDate->format('Y-m-d_H_i_s')."<br/>\n";
					}

					if (isset($scanStartDate) && isset($scanEndDate)) {
						$scanReportFilename = trim($scanStartDate->format('Y-m-d_H_i_s')."_".$scanName."_".$scanID);
						$scanReportFilename = filter_var($scanReportFilename, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
					}
					else {
						$scanErrorDate = new DateTime();
						$scanErrorDate->setTimeZone(new DateTimeZone('America/New_York'));
						
						$scanReportFilename = trim($scanErrorDate->format('Y-m-d_H_i_s')."_".$scanName."_".$scanID);
						$scanReportFilename = filter_var($scanReportFilename, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);						
						print "Error date filename is: ".$scanReportFilename."<br/>\n";
					}

					$scanData["scan_id"] = $scanID;
					$scanData["format"] = "html"; // nessus, csv, db, html, pdf are valid options
					$scanData["chapters"] = "vuln_by_host"; // vuln_hosts_summary, vuln_by_host, compliance_exec, remediations, vuln_by_plugin, compliance

					$jsonData = json_encode($scanData);

					print "Google Filename will be (scanReportFilename): ".$scanReportFilename."<br/>\n";
					
					$action = "scans/".$scanID."/export";
					$exportScanJSON = nessusAPIQuery($action, "export", $jsonData);
					$exportScan = json_decode($exportScanJSON);
					
					$processing = 1;

					print "Beginning Export process on NESSUS, this may take a bit... <br/>\n";

					while ($processing == 1) {

						$action = "scans/".$scanID."/export/".$exportScan->file."/status";
						$scanDLStatusJSON = nessusAPIQuery($action, "get", "");
						$scanDLStatus = json_decode($scanDLStatusJSON);

						if ($scanDLStatus->status == "ready") {

							print "Report READY for export...<br/>\n";

							$action = "scans/".$scanID."/export/".$exportScan->file."/download";
							$scanDownload = nessusAPIQuery($action, "download", $scanReportFilename);

							//print $scanDownload;
							$processing = 0;
							print "NESSUS has sent the filestream, ending NESSUS processing loop... [".$processing."]<br/>\n";

							$fileID = "NONE";

							if ($scanReportGoogle == 1) {
								print "Google Drive Export enabled...<br/>\n";
								// Google Drive API
								require_once '../google-api-php-client-2.2.0/vendor/autoload.php';
								putenv('GOOGLE_APPLICATION_CREDENTIALS=../assets/itso-hoist-dd5c14498638.json');

								$client = new Google_Client();

								if (getenv('GOOGLE_APPLICATION_CREDENTIALS')) {
									// use the application default credentials
									$client->useApplicationDefaultCredentials();
									//print "<br/>Success: Google API Authorized. <br/>";
								}
								else { 
									print "<br/>ERROR: Google API Authorization Failed. Exiting. <br/>"; 
									exit; 
								}

								$client->addScope(Google_Service_Drive::DRIVE);

								$drive_service = new Google_Service_Drive($client);

								// test parent folder existence
								$googleFolderFound = 0;

								print "Checking to see if Parent Folder, ".$googleFolderName.", exists...<br/>\n";
								try {
									$tdFilesList = $drive_service->files->listFiles(array("corpora"=>"teamDrive","supportsTeamDrives"=>1,"teamDriveId"=>"0AH64_VBABStyUk9PVA","includeTeamDriveItems"=>1,"q"=>"trashed=false"))->getFiles();

									foreach ($tdFilesList as $tdFile) {
										if ($tdFile->name == $googleFolderName) {
											$googleFolderFound = 1;
											$googleFolderID = $tdFile->id;
											print "<br/>Parent Folder exists on Google: ".$tdFile->name.", ID: ".$tdFile->id."<br/>\n";
										}
									}
								}
								catch (Exception $e) {
									print $e->getMessage();
								}

								if ($googleFolderFound != 1) {
									print "Scan Folder, ".$googleFolderName.", NOT FOUND.  Creating it...<br/>\n";

									// create folder structure if needed... 
									try {
										// ITSO Scan Reports Parent Folder:
										$parentFolderID = "0AH64_VBABStyUk9PVA";
										$folderMetadata = new Google_Service_Drive_DriveFile(array(
											'name' => $googleFolderName,
											'teamDriveId' => $parentFolderID,
											'mimeType' => 'application/vnd.google-apps.folder',
											'parents' => array($parentFolderID)
										));

										$folder = $drive_service->files->create($folderMetadata, array(
											'supportsTeamDrives' => 1,
											'fields' => 'id'));

										$googleFolderID = trim($folder->id);
										print "Scan Folder CREATED: ".$googleFolderName.", ID: ".$googleFolderID."<br/>\n";								
									}
									catch (Exception $e) {
										print $e->getMessage();
									}
								}


								// check to see if the report already exists in Google Drive
								$reportFileFound = 0;
								$reportFileID = "";

								try {
									$tdFilesList = $drive_service->files->listFiles(array("corpora"=>"teamDrive","supportsTeamDrives"=>1,"teamDriveId"=>"0AH64_VBABStyUk9PVA","includeTeamDriveItems"=>1,"q"=>"trashed=false"))->getFiles();

									foreach ($tdFilesList as $tdFile) {

										print "Google Filename: [".$tdFile->name."], Searching for: [".$scanReportFilename.".html]<br/>\n";

										if ($tdFile->name == $scanReportFilename.".html") {
											$reportFileFound = 1;
											$reportFileID = $tdFile->id;
											print "<br/>REPORT FOUND: Report ID: ".$tdFile->id.", Report Name: ".$tdFile->name."<br/>";
											break;
										}
									}
								}
								catch (Exception $e) {
									print $e->getMessage();
								}

								if ($reportFileFound != 1) {

									print "Report NOT FOUND, CREATING NEW FILE...<br/>\n";

									// create and upload a new Google Drive file, including the data
									try {

										$subFolderID = $googleFolderID;
										$fileMetadata = new Google_Service_Drive_DriveFile(array(
											'name' => $scanReportFilename.'.html', 
											'teamDriveId' => $subFolderID,
											'parents' => array($subFolderID)
										));

										//$content = file_get_contents($scanDownload);
										$content = $scanDownload;
										$file = $drive_service->files->create($fileMetadata, array(
											'data' => $content,
											'mimeType' => 'text/html',
											'uploadType' => 'multipart',
											'supportsTeamDrives' => 1,
											'fields' => 'id'));

										$reportFileID = trim($file->id);
										$drive_service->getClient()->setUseBatch(true);

										print "Created.  Setting permissions...<br/>\n";

										try {
											$batch = $drive_service->createBatch();

											$userPermission = new Google_Service_Drive_Permission(array(
												'type' => 'user',
												'role' => 'reader',
												'emailAddress' => 'itsotest@vt.edu'
											));
											$request = $drive_service->permissions->create(
												$reportFileID, $userPermission, array('supportsTeamDrives' => 1,'fields' => 'id'));
											$batch->add($request, 'user');

											$results = $batch->execute();

											foreach ($results as $result) {
												if ($result instanceof Google_Service_Exception) {
													// Handle error
													printf($result);
												} else {
													print "Permission ID: ".$result->id."<br/>\n";
												}
											}
										} finally {
											$drive_service->getClient()->setUseBatch(false);
										}

									}
									catch (Exception $e) {
										print $e->getMessage();
									}
								} // END IF REPORTFILE NOT FOUND IF
							} // END IF SCANREPORTGOOGLE

							//print trim($reportFileID)."|DATA|".$scanDownload."|DATA|".$debugMsg;
							//print trim($reportFileID)."|DATA|Full report data Suppressed to prevent flood of debug info.|DATA|".$debugMsg;
							print "Success.  reportFileID is: ".trim($reportFileID)."<br/>\n";
							print "Updating DB tables... <br/>\n";

							// Update Scan DB

							include("./db_info.inc.php");
							$dbName = "hoist";

							try {
								$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
								$statement = $connection->prepare("UPDATE scans SET googleReportID = :googleReportID WHERE scanID = :scanID");
								$statement->bindValue(":googleReportID", $reportFileID, PDO::PARAM_STR); 
								$statement->bindValue(":scanID", $scanID, PDO::PARAM_INT); 
								$update = $statement->execute();
							}
							catch(PDOException $e) { print "Error: ".$e->getMessage(); }

							print "ScanDB updated with googleReportID.<br/>\n";

							// Log the action

							try {
								$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
								unset($dbUser);
								unset($dbPass);
								$statement = $connection->prepare("INSERT INTO logs (logUser, logType, logDataID, logInfo) VALUES (:logUser, :logType, :logDataID, :logInfo)");
								$statement->execute(array("logUser" => $scanCreator, "logType" => "updateScan", "logDataID" => $scanID, "logInfo" => $reportFileID));
							}
							catch(PDOException $e) { print "Error: ".$e->getMessage(); }

							print "Logs updated with updateScan action.<br/>\n";
							
							if ($scanAutoNotify == 1) {
								
								include("servicenow_funcs.php");

								// force DEV mode if we're on a dev instance...
								if (strstr($_SERVER["PWD"], "/hoistdev")) { 
									$mode = "DEV"; 
									$modeURL = "dev"; 
									print "Using DEVELOP instance of SN.\n"; 
								} 
								else { 
									$mode = "LIVE"; 
									$modeURL = ""; 
									print "Using LIVE instance of SN.\n"; 
								}
																								
								$googleDriveURL = "https://drive.google.com/uc?export=download&id=".trim($reportFileID);
								
								print "Auto-Notification Enabled.\nSending Google Drive link: ".$googleDriveURL."\nService-Now ".$mode.": ".$snRITM."\n\n";
								
								notifyRecipients($snRITM, $googleDriveURL, "itso_user", $modeURL);
								
							}

							// clear the connection
							$connection = null;
							print "\n====  END EXPORT PROCESS =====<br/>\n";

						}
						else {
							$nowDate = date("Y-m-d H:i:s T");
							print "Scan export not ready, waiting...".$nowDate."<br/>\n";
							sleep(3);
							$processing = 1;
						}

					} // END WHILE PROCESSING

				} // end completed report IF
				else {
					print "No reports have status: COMPLETED.  Will try again shortly...<br/>\n";	
				}
				
			} // end scanRow foreach
			

		} // endif scans with googleReportIDs exist
		else {
			$nowDate = date("Y-m-d H:i:s T");
			print "No scans to export on ".$nowDate.".<br/>\n";	
			print "\n\n====  END EXPORT PROCESS =====<br/>\n";

		}
	}
	catch(PDOException $e) { print "Error: ".$e->getMessage(); }

	// clear the connection
	$connection = null;
?>