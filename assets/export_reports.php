<?php

/****************************************************************************

	CRON JOB TO HANDLE NESSUS REPORT AUTO-EXPORT TO GOOGLE DRIVE

****************************************************************************/

	// SERVER TIMEZONE REQUIRED FOR DATE FUNCTIONS
	date_default_timezone_set('UTC');

	// PHP ERROR REPORTING LEVEL
	error_reporting(E_ALL); 
	ini_set('display_errors', '1');

	// include the Nessus API functions
	include_once("./api_funcs.inc.php");

	include("db_info.inc.php");
	$dbName = "hoist";

	print "Starting...<br/>\n";
	try {
		$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
		unset($dbUser);
		unset($dbPass);

		$statement = $connection->prepare("SELECT scanID, scanCreator, scanRequestor, scanFolderID, scanFolderName, googleFolderName, googleReportID, scanAutoExport FROM scans WHERE googleReportID IS NULL AND scanAutoExport = 'true'");
		$statement->execute();

		if ($statement->rowCount() > 0) {

			$rows = $statement->fetchAll(PDO::FETCH_ASSOC);

			foreach ($rows as $scanRow) {
				//var_dump($scanRow);
				print "DEBUG: scanID: ".$scanRow["scanID"]."<br/>\n";
				
				$scanInfoJSON = apiQuery("scans/".$scanRow["scanID"], "get", "");
		
				$scanInfo = json_decode($scanInfoJSON);
				//print "<br/>DEBUG: scanInfo is: <br/>";
				//var_dump($scanInfo);
				//print "<br/><br/>";
				
				$scanID = $scanRow["scanID"];
				$scanCreator = $scanRow["scanCreator"];
				$scanRequestor = $scanRow["scanRequestor"];
				$scanFolderName = $scanRow["googleFolderName"];
				$scanReportGoogle = 1;
				$debugMsg = "DEBUG: ";

				print "Checking Status: <br/>\n";
				
				if (isset($scanInfo->info->status) && $scanInfo->info->status == "completed") {
					print "Found Report that is COMPLETED...<br/>\n";

					if (isset($scanInfo->info->scan_start)) {
						$epochStart = $scanInfo->info->scan_start;
						$scanStartDate = new DateTime("@$epochStart");
						$scanStartDate->setTimeZone(new DateTimeZone('America/New_York'));
						
						print "Start date is: ".$scanStartDate->format('Y-m-d_H_i_s')."<br/>\n";

					}

					if (isset($scanInfo->info->scan_end)) {
						$epochEnd = $scanInfo->info->scan_end; 
						$scanEndDate = new DateTime("@$epochEnd");
						$scanEndDate->setTimeZone(new DateTimeZone('America/New_York'));
						
						print "End date is: ".$scanEndDate->format('Y-m-d_H_i_s')."<br/>\n";
					}

					if (isset($scanStartDate) && isset($scanEndDate)) {
						$scanReportFilename = $scanStartDate->format('Y-m-d_H_i_s')."-".$scanEndDate->format('Y-m-d_H_i_s')."_".$scanRequestor."_".$scanID;
					}
					else {
						$scanErrorDate = new DateTime();
						$scanErrorDate->setTimeZone(new DateTimeZone('America/New_York'));						
						$scanReportFilename = $scanErrorDate->format('Y-m-d_H_i_s')."-".$scanErrorDate->format('Y-m-d_H_i_s')."_".$scanRequestor."_".$scanID;
						
						print "Error date is: ".$scanErrorDate->format('Y-m-d_H_i_s')."<br/>\n";
					}

					$scanData["scan_id"] = $scanID;
					$scanData["format"] = "html"; // nessus, csv, db, html, pdf are valid options
					$scanData["chapters"] = "vuln_by_host"; // vuln_hosts_summary, vuln_by_host, compliance_exec, remediations, vuln_by_plugin, compliance

					$jsonData = json_encode($scanData);

					print "Filename is: ".$scanReportFilename."<br/>\n";
					
					$action = "scans/".$scanID."/export";
					$exportScanJSON = apiQuery($action, "export", $jsonData);
					$exportScan = json_decode($exportScanJSON);
				}
			}
			
			$processing = 1;

			print "Beginning Export... <br/>\n";

			while ($processing == 1) {
			
				$action = "scans/".$scanID."/export/".$exportScan->file."/status";
				$scanDLStatusJSON = apiQuery($action, "get", "");
				$scanDLStatus = json_decode($scanDLStatusJSON);

				if ($scanDLStatus->status == "ready") {
					print "Ready report found...<br/>\n";

					$action = "scans/".$scanID."/export/".$exportScan->file."/download";
					$scanDownload = apiQuery($action, "download", $scanReportFilename);

					//print $scanDownload;
					$processing = 0;
					$fileID = "NONE";

					if ($scanReportGoogle == 1) {
						print "Google Drive Export enabled...<br/>\n";
						// Google Drive API
						require_once '../google-api-php-client-2.2.0/vendor/autoload.php';
						putenv('GOOGLE_APPLICATION_CREDENTIALS=../assets/itso-hoist-dd5c14498638.json');

						$client = new Google_Client();

						if (getenv('GOOGLE_APPLICATION_CREDENTIALS')) {
							// use the application default credentials
							$client->useApplicationDefaultCredentials();
							//print "<br/>Success: Google API Authorized. <br/>";
						}
						else { 
							print "<br/>ERROR: Google API Authorization Failed. Exiting. <br/>"; 
							exit; 
						}

						$client->addScope(Google_Service_Drive::DRIVE);

						$drive_service = new Google_Service_Drive($client);

						// test parent folder existence
						$scanFolderFound = 0;

						try {
							$tdFilesList = $drive_service->files->listFiles(array("corpora"=>"teamDrive","supportsTeamDrives"=>1,"teamDriveId"=>"0AH64_VBABStyUk9PVA","includeTeamDriveItems"=>1))->getFiles();

							foreach ($tdFilesList as $tdFile) {
								if ($tdFile->name == $scanFolderName) {
									$scanFolderFound = 1;
									$scanFolderID = $tdFile->id;
									$debugMsg = $debugMsg."<br/>ID: ".$tdFile->id.", Name: ".$tdFile->name."<br/>";
								}
							}
						}
						catch (Exception $e) {
							print $e->getMessage();
						}

						if ($scanFolderFound != 1) {
							// create folder structure if needed... 
							try {

								$parentFolderID = "0AH64_VBABStyUk9PVA";
								$folderMetadata = new Google_Service_Drive_DriveFile(array(
									'name' => $scanFolderName,
									'teamDriveId' => $parentFolderID,
									'mimeType' => 'application/vnd.google-apps.folder',
									'parents' => array($parentFolderID)
								));
									//'parents' => array($folderID)

								$folder = $drive_service->files->create($folderMetadata, array(
									'supportsTeamDrives' => 1,
									'fields' => 'id'));

								$scanFolderID = trim($folder->id);
							}
							catch (Exception $e) {
								print $e->getMessage();
							}
						}


						// check to see if the report already exists in Google Drive
						$reportFileFound = 0;
						$reportFileID = "";

						try {
							$tdFilesList = $drive_service->files->listFiles(array("corpora"=>"teamDrive","supportsTeamDrives"=>1,"teamDriveId"=>"0AH64_VBABStyUk9PVA","includeTeamDriveItems"=>1))->getFiles();

							foreach ($tdFilesList as $tdFile) {
								if ($tdFile->name == $scanReportFilename) {
									$reportFileFound = 1;
									$reportFileID = $tdFile->id;
									$debugMsg = $debugMsg."<br/>Report ID: ".$tdFile->id.", Report Name: ".$tdFile->name."<br/>";
								}
							}
						}
						catch (Exception $e) {
							print $e->getMessage();
						}

						if ($reportFileFound != 1) {
							// create and upload a new Google Drive file, including the data
							try {

								$subFolderID = $scanFolderID;
								$fileMetadata = new Google_Service_Drive_DriveFile(array(
									'name' => $scanReportFilename.'.html', 
									'teamDriveId' => $subFolderID,
									'parents' => array($subFolderID)
								));
									//$fileCreationDate->format('Y-m-d_H.i.s').'_nessus_report_'.$scanID.'.html',
									//'parents' => array($folderID)

								//$content = file_get_contents($scanDownload);
								$content = $scanDownload;
								$file = $drive_service->files->create($fileMetadata, array(
									'data' => $content,
									'mimeType' => 'text/html',
									'uploadType' => 'multipart',
									'supportsTeamDrives' => 1,
									'fields' => 'id'));

								$reportFileID = trim($file->id);
								$drive_service->getClient()->setUseBatch(true);

								try {
									$batch = $drive_service->createBatch();

									$userPermission = new Google_Service_Drive_Permission(array(
										'type' => 'user',
										'role' => 'reader',
										'emailAddress' => 'itsotest@vt.edu'
									));
									$request = $drive_service->permissions->create(
										$reportFileID, $userPermission, array('supportsTeamDrives' => 1,'fields' => 'id'));
									$batch->add($request, 'user');

									$results = $batch->execute();

									foreach ($results as $result) {
										if ($result instanceof Google_Service_Exception) {
											// Handle error
											printf($result);
										} else {
											$debugMsg = $debugMsg."Permission ID: ".$result->id;
										}
									}
								} finally {
									$drive_service->getClient()->setUseBatch(false);
								}

							}
							catch (Exception $e) {
								print $e->getMessage();
							}
						} // END IF REPORTFILE NOT FOUND IF
					} // END IF SCANREPORTGOOGLE
					
					print trim($reportFileID)."|DATA|".$scanDownload."|DATA|".$debugMsg;

					print "Report exported... <br/>\nUpdating DB... <br/>\n";

					// Update Scan DB

					include("./db_info.inc.php");
					$dbName = "hoist";

					try {
						$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
						$statement = $connection->prepare("UPDATE scans SET googleReportID = :googleReportID WHERE scanID = :scanID");
						$statement->bindValue(":googleReportID", $reportFileID, PDO::PARAM_STR); 
						$statement->bindValue(":scanID", $scanID, PDO::PARAM_INT); 
						$update = $statement->execute();
					}
					catch(PDOException $e) { print "Error: ".$e->getMessage(); }

					// Log the action

					try {
						$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
						unset($dbUser);
						unset($dbPass);
						$statement = $connection->prepare("INSERT INTO logs (logUser, logType, logDataID, logInfo) VALUES (:logUser, :logType, :logDataID, :logInfo)");
						$statement->execute(array("logUser" => $scanCreator, "logType" => "updateScan", "logDataID" => $scanID, "logInfo" => $reportFileID));
					}
					catch(PDOException $e) { print "Error: ".$e->getMessage(); }

					// clear the connection
					$connection = null;

				}
				else {
					print "Scan export not ready, waiting...<br/>\n";
					sleep(3);
					$processing = 1;
				}

			} // END WHILE PROCESSING

		} // endif scans with googleReportIDs exist
		else {
			$nowDate = date("Y-m-d H:i:s");
			print "No scans to export on ".$nowDate.".<br/>\n";	
		}
	}
	catch(PDOException $e) { print "Error: ".$e->getMessage(); }

	// clear the connection
	$connection = null;
?>