<?php 

/****************************************************************************

	HOIST Scan DB Details Form Include
	
	This makes up the HTML form that appears in the HOIST Edit HOIST Details
	jQuery Dialog box.

****************************************************************************/

?>
		<table class="scanFormTable">
			<tr>
				<td width="200px">
					Scan Name:
				</td>
				<td>
					[ <?php print $scanInfo->info->name; ?> ]
				</td>
				<td width="200px">
					Scan ID:
				</td>
				<td>
					<?php print $scanInfo->info->object_id; ?>
				</td>
			</tr>
			<tr>
				<td width="200px">
					Scan Creator:
				</td>
				<td>
					<?php print $scanCreator; ?>
				</td>
				<td width="200px">
					Scan Requestor:
				</td>
				<td>
					<?php print $scanRequestor; ?>
				</td>
			</tr>
		</table>
		<br/>
		<br/>
				
		<table id="scanHistListTable" class="display" cellspacing="0" width="100%">
			<thead>
			<tr>
				<th>Start Time</th>
				<th>History ID</th>
				<th>Scheduled</th>
				<th>Status</th>
				<th>G Drive Report</th>
				<th>Alternate Targets</th>
			</tr>
			</thead>
			<tfoot>
			<tr>
				<th>Start Time</th>
				<th>History ID</th>
				<th>Scheduled</th>
				<th>Status</th>
				<th>G Drive Report</th>
				<th>Alternate Targets</th>
			</tr>
			</tfoot>
			<tbody>
<?php				
			foreach ($scanInfo->history as $scanHistItem) {
				
				$histID = $scanHistItem->history_id;
				
				$epochHStart = $scanHistItem->creation_date;
				$scanHStartDate = new DateTime("@$epochHStart");
				$scanHStartDate->setTimeZone(new DateTimeZone('America/New_York'));
				
				$epochHEnd = $scanHistItem->last_modification_date;
				$scanHEndDate = new DateTime("@$epochHEnd");
				$scanHEndDate->setTimeZone(new DateTimeZone('America/New_York'));
				
				if ($scanHistItem->status == "empty") { $niceHStatus = "<span class='red'>Never Run</span>"; }
				else if ($scanHistItem->status == "completed") { $niceHStatus = "<span class='green'>Completed</span>"; }
				else if ($scanHistItem->status == "running") { $niceHStatus = "<span class='blue'>Running</span>"; }
				else { $niceHStatus = $scanHistItem->status; }
				
				if ($scanHistItem->scheduler == 1) { $niceHScheduled = "<span class='green'>Yes</span>"; }
				else if ($scanHistItem->scheduler == 0) { $niceHScheduled = "<span class='blue'>No</span>"; }
				else { $niceHScheduled = $scanHistItem->scheduler; }

				if ($scanHistItem->alt_targets_used == "") { $niceAltTargets = "<span class='blue'>No</span>"; }
				else { $niceAltTargets = "<span class='red'>".$scanHistItem->alt_targets_used."</span>"; }

				if ($scanHistItem->status == "completed") {
					$reportHistURL = "<button id='histGenReport-".$histID."' class='styledButton histGenReport' value='".$histID."'><span>Generate Report</span></button>";
				}
				else {
					$reportHistURL = "Scan is incomplete";
				}
				
				//$reportURL = "<a href='https://drive.google.com/uc?export=download&id=".$srComp["googleReportID"]."' target='_blank'>DL Report</a>";

				include("assets/db_info.inc.php");
				$dbName = "hoist";
				
				try {
					$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
					unset($dbUser);
					unset($dbPass);

					$statement = $connection->prepare('SELECT googleReportID FROM scanhistory WHERE histID = :histID LIMIT 1');
					$statement->execute(array('histID' => $histID));

					if ($statement->rowCount() > 0) {

						$rows = $statement->fetchAll(PDO::FETCH_ASSOC);

						foreach ($rows as $scanRow) { 
							$scanHistDBInfo["googleReportID"] = $scanRow["googleReportID"];
							if ($scanHistDBInfo["googleReportID"] != "") {
								$reportHistURL = "<a style='color: #fe5b00;' href='https://drive.google.com/uc?export=download&id=".$scanHistDBInfo["googleReportID"]."' target='_blank'>DL Report</a>";
							}
						}
					}
				}
				catch(PDOException $e) { print "Error: ".$e->getMessage(); }
				
				//print "<br/>DEBUG: scanHistItem is: <br/>";
				//var_dump($scanHistItem);
				//print "<br/>DEBUG END<br/>";
?>
			<tr>
				<td>
					<?php print $scanHStartDate->format('Y-m-d H:i:s T'); ?>
					<input type="hidden" id="scanLastModified-<?php print $histID; ?>" value="<?php print $epochHEnd; ?>" />
				</td>
				<td><?php print $scanHistItem->history_id; ?></td>
				<td><?php print $niceHScheduled; ?></td>
				<td><?php print $niceHStatus; ?></td>
				<td><span id="scanURL-<?php print $histID; ?>"><?php print $reportHistURL; ?></span></td>
				<td><?php print $niceAltTargets; ?></td>
			</tr>
<?php
		   }
?>
			</tbody>
		</table>
<?php

// EOF

?>