<?php 

/****************************************************************************

	HOIST Scan DB Details Form Include
	
	This makes up the HTML form that appears in the HOIST Edit HOIST Details
	jQuery Dialog box.

****************************************************************************/

?>
			<form id="scanDBDetailsForm">
				<table class="scanFormTable">
				<tr>
					<td width="200px">
						Scan Name:
					</td>
					<td>
						[ <?php print $scanInfo->info->name; ?> ]
					</td>
					<td width="200px">
						Scan ID:
					</td>
					<td>
						<?php print $scanInfo->info->object_id; ?>
					</td>
				</tr>
				<tr>
					<td width="200px">
						Scan Creator:
					</td>
					<td>
						<input type="text" id="scanCreatorUpdate" value="<?php print $scanCreator; ?>" />
					</td>
					<td width="200px">
						Scan Requestor:
					</td>
					<td>
						<input type="text" id="scanRequestorUpdate" value="<?php print $scanRequestor; ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="3">
						Scan Targets: Should be CIDR Notation or Individual IPs in a comma-separated list. <br/>
						<textarea id="scanTargetIPsUpdate" rows="5" cols="101"><?php print $scanInfo->info->targets; ?></textarea>
					</td>
					<td>
						&nbsp;
					</td>
				</tr>
				<tr>
					<td colspan="3">
						Scan Report Recipients: Should be a comma-separated list of <em>full email addresses</em> <br/>
						<textarea id="scanRecipientsUpdate" rows="2" cols="101"><?php print $scanRecipients; ?></textarea> 
					</td>
					<td>				
						&nbsp;
					</td>
				</tr>
				<tr>
					<td colspan="4">
						
<?php
					if ($scanAutoExport == "1") { $scanAutoExportChecked = "checked=\"checked\""; } else { $scanAutoExportChecked = ""; } 
					if ($scanAutoNotify == "1") { $scanAutoNotifyChecked = "checked=\"checked\""; } else { $scanAutoNotifyChecked = ""; } 
?>
					<label><input type="checkbox" id="setFolderByPID" value="autosetfolder" checked="checked" /></label>
					Auto-set Nessus/Google Drive Folder Destination based on Requestor PID's Department lookup in Service-Now? 
					<br/>
					<label><input type="checkbox" id="scanAutoExportUpdate" value="autoexportreport" <?php print $scanAutoExportChecked; ?> /></label>
					Auto-export report to Google Drive after Scan Details are updated? 
					<br/>	
					Auto-notify the report recipients of the Google Drive link through Service-Now once scan is complete? 
					<label><input type="checkbox" id="scanAutoNotifyUpdate" value="autonotifyreport" <?php print $scanAutoNotifyChecked; ?>/></label>				
				</td>
				</tr>					
				<tr>
					<td>Google Folder Destination:</td>
					<td>
						<input type="text" id="googleFolderNameUpdate" value="<?php print $googleFolderName; ?>" size="27" />
					</td>
					<td>Service-Now Number:</td>
					<td>
						<?php print $snRITM; ?>
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<div style="position:relative; top: 15px; margin-left: 720px;"><input type="button" id="updateScanDB" value="Update Scan" style="width: 217px;" /> </div>
					</td>
			</tr>
			</table>
			</form>			
<?php

// EOF

?>