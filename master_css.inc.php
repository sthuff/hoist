<?php 

/****************************************************************************

	The HOIST automates Nessus scans and reporting features for the ITSO.

****************************************************************************/
/****************************************************************************

	master_css.inc.php
	
	Included by index.php
	
	Includes the master stylesheet and sets the favicon for the site.	

****************************************************************************/
?>
	<!-- Master CSS -->
		<link rel="stylesheet" type="text/css" media="all" href="./css/master.css" />
		<link rel="shortcut icon" href="./favicon.ico" />
<?php 

// EOF

?>