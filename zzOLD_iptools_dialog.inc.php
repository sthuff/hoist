<?php 

/****************************************************************************

	HOIST Scan DB Details Form Include
	
	This makes up the HTML form that appears in the HOIST Edit HOIST Details
	jQuery Dialog box.

****************************************************************************/

?>
			<form id="ipToolsForm">
				<table class="scanFormTable">
				<tr>
					<td colspan="4">
						Original Scan Target IPs: <br/>
						<?php print $scanTargetIPString; ?>
					</td>
				</tr>
				<tr>
					<td width="200px">
						IP Range Start:
					</td>
					<td>
						<input type="text" id="ipRangeStart" value="" size="15" />
					</td>
					<td width="200px">
						IP Range End:
					</td>
					<td>
						<input type="text" id="ipRangeEnd" value="" size="15" />
					</td>
				</tr>
				<tr>
					<td width="200px">
						Target Format:
					</td>
					<td colspan="3">
						<span class="targetFormatRadio"><label><input type="radio" id="formatCIDR" name="targetFormat" value="CIDR" checked="checked"> CIDR Notation</label></span>
						<span class="targetFormatRadio"><label><input type="radio" id="formatIPList" name="targetFormat" value="ipList"> List of IPs</label></span>
					</td>
				</tr>
				<tr>
					<td colspan="4">
						New/Reformatted Scan Targets: Should be CIDR Notation or Individual IPs in a comma-separated list. <br/>
						<textarea id="scanTargetIPsUpdate" rows="5" cols="90"></textarea>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<div id="feedback" class="feedbackFrame">&nbsp;</div>
					</td>
					<td>
						<div style="position:relative; top: 15px; margin-right: 80px;"><input type="button" id="updateTargetIPs" value="Update Target IPs" style="width: 217px;" /> </div>
					</td>
				</tr>
			</table>
			</form>			
<?php

// EOF

?>