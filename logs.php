<?php
/****************************************************************************

	The HOIST automates Nessus scans and reporting features for the ITSO.

****************************************************************************/
/****************************************************************************

	logs_funcs.php
	
	Creates logs in DB for actions taken in HOIST
	
****************************************************************************/

	include_once("initial_config.inc.php");
	include_once("doctype.inc.php");
?>
<html>
<head>
	<title>HOIST - Logs</title>
<?php
	include_once("master_css.inc.php");
	include_once("meta_data.inc.php");
?>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js "></script>
	<link type="text/css" rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

	<link rel="stylesheet" type="text/css" href="./DataTables/datatables.min.css"/>
	<script type="text/javascript" src="./DataTables/datatables.min.js"></script>

	<script>
		$(document).ready(function(){
			$('#logsTable').DataTable({
				order: [2, 'dec'],
				"paging": true,
				"lengthMenu": [[50, 100, 200, 500, -1], [50, 100, 200, 500, "All"]],
				"deferRender": true
			});
		});	// END DOCREADY
	</script>
</head>
<body>
	
<div id="header">			<!-- header -->
	<div class="bg">
		<div class="container"> 	<!-- container -->
				<div class="title"></div>
				<div class="logo"></div>
				<div class="content">&nbsp;</div>
				<div class="navbar">
<?php
					include_once("navbar.php");					
?>
				</div>
				<div class="clear"></div>
		</div> 				<!-- container end -->
	</div>
</div> 					<!-- header end -->

<div id="maincontent"> <!-- maincontent -->
		<div class="bg">
		<div class="container">
			
			<div>
				Filter Logs: 
<?php
				if (isset($_GET["filter"]) && $_GET["filter"] == "live") {
?>
				<strong>Live Only</strong> | <a href="./logs.php?filter=dev">Dev Only</a> | <a href="./logs.php?filter=all">All</a> (default)
<?php
				} else if (isset($_GET["filter"]) && $_GET["filter"] == "dev") {
?>
				<a href="./logs.php?filter=live">Live Only</a> | <strong>Dev Only</strong> | <a href="./logs.php?filter=all">All</a> (default)
<?php
				} else if (isset($_GET["filter"]) && $_GET["filter"] == "all") {
?>
				<a href="./logs.php?filter=live">Live Only</a> | <a href="./logs.php?filter=dev">Dev Only</a> | <strong>All</strong> (default)
<?php
				} else {
?>
				<a href="./logs.php?filter=live">Live Only</a> | <a href="./logs.php?filter=dev">Dev Only</a> | <strong>All</strong> (default)
<?php
				}
?>
				<br/>
				<br/>
			</div>
<?php

				
	if ($userPermissions == "dbreadwrite") {

	/* MALWARE INFO DB LOOKUP */ 

	include("./assets/db_info.inc.php");
	$dbName = "feint";

		try {
			$connection = new PDO($dbDriver.':host='.$dbHost.';dbname='.$dbName.';charset='.$dbCharset, $dbUser, $dbPass);
			unset($dbUser);
			unset($dbPass);

			if (isset($_GET["filter"]) && $_GET["filter"] == "all") { 
				$statement = $connection->prepare('SELECT logID, logUser, logDate, logType, logDataID, logInfo, logSrcIP, logIncMalware FROM logs ORDER BY logID ASC');
			}
			else if (isset($_GET["filter"]) && $_GET["filter"] == "dev") { 
				$statement = $connection->prepare('SELECT logID, logUser, logDate, logType, logDataID, logInfo, logSrcIP, logIncMalware FROM logs WHERE logType LIKE "DEV%" ORDER BY logID ASC');	
			}
			else if (isset($_GET["filter"]) && $_GET["filter"] == "live") { 
				$statement = $connection->prepare('SELECT logID, logUser, logDate, logType, logDataID, logInfo, logSrcIP, logIncMalware FROM logs WHERE logType NOT LIKE "DEV%" ORDER BY logID ASC');	
			}
			else {
				$statement = $connection->prepare('SELECT logID, logUser, logDate, logType, logDataID, logInfo, logSrcIP, logIncMalware FROM logs ORDER BY logID ASC');
			}
			
			$statement->execute();

			$rows = $statement->fetchAll(PDO::FETCH_ASSOC);
			
			if ($statement->rowCount() > 0) {
?>
			<div stle="width:1100px">
			<table id="logsTable" class="display" cellspacing="0" width="100%">
				<thead>
				<tr>
					<th>ID</th>
					<th>User</th>
					<th>Date</th>
					<th>Type</th>
					<th>DataID</th>
					<th>Info</th>
					<th>SrcIP</th>
					<th>IncMalware</th>
				</tr>
				</thead>
				<tfoot>
					<th>ID</th>
					<th>User</th>
					<th>Date</th>
					<th>Type</th>
					<th>DataID</th>
					<th>Info</th>
					<th>SrcIP</th>
					<th>IncMalware</th>
				</tfoot>
			<tbody>
<?php				
				foreach ($rows as $row) { 
?>
				<tr class="topborder">
					<td><?php print $row["logID"]; ?></td>
					<td><?php print $row["logUser"]; ?></td>
					<td class="dt-nowrap"><?php print $row["logDate"]; ?></td>
					<td><?php print $row["logType"]; ?></td>
<?php
					if ($row["logType"] == "createIncident" || $row["logType"] == "ackAlertSuccess" || $row["logType"] == "ackAlertFailed" || $row["logType"] == "DEVcreateIncident") {
?>
					<td><a href="https://fireeye.iso.vt.edu/event_stream/events_for_bot?ev_id=<?php print $row["logDataID"]; ?>" target="_blank" style="color: #fe5b00;"><?php print $row["logDataID"]; ?></a></td>
<?php
						if (strpos($row["logInfo"], 'INC') === 0) {
							if ($row["logType"] == "DEVcreateIncident") { $modeHistURL = "dev"; } else { $modeHistURL = ""; }

?>
					<td><a href="https://vt4help<?php print $modeHistURL; ?>.service-now.com/nav_to.do?uri=incident.do?sysparm_query=number=<?php print $row["logInfo"]; ?>" target="_blank" style="color: #fe5b00;"><?php print $row["logInfo"]; ?></a></td>
<?php
						} 
						else { 
?>
						<td><?php print $row["logInfo"]; ?></td>
<?php
						}
					}
					else if ($row["logType"] == "editMalware" || $row["logType"] == "addMalware") {
?>
					<td><a href="./update_malwaredb.php?mwID=<?php print $row["logDataID"]; ?>&mwFunc=edit" target="_blank" style="color: #fe5b00;"><?php print $row["logDataID"]; ?></a></td>
					<td><a href="./update_malwaredb.php?mwID=<?php print $row["logDataID"]; ?>&mwFunc=edit" target="_blank" style="color: #fe5b00;"><?php print $row["logInfo"]; ?></a></td>
<?php						
					}
					else if ($row["logType"] == "deleteMalware") {
?>
					<td><span style="color:#FF0000; font-weight:bold;"><?php print $row["logDataID"]; ?></span></td>
					<td><span style="color:#FF0000; font-weight:bold;"><?php print $row["logInfo"]; ?></span></td>
<?php						
					}										 
					else {
?>
					<td><?php print $row["logDataID"]; ?></td>
					<td><?php print $row["logInfo"]; ?></td>
<?php
					}
?>
					<td><?php print $row["logSrcIP"]; ?></td>
					<td class="dt-nowrap"><?php print $row["logIncMalware"]; ?></td>
				</tr>
<?php
				}
?>
			</tbody>
		</table>
		</div>
<?php
			}
		}
		catch(PDOException $e) {
			print "Error: ".$e->getMessage();
		}
		
	// clear the connection
	$connection = null;
	} // end user permissions readwrite if
	else { 
		print "You are not authorized."; 
	}
?>
		</div> 					<!-- container class end -->
	</div>
</div> 					<!-- maincontent end -->
			
</body>
</html>