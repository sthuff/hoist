<?php 

/****************************************************************************

	The HOIST automates Nessus scans and reporting features for the ITSO.

****************************************************************************/
/****************************************************************************

	javascripts.inc.php
	
	Included by index.php
	
	Includes Javascript and jQuery Includes. 
	
	The HOIST makes heavy use of jQuery and the jQueryUI throughout.  

****************************************************************************/

?>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js "></script>
	<link type="text/css" rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

	<link type="text/css" rel="stylesheet" href="./css/jquery-ui-timepicker-addon.css">
	<script type="text/javascript" src="./js/jquery-ui-timepicker-addon.js"></script>

	<script type="text/javascript" src="./js/copyme.js"></script>
	<script type="text/javascript" src="./js/moment.js"></script>

	<link rel="stylesheet" type="text/css" href="./DataTables/datatables.min.css"/>
	<script type="text/javascript" src="./DataTables/datatables.min.js"></script>
<?php
	// While the bulk of this file could be kept as a javascript file (.js), it is 
	// instead remains a PHP include so that most comments can be hidden from the 
	// the client's browser
?>
	<script>
	$(document).ready(function(){
	
	// OS CHECKS

	var OSName="unknown";
	if (navigator.appVersion.indexOf("Win")!=-1) { OSName = "win"; }
	if (navigator.appVersion.indexOf("Mac")!=-1) { OSName = "mac"; }
	if (navigator.appVersion.indexOf("X11")!=-1) { OSName = "unix"; }
	if (navigator.appVersion.indexOf("Linux")!=-1) { OSName = "linux"; }
		
	// BROWSER CHECKS

    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    var isFirefox = typeof InstallTrigger !== 'undefined';
    var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);
    var isIE = /*@cc_on!@*/false || !!document.documentMode;
    var isEdge = !isIE && !!window.StyleMedia;
    var isChrome = !!window.chrome && !!window.chrome.webstore;
    var isBlink = (isChrome || isOpera) && !!window.CSS;

	var browserName = "unknown";
	if (isOpera) { browserName = "opera"; }
	if (isFirefox) { browserName = "firefox"; }
	if (isSafari) { browserName = "safari"; }
	if (isIE) { browserName = "ie"; }
	if (isEdge) { browserName = "edge"; }
	if (isChrome) { browserName = "chrome"; }
	if (isBlink) { browserName = "blink"; }
		
	// DEBUG
	//alert(OSName+", "+browserName);
		
	if (isFirefox && OSName == "win") {
	}
		
	if (isChrome && OSName == "linux") {
	}
		
	if (isFirefox && OSName == "unix") {
	}
		
	if (isIE) {
	}

	if (isEdge) {
	}

<?php		
		// SET USER PERMISSIONS
?>			
		if ($('#userPermissions').val() == "dbreadonly") {
<?php
			// If the user is a non-ITSO staff member (dbreadonly access), hide the 
			// Update Malware button, set the malware description textarea field 
			// to readonly, and set the text inside it to a light gray to indicate
			// cannot be edited.
?>			

		} else {
<?php
			// If the user is a ITSO staff member (dbreadwrite access), the
			// Update Malware button is visible and the malware description textarea field 
			// is only set to light gray if the description is empty.  In this case, 
			// a default blurb is presented requsting the field be completed. This
			// blurb is removed whenever the field gains keyboard or mouse focus.
?>
			
		}
		
<?php
		// NOTE - LAA expects timestamps in this format:
		// 2017-02-26T03:40:39.890Z

		// The timestamp formats that the LAA expects and the formats used by the jQueryUI date picker 
		// are not compatible.  Some conversion is done here.  We also perform some sanity checks
		// such that if an end date is picked that is earlier than a start date, the start date is forced 
		// to equal the end date.  Similarly, if an end date is chosen that is earlier than the start date, 
		// the the start date is forced to the end date to avoid errors.
?>		
		$("#dateTimeSchedScan").datetimepicker({ dateFormat: "yy-mm-dd", timeInput: true, timeFormat: "HH:mm:ss z",
												onClose: function(dateText, inst) { $("#dateTimeSchedScan").val(dateText); }
		}); 
		
<?php
		// SUBMIT SCAN TO NESSUS
?>
		$("#submitScan").click(submitScan);		
<?php
		// UPDATE SCAN TO HOIST DB
?>		
		$("#updateScanDB").click(updateScanDB);	
		
		$("#rejectScanButton").click(rejectScan);
		
		$("#approveScanButton").click(approveScan);
		
		$("#notifyRecipients").click(notifyRecipients);
		
		$("#refreshScanStatus").click(function() { location.reload(true); });
				
		$('#scansListTable').DataTable({
			order: [[1, 'dec'],[0,'dec']],
			rowGroup: { 
				startRender: function ( rows, group ) {
					return group +' ('+rows.count()+')';
				},
				dataSrc: 1
			},
			"deferRender": true
		});
		
		$('#jobsListTable').DataTable({
			order: [0, 'dec'],
			"deferRender": true
		});
		
		$('#scanHistListTable').DataTable({
			order: [0, 'dec'],
			"deferRender": true
		});
		
		var pathURL = window.location.pathname;
		
		if ($('#scanRequestor').length > 0 && $('#setFolderByPID').is(":checked") && pathURL.indexOf("scansched.php") !== -1) {
			getFolderDestination(0);
    	}
		
		if ($('#scanNotInDB').val() == 1) {
				$('#generateReport').prop("disabled","disabled");
				$('#scanReportGoogle').prop("disabled","disabled");
				$('#scanReportLocal').prop("disabled","disabled");
		}

		$('#scanRequestor').on('blur', function() {
			if ($('#setFolderByPID').is(":checked")) {
				getFolderDestination(0);
				return false;
			} else { $('#feedback').html("Auto-setting Nessus/Google Drive Destination Disabled."); }
		});

		$('#scanRequestorUpdate').on('blur', function() {
			if ($('#setFolderByPID').is(":checked")) {
				getFolderDestination(1);
				return false;
			} else { $('#feedback').html("Auto-setting Nessus/Google Drive Destination Disabled."); }
		});

		$('#setFolderByPID').on('change', function () {
			if ($('#setFolderByPID').is(":checked")) {
				getFolderDestination(0);
				return false;
			} else { $('#feedback').html("Auto-setting Nessus/Google Drive Destination Disabled."); }
		});

		$('#nowButton').click( function () {
				var scanDateNowFormat = moment().format("YYYY-MM-DD hh:mm:ss ZZ");
				$('#dateTimeSchedScan').val(scanDateNowFormat);
		});
		
		$('#scanScheduled').on('change', function () {
			if ($('#scanScheduled').val() == "now") {
				var scanDateNowFormat = moment().format("YYYY-MM-DD hh:mm:ss ZZ");
				$('#dateTimeSchedScan').val(scanDateNowFormat);
				$('#dateTimeSchedScan').prop("readonly", true);
				$('#scanFrequency').val("ONCE");
				updateScanFreqAndInterval();
				$('#scanFrequency').prop("disabled", true);
				$('#scanInterval').val("1");
				$('#scanInterval').prop("readonly", true);

				
			} else {
				$('#dateTimeSchedScan').prop("readonly", false);
				$('#scanFrequency').prop("disabled", false);
				$('#scanInterval').prop("readonly", false);
			}
			return false;
		});

		$('#scanFrequency').on('change', function () { updateScanFreqAndInterval(); return false; });

		$('#scanInterval').on('change', function () { updateScanFreqAndInterval(); return false; });
		
		$('input[name=BYDAY]').on('change', function () { updateScanFreqAndInterval(); return false; });
		
		$('#dateTimeSchedScan').on('change', function () { updateScanFreqAndInterval(); return false; });
		
		$("#editScanDBButton").click(openScanDetailsDialog);
		
		$("#scanHistoryButton").click(openScanHistoryDialog);
		
		$("#rejectScanButton").hover(function() { 
			$("#feedback").html("Clicking <span class='red'>REJECT</span> will close and cancel the Service-Now request and notify the end-user that ITSO has rejected this scan request."); 
		}, function() { $("#feedback").html("&nbsp;"); });
									 
		$("#approveScanButton").hover(function() { 
			$("#feedback").html("Clicking <span class='green'>APPROVE</span> will advance the Service-Now request status and notify the Scan Requestor of the approval."); 
  		}, function() { $("#feedback").html("&nbsp;"); });

		$("#resetRecipients").hover(function() { 
			$("#feedback").html("Originally: "+$("#scanRecipientsFullOrig").val()); 
  		}, function() { $("#feedback").html("&nbsp;"); });

		if ($("#snRITM").val() == "") {
			$("#resetRecipients").show();
			$("#resetRecipients").click(function() { $("#scanRecipients").val($("#scanRecipientsOrig").val()) });
			$('#scanRecipients').prop("readonly", false);
		}

		$("#resetTargetIPs").hover(function() { 
			$("#feedback").html("Originally: "+$("#scanTargetIPs").val()); 
  		}, function() { $("#feedback").html("&nbsp;"); });

		$("#resetTargetIPs").click(function() { $("#scanTargetIPs").val($("#scanTargetIPsOrig").val()) });									 

		// Report Generation
		$("#generateReport").click(function() {
			generateReport($('#histID').val(), "latest");
		});	
		
		$('.histGenReport').click(function() {
			var genhistID = $(this).val();
			generateReport(genhistID, "history");
		});
		
	});	// END DOCREADY

function updateScanFreqAndInterval() {
	
	// ensure no values are frozen
	$('#dateTimeSchedScan').prop("readonly", false);
	$('#scanFrequency').prop("disabled", false);
	$('#scanInterval').prop("readonly", false);
	
	if ($('#scanInterval').val() < 1) { $('#scanInterval').val("1"); }
	if ($('#scanInterval').val() > 20) { $('#scanInterval').val("20"); }
	
	if ($('#scanFrequency').val() == "ONCE") {
		$('#scanWeeklyBoxes').slideUp();
		$('#scanInterval').val("1");
		$('#scanIntervalText').html("Scan will <strong>not</strong> repeat");
		$('#scanIntervalText').animate( { height: 30 } );
		$('#scanRecurrence').prop('checked', false);
	} 
	else if ($('#scanFrequency').val() == "DAILY") {
		$('#scanWeeklyBoxes').slideUp();
		$('#scanIntervalText').html("Scan will repeat every <strong>"+$('#scanInterval').val()+"</strong> day");
		$('#scanRecurrence').prop('checked', true);
	} 
	else if ($('#scanFrequency').val() == "WEEKLY") {
		$('#scanWeeklyBoxes').slideDown();
		$('#scanIntervalText').html("Scan will repeat every <strong>"+$('#scanInterval').val()+"</strong> week");
		if ($('#scanInterval').val() > 1) { $('#scanIntervalText').html($('#scanIntervalText').html()+"s"); }
		$('#scanIntervalText').html($('#scanIntervalText').html()+" on: <br/>");
		
		$('input[name=BYDAY]').each(function () {
	    	if ($(this).is(":checked")) { $('#scanIntervalText').html($('#scanIntervalText').html()+$(this).val()+", ") } 
		});

		// lop off the last 2 ,[space] chars
		$('#scanIntervalText').html(function (_,txt) { return txt.slice(0, -2); });
		
		var niceDate = moment($('#dateTimeSchedScan').val(), "YYYY-MM-DD hh:mm:ss").format("MMM D YYYY");
		$('#scanIntervalText').html($('#scanIntervalText').html()+"<br/>starting on "+niceDate);
		
		$('#scanIntervalText').animate( { height: 80 } );
		$('#scanRecurrence').prop('checked', true);
	} 
	else if ($('#scanFrequency').val() == "MONTHLY") {
		$('#scanWeeklyBoxes').slideUp();
		$('#scanIntervalText').html("Scan will repeat every <strong>"+$('#scanInterval').val()+"</strong> month");
		$('#scanRecurrence').prop('checked', true);
	} 
	else if ($('#scanFrequency').val() == "YEARLY") {
		$('#scanWeeklyBoxes').slideUp();
		$('#scanIntervalText').html("Scan will repeat every <strong>"+$('#scanInterval').val()+"</strong> year");
		$('#scanRecurrence').prop('checked', true);
	} 
	
	if ($('#scanInterval').val() > 1 && $('#scanFrequency').val() != "WEEKLY") { 
		$('#scanIntervalText').html($('#scanIntervalText').html()+"s"); 
	}

	if ($('#scanFrequency').val() != "ONCE" && $('#scanFrequency').val() != "WEEKLY") {
		var niceDate = moment($('#dateTimeSchedScan').val(), "YYYY-MM-DD hh:mm:ss").format("MMM D YYYY");
		$('#scanIntervalText').html($('#scanIntervalText').html()+"<br/>starting on "+niceDate);
		$('#scanIntervalText').animate( { height: 50 } );
		$('#scanRecurrence').prop('checked', true);
	}
}
		
function generateReport(genHistID, reportType) {

	var googleReport = 0;
	var scanLastModified = $("#scanLastModified").val();
	
	if (reportType == "history") { 
		googleReport = 1; 
		$('#scanURL-'+genHistID).html("<img src='./images/loading16x16.gif' /> Awaiting Nessus...");
		scanLastModified = $('#scanLastModified-'+genHistID).val();
	}
	else {
		$('#scanURL').html("<img src='./images/loading16x16.gif' /> Awaiting response from Nessus, please wait...");
	}
	
	if ($("#scanReportGoogle").is(":checked") && reportType == "latest") { googleReport = 1; }
		
	$.post('./assets/nessus_api_funcs.inc.php', { 
				scanID: $('#scanID').val(), histID: genHistID, reportType: reportType, snRITM: $("#snRITM").val(), scanUUID: $("#scanUUID").val(), 
				scanName: $("#scanName").val(), scanReportGoogle: googleReport, scanLastModified: scanLastModified,
				scanCreator: $('#scanCreator').val(), scanRequestor: $('#scanRequestor').val(), scanTargetIPs: $('#scanTargetIPs').val(), 
				reportFormat: $('#reportFormat').val(), scanRecipients: $('#scanRecipients').val(), 
				scanStartDate: $('#scanStartDate').val(), scanEndDate: $('#scanEndDate').val(), scanFolderID: $('#scanFolderID').val(), 
				scanFolderName: $('#scanFolderName').val(), googleFolderName: $('#googleFolderName').val(),
				formAction: "genreport",
				casUser: $("#casUser").val(), userPermissions: $("#userPermissions").val()
	}, function(data) {
		alert(data);
		
		var respData = data.split("|GENREPORTDATA|");
		
		if (respData[0].trim() == "NONE") { 
			$('#scanURL').html(""); 
		}
		else if (reportType == "latest") { 
			$('#scanURL').html("Google Drive Link:<br/><a href='https://drive.google.com/uc?export=download&id="+respData[1].trim()+"' target='_blank'>https://drive.google.com/uc?export=download&id="+respData[1].trim()+"</a><br/>"); 
			$('#scanURL-'+genHistID).html("<a style='color: #fe5b00;' href='https://drive.google.com/uc?export=download&id="+respData[1].trim()+"' target='_blank'>DL Report</a><br/>"); 
			$('#genReportStatus').html("Report Exists.");
			$('#genReportStatusCBs').html("");
		} 
		else { 
			$('#scanURL-'+genHistID).html("<a style='color: #fe5b00;' href='https://drive.google.com/uc?export=download&id="+respData[1].trim()+"' target='_blank'>DL Report</a><br/>"); 

			if (genHistID == $('#histID').val()) {
				$('#scanURL').html("Google Drive Link:<br/><a href='https://drive.google.com/uc?export=download&id="+respData[1].trim()+"' target='_blank'>https://drive.google.com/uc?export=download&id="+respData[1].trim()+"</a><br/>");
				$('#genReportStatus').html("Report Exists.");
				$('#genReportStatusCBs').html("");
			}
		}
		
		if ($('#scanReportLocal').is(":checked") && reportType == "latest") {
			var win = window.open("", "_blank", "");
			win.document.body.innerHTML = respData[2];
			$('#scanURL').html($('#scanURL').html()+"A local copy of the report has been opened in a new window.");
		}
		
		// DEBUG
		$('#feedback').html(respData[1].trim()+", \nDEBUGMSG: "+respData[0]);

	}).fail(function() {
		$('#response').html("API call to Nessus failed.");
	});
	
	return false;	
}

function rejectScan() {
	
	$('#feedback').html("<img src='./images/loading16x16.gif' /> Awaiting response from Service-Now, please wait...");
	
	$.post('./assets/servicenow_funcs.php', { 
				jobID: $('#jobID').val(), casUserSysID: $("#casUserSysID").val(), 
				formAction: "rejectscan",
				casUser: $("#casUser").val(), userPermissions: $("#userPermissions").val()
	}, function(data) {
		
		var respData = data.split("|REJECTDATA|");
			
		$('#response').html(respData[0]);
		$('#formFrame').hide();
		$('#responseFrame').show();

	}).fail(function() {
		$('#formFrame').hide();
		$('#responseFrame').show();
		$('#response').html("API call to Service-Now failed.");
	});
	
	return false;	
}

function notifyRecipients() {
	
	$('#feedback').html("<img src='./images/loading16x16.gif' /> Awaiting response from Service-Now, please wait...");
	
	$.post('./assets/servicenow_funcs.php', { 
				snRITM: $('#snRITM').val(), googleDriveURL: $('#googleDriveURL').val(),
				formAction: "notifyrecipients",
				casUser: $("#casUser").val(), userPermissions: $("#userPermissions").val()
	}, function(data) {
		
		var respData = data.split("|NOTIFYDATA|");
		
		$('#notifyRecipients').prop("disabled", true);
		$('#feedback').html("");
		$('#response').html(respData[1]);
		$('#formFrame').hide();
		$('#responseFrame').show();

		//alert("DEBUG: "+data);
		
	}).fail(function() {
		$('#formFrame').hide();
		$('#responseFrame').show();
		$('#response').html("API call to Service-Now failed.");
	});
	
	return false;	
}
		
function approveScan() {
	
	$('#formFrame').hide();
	$('#responseFrame').show();

	//$('#feedback').html("<img src='./images/loading16x16.gif' /> Awaiting response from Service-Now, please wait...");
	$('#response').html("<img src='./images/loading16x16.gif' /> Awaiting response from Service-Now, please wait...");	
	
	if ($("#gotoSchedule").is(":checked")) { var gotoScheduleValue = "SCHEDULENOW"; } else { var gotoScheduleValue = "SCHEDULELATER"; }
	
	$.post('./assets/servicenow_funcs.php', { 
				jobID: $('#jobID').val(), casUserSysID: $("#casUserSysID").val(), 
				formAction: "approvescan",
				casUser: $("#casUser").val(), userPermissions: $("#userPermissions").val()
	}, function(data) {
		
		var respData = data.split("|APPROVEDATA|");
		
		$('#response').html(respData[0]);
		$('#formFrame').hide();
		$('#responseFrame').show();
		
		if (gotoScheduleValue == "SCHEDULENOW") { 
			$('#response').html($('#response').html()+"<br/>Redirecting to the Schedule Scan page, please wait...");
			window.location = "./scansched.php?jobID="+respData[1]; 
		}

	}).fail(function() {
		$('#formFrame').hide();
		$('#responseFrame').show();
		$('#response').html("API call to Service-Now failed.");
	});
	
	return false;	
}

function submitScan() {

	// ensure no values are frozen
	$('#dateTimeSchedScan').prop("readonly", false);
	$('#scanFrequency').prop("disabled", false);
	$('#scanInterval').prop("readonly", false);

	
	if ($('#scanName').val() == "") { $("#feedback").html("Error: The Scan Name value cannot be empty."); return false }
	if ($('#scanTargetIPs').val() == "") { $("#feedback").html("Error: The Scan Target IP values cannot be empty."); return false }

	if ($("#scanEnabled").is(":checked")) { var scanEnabledValue = "true"; } else { var scanEnabledValue = "false"; }

	if ($("#scanAutoExport").is(":checked")) { var autoExportReportEnabled = 1 } else { var autoExportReportEnabled = 0; }
	if ($("#scanAutoNotify").is(":checked")) { var autoNotifyReportEnabled = 1 } else { var autoNotifyReportEnabled = 0; }
	
	var scanFrequencyString = "FREQ="+$("#scanFrequency").val();
	
	var scanIntervalString = "INTERVAL="+$("#scanInterval").val();	
	
	var scanByDayString = "";
	
	if ($("#scanFrequency").val() == "WEEKLY") {
		$('input[name=BYDAY]').each(function () {
		   	if ($(this).is(":checked")) { scanByDayString = scanByDayString+$(this).val()+","; } 
		});

		if (scanByDayString != "") { 
			scanByDayString = "BYDAY="+scanByDayString; 
			scanByDayString = scanByDayString.slice(0, -1);
		}
		
		var rrulesString = scanFrequencyString+";"+scanIntervalString+";"+scanByDayString;
	}
	else if ($("#scanFrequency").val() == "ONCE") {
		var rrulesString = scanFrequencyString;
	}
	else {
		var rrulesString = scanFrequencyString+";"+scanIntervalString;
	}
	
	//alert("rrules string is: "+rrulesString);
	
	// No Service-Now RITM exists, create one...
	if ($("#snRITM").val() == "") {

		$('#feedback').html("<img src='./images/loading16x16.gif' /> Awaiting response from Service-Now, please wait...");

		if ($("#scanIsAuth").is(":checked")) { var scanIsAuthValue = "true"; } else { var scanIsAuthValue = "false"; }
		if ($("#scanIsUnauth").is(":checked")) { var scanIsUnauthValue = "true"; } else { var scanIsUnauthValue = "false"; }
		
		if ($("#scanRecurrence").is(":checked")) { var scanRecurrenceValue = "true"; } else { var scanRecurrenceValue = "false"; }
		
		$.post('./assets/servicenow_funcs.php', { 
					scanCreator: $("#scanCreator").val(), departmentSysID: $("#departmentSysID").val(), 
					scanRequestorSysID: $("#scanRequestorSysID").val(), casUserSysID: $("#casUserSysID").val(), 
					scanRequestor: $("#scanRequestor").val(), preferredTime: $("#preferredTime").val(), 
					scanHostApp: $("#scanHostApp").val(), scanIsUnauth: scanIsUnauthValue, 
					scanIsAuth: scanIsAuthValue, scanRecurrence: scanRecurrenceValue, scanTargetIPs: $("#scanTargetIPs").val(), 
					scanRecipients: $("#scanRecipients").val(), otherNotes: $("#otherNotes").val(), 
					formAction: "createritm",
					casUser: $("#casUser").val(), userPermissions: $("#userPermissions").val()
		}, function(data) {

			var createData = data.split("|CREATEDATA|");
			
			$('#response').html(createData[0]);
			$("#snRITM").val(createData[1]);
			
			if ($("#scanIsAuth").is(":checked")) { var scanIsAuthValue = 1; } else { var scanIsAuthValue = 0; }
			if ($("#scanIsUnauth").is(":checked")) { var scanIsUnauthValue = 1; } else { var scanIsUnauthValue = 0; }
			
			$.post('./assets/nessus_api_funcs.inc.php', {
					scanID: $('#scanID').val(), scanCreator: $("#scanCreator").val(), snRITM: $('#snRITM').val(),
					scanRequestor: $("#scanRequestor").val(), scanScheduled: $("#scanScheduled").val(), 
					scanEnabled: scanEnabledValue, dateTimeSchedScan: $("#dateTimeSchedScan").val(), 
					scanHostApp: $("#scanHostApp").val(), scanIsUnauth: scanIsUnauthValue, 
					scanIsAuth: scanIsAuthValue, reportFormat: $("#reportFormat").val(),
					scanFrequency: $("#scanFrequency").val(), scanRRules: rrulesString, googleFolderName: $("#googleFolderName").val(), 
					scanAutoExport: autoExportReportEnabled, scanAutoNotify: autoNotifyReportEnabled,
					scanName: $("#scanName").val(), scanTargetIPs: $("#scanTargetIPs").val(), scanRecipients: $("#scanRecipients").val(), 
					formAction: "submitscan",
					casUser: $("#casUser").val(), userPermissions: $("#userPermissions").val()
			}, function(data) {
				var ritmURL = "<a href='https://vt4help"+createData[3]+".service-now.com/sc_req_item.do?sys_id="+createData[2]+"&sysparm_view=ess&sysparm_record_target=sc_req_item'>"+createData[1]+"</a>";
				
				// DEBUG
				//$('#response').html($('#response').html()+"<br/><br/>"+data+"<br/>Service-Now Record created: "+ritmURL);
				
				// NORMAL
				$('#response').html(data+"<br/>Service-Now Record created: "+ritmURL);

			}).fail(function() {
				$('#response').html("API call to Nessus failed.");
			});

			$('#formFrame').hide();
			$('#responseFrame').show();
			//alert(createData[1]);

		}).fail(function() {
			$('#formFrame').hide();
			$('#responseFrame').show();
			$('#response').html("API call to Service-Now failed.");
		});
		
	} // END IF CREATE RITM
	else {
	
		if ($("#scanIsAuth").val() == "true") { var scanIsAuthValue = 1; } else { var scanIsAuthValue = 0; }
		if ($("#scanIsUnauth").val() == "true") { var scanIsUnauthValue = 1; } else { var scanIsUnauthValue = 0; }
		
		$.post('./assets/nessus_api_funcs.inc.php', {
				scanID: $('#scanID').val(), scanCreator: $("#scanCreator").val(), snRITM: $('#snRITM').val(),
				scanRequestor: $("#scanRequestor").val(), scanScheduled: $("#scanScheduled").val(), 
				scanEnabled: scanEnabledValue, dateTimeSchedScan: $("#dateTimeSchedScan").val(), 
				scanHostApp: $("#scanHostApp").val(), scanIsUnauth: scanIsUnauthValue, 
				scanIsAuth: scanIsAuthValue, reportFormat: $("#reportFormat").val(),
				scanFrequency: $("#scanFrequency").val(), scanRRules: rrulesString, googleFolderName: $("#googleFolderName").val(), 
				scanAutoExport: autoExportReportEnabled, scanAutoNotify: autoNotifyReportEnabled,
				scanName: $("#scanName").val(), scanTargetIPs: $("#scanTargetIPs").val(), scanRecipients: $("#scanRecipients").val(), 
				formAction: "submitscan",
				casUser: $("#casUser").val(), userPermissions: $("#userPermissions").val()
		}, function(data) {

			$('#response').html(data);

		}).fail(function() {
			$('#response').html("API call to Nessus failed.");
		});

		$("#formFrame").hide();			
		$("#responseFrame").show();
	}	
	return false;

} // END SUBMIT SCAN

function updateScanDB() {
	
	$("#scanCreator").val($("#scanCreatorUpdate").val());
	$("#scanRequestor").val($("#scanRequestorUpdate").val());
	$("#googleFolderName").val($("#googleFolderNameUpdate").val());
	$("#scanTargetIPs").val($("#scanTargetIPsUpdate").val());
	$("#scanRecipients").val($("#scanRecipientsUpdate").val());	
	
	if ($("#scanAutoExportUpdate").is(":checked")) { var autoExportReportEnabled = "true" } 
	else { var autoExportReportEnabled = "false"; }

	if ($("#scanAutoNotifyUpdate").is(":checked")) { var autoNotifyReportEnabled = "true" } 
	else { var autoNotifyReportEnabled = "false"; }
	
		 
	//alert("checked is: "+autoExportReportEnabled);
	
	$.post('./assets/nessus_api_funcs.inc.php', { 
				scanID: $('#scanID').val(), scanUUID: $('#scanUUID').val(),
				scanCreator: $("#scanCreator").val(), scanRequestor: $("#scanRequestor").val(), 
				scanName: $("#scanName").val(), snRITM: $("#snRITM").val(), 
				scanTargetIPs: $("#scanTargetIPs").val(), scanRecipients: $("#scanRecipients").val(),
				scanStartDate: $("#scanStartDate").val(), scanFolderID: $("#scanFolderID").val(),
				scanFolderName: $("#scanFolderName").val(), googleFolderName: $("#googleFolderName").val(), 
				scanAutoExport: autoExportReportEnabled, scanAutoNotify: autoNotifyReportEnabled,
				formAction: "updatescandb", scanNotInDB: $("#scanNotInDB").val(),
				casUser: $("#casUser").val(), userPermissions: $("#userPermissions").val()
	}, function(data) {

		var respData = data.split("|DATA|");
		
		// DEBUG
		$("#formFrame").hide();		
		$("#responseFrame").show();
		$('#response').html("ID: "+respData[0]+", DEBUG: "+respData[1]+"<br/>Reloading to reflect changes...");
				
		window.location = "./index.php?scanID="+respData[0];
		
	}).fail(function() {
		$('#response').html("DB call to HOIST failed.");
	});

	return false;

} // END UPDATE SCAN

		
function getFolderDestination(isDBUpdate) {

	if (isDBUpdate == 1) {
		var scanCreatorVal = $("#scanCreatorUpdate").val();
		var scanRequestorVal = $("#scanRequestorUpdate").val();		
	}
	else {
		var scanCreatorVal = $("#scanCreator").val();
		var scanRequestorVal = $("#scanRequestor").val();
	}
	
	$.post('./assets/servicenow_funcs.php', {
				scanCreator: scanCreatorVal, scanRequestor: scanRequestorVal,
				formAction: "getfolderdest",
				casUser: $("#casUser").val(), userPermissions: $("#userPermissions").val()
	}, function(data) {

		var respData = data.split("|DATA|");
		
		// DEBUG
		//alert("resp: "+respData[0]+", "+respData[1]);
		
		if (respData[0] == 1) { 
			$("#scanRequestor").val($("#casUser").val()); 
			$('#feedback').html("The Requestor PID value was not found.  Resetting PID to '<strong>"+$("#casUser").val()+"</strong>' and Destination '<strong>General_Scans</strong>'");
			$("#googleFolderName").val("General_Scans"); 
			$("#departmentSysID").val(""); 
			$("#scanRequestorSysID").val(""); 
			$("#casUserSysID").val(respData[4]);
			
		} else {
			$("#googleFolderName").val(respData[1]); 
			$("#googleFolderNameUpdate").val(respData[1]);
			$("#departmentSysID").val(respData[2]); 
			$("#scanRequestorSysID").val(respData[3]); 
			$("#casUserSysID").val(respData[4]);
		
			$('#feedback').html("Requestor PID Lookup in Service-Now set the Nessus/Google Drive Destination to: <br/><strong>"+respData[1]+"</strong>");
		}

	}).fail(function() {
		//alert("failed");
		$('#feedback').html("API call to Service-Now failed.");
	});

}

function openScanDetailsDialog() {
	$( "#scanDetailsDialog" ).dialog({ title: "Update HOIST Scan Details", modal: true, minWidth: 1200, minHeight: 730, position: { my: "center", at: "center", of: window } });
}

function openScanHistoryDialog() {
	$( "#scanHistoryDialog" ).dialog({ title: "Scan History", modal: true, minWidth: 1200, minHeight: 730, position: { my: "center", at: "center", of: window } });
}

<?php
	// Convert a date string to ISO format
?>
function ISODateString(d) {
	//DEBUG
	//alert (d);
	function pad(n){
		return n<10 ? '0'+n : n
	}
	return d.getUTCFullYear()+'-'
      + pad(d.getUTCMonth()+1)+'-'
      + pad(d.getUTCDate())+'T'
      + pad(d.getUTCHours())+':'
      + pad(d.getUTCMinutes())+':'
      + pad(d.getUTCSeconds())+'Z'
}

<?php
	// When field gets focus... if its contents equals the default malware desc 
	// blurb with instructions on filling out the empty field, empty it so the 
	// user can do so...
?>
function inputFocus(i, defaultTxt) {
    if (i.value == defaultTxt) {
        i.value = "";
        $(i).removeClass("blurredDefaultText");
    }
}

<?php
	// if the field is empty and we're leaving it, put the default malware blurb
	// back in and gray it out...
?>
function inputBlur(i, defaultTxt) {
    if (i.value == "") {
        i.value = defaultTxt;
        $(i).addClass("blurredDefaultText");
    }
}

		
function SaveToDisk(fileURL, fileName) {
    // for non-IE
    if (!window.ActiveXObject) {
        var save = document.createElement('a');
        save.href = fileURL;
        save.download = fileName || 'unknown';
        save.style = 'display:none;opacity:0;color:transparent;';
        (document.body || document.documentElement).appendChild(save);

        if (typeof save.click === 'function') {
            save.click();
        } else {
            save.target = '_blank';
            var event = document.createEvent('Event');
            event.initEvent('click', true, true);
            save.dispatchEvent(event);
        }

        (window.URL || window.webkitURL).revokeObjectURL(save.href);
    }

    // for IE
    else if (!!window.ActiveXObject && document.execCommand) {
        var _window = window.open(fileURL, '_blank');
        _window.document.close();
        _window.document.execCommand('SaveAs', true, fileName || fileURL);
        _window.close();
    }
}
	</script>
<?php 

// EOF javascripts.inc.php

?>